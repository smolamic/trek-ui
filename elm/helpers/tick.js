function tickOnIdle(callback) {
  setTimeout(
    () => window.requestIdleCallback(
      () => {
        callback();
        tickOnIdle(callback);
      }),
    30000,
  );
}

function tickAfterTimeout(callback) {
  setTimeout(
    () => {
      callback();
      tickAfterTimeout(callback);
    },
    30000,
  );
}

export default function tick(callback) {
  if (window.requestIdleCallback) {
    tickOnIdle(callback);
  } else {
    tickAfterTimeout(callback);
  }
}
