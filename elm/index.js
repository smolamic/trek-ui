/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
import { Elm } from './src/Main.elm';
import './reset.css';
import './components/html';
import './components/connect-notifier';
import './components/click-outside';
import './components/barcode';

const app = Elm.Main.init({
  node: document.body,
  flags: {
    scene: {
      width: window.innerWidth,
      height: window.innerHeight,
    },
    user: JSON.parse(window.localStorage.getItem('trek-user')),
  },
});

//app.ports.persistUser.subscribe((user) => {
//  window.localStorage.setItem('trek-user', JSON.stringify(user));
//});
//
//app.ports.clearUser.subscribe(() => {
//  window.localStorage.removeItem('trek-user');
//});

window.addEventListener('storage', () => {
  app.ports.updateUser.send(JSON.parse(window.localStorage.getItem('trek-user')));
});
