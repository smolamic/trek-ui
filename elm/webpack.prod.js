/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
/* eslint-disable */
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
  entry: {
    app: path.resolve(__dirname, 'index.js'),
  },
  context: path.resolve(__dirname),
  mode: 'production',

  module: {
    rules: [{
      test: /\.(css|scss|sass)$/,
      use: [{
        loader: 'style-loader',
      }, {
        loader: 'css-loader',
        options: { sourceMap: false },
      }],
    },{
      test: /\.svg$/,
      loader: 'svg-inline-loader',
    },{
      test: /\.(php|jpe?g)$/,
      loader: 'file-loader',
    },{
      test: /\.elm$/,
      loader: 'elm-webpack-loader',
      options: {
        cwd: __dirname,
        verbose: true,
        optimize: true,
        //debug: true,
      },
    }],
  },

  plugins: [
    new HtmlWebpackPlugin({
      title: 'Trek',
      meta: {
        viewport: 'width=device-width, initial-scale=1, shrink-to-fit=no',
        description: 'Trek - A simple SQL frontend',
        author: 'Michel Smola',
      },
    }),
    new webpack.DefinePlugin({
      DEBUG: false,
    }),
  ],

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'assets/trek.[hash].js',
    chunkFilename: 'assets/[name].[hash].js',
    publicPath: '/',
  },
};
