/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
window.customElements.define('trek-input', class TrekInput extends HTMLElement {
  constructor() {
    super();
    this.input = document.createElement('intput');
    this._value = '';
  }

  set value(value) {
    this.input.value = value;
  }

  get value() {
    return this.input.value;
  }

  set type(type) {
    this.input.type = type;
  }

  get type() {
    return this.input.type;
  }

  connectedCallback() {
    this.input = document.createElement('input');
  }
});
