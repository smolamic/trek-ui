/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
window.customElements.define('trek-connect-notifier', class TrekConnectNotifier extends HTMLElement {
  connectedCallback() {
    this.dispatchEvent(new CustomEvent('connect'));
  }
});
