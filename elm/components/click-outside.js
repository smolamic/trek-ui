/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
window.customElements.define('trek-click-outside', class TrekClickOutside extends HTMLElement {
  constructor() {
    super();
    this._active = false;
    this.handleClick = this.handleClick.bind(this);
  }

  set active(active) {
    if (active) {
      window.addEventListener('click', this.handleClick, true);
    } else {
      window.removeEventListener('click', this.handleClick, true);
    }
    this._active = active;
  }

  get active() {
    return this._active;
  }

  handleClick(event) {
    if (!this.contains(event.target)) {
      event.stopPropagation();
      event.preventDefault();
      this.dispatchEvent(new CustomEvent('clickoutside'));
    }
  }

  disconnectedCallback() {
    window.removeEventListener('click', this.handleClick, true);
  }
});
