/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
window.customElements.define('trek-html', class TrekHtml extends HTMLElement {
  constructor() {
    super();
    this.section = document.createElement('section');
  }

  connectedCallback() {
    this.appendChild(this.section);
  }

  set content(content) {
    this.section.innerHTML = content;
  }

  get content() {
    return this.section.innerHTML;
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (name === 'content') this.section.innerHTML = newValue;
  }
});
