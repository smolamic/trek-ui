/**
 * @package Trek
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
window.customElements.define('trek-barcode', class TrekBarcode extends HTMLElement {
  constructor() {
    super();
    this._value = null;
    this.img = document.createElement('img');
  }

  set className(className) {
    this.img.className = className;
  }

  get className() {
    return this.img.className;
  }

  set value(value) {
    this._value = value;
    this.img.id = `barcode-${this._value}`;
    if (this.isConnected) this.render();
  }

  async render() {
    const JsBarcode = (await import(/* webpackChunkName: "jsbarcode" */ 'jsbarcode')).default;
    switch (this._value.length) {
      case 7:
      case 8:
        JsBarcode(`#barcode-${this._value}`, this._value, {
          format: 'EAN8',
          displayValue: true,
          width: 2,
          height: 80,
          fontSize: 16,
        });
        return;
      case 12:
      case 13:
      default:
        JsBarcode(`#barcode-${this._value}`, this._value, {
          format: 'EAN13',
          displayValue: true,
          width: 2,
          height: 80,
          fontSize: 16,
        });
        return;
    }
  }

  get value() {
    return this._value;
  }

  connectedCallback() {
    this.appendChild(this.img);
    if (this.isConnected) this.render();
  }
});
