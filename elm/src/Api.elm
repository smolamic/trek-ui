{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Api exposing (update)

import Api.Core exposing (..)
import Api.Decode exposing (ignore, response)
import Api.Request
import Browser.Navigation as Nav
import Config
import Config.Core exposing (Config)
import Core
import Dict exposing (Dict)
import Enum.ErrorCode as ErrorCode
import Form
import Form.Core exposing (Row)
import Http
import Input.Core
import Json.Decode as D
import Log
import Log.Core
import Login
import Login.Core
import Main
import Main.Core exposing (Index, Password, User)
import Path
import Process
import StateTimer
import Task
import Time exposing (Posix)
import Translation
import Translation.Core exposing (Translation)
import Update
import Url.Builder exposing (QueryParameter)
import Views
import Views.Core exposing (View)



-- CONST


update : Posix -> Msg -> Core.Model -> ( Core.Model, Cmd Core.Msg )
update time msg model =
    case msg of
        GotAuthenticateResponse response ->
            handleAuthenticateResponse time response model

        GotConfigResponse response ->
            handleConfigResponse response model

        GotTranslationResponse response ->
            handleTranslationResponse response model

        GotIndexResponse count response ->
            handleIndexResponse time count response model

        GotViewResponse path requestTime response ->
            handleViewResponse path requestTime response model

        GotDeleteResponse path response ->
            handleDeleteResponse time path response model

        GotReconcileResponse path response ->
            handleReconcileResponse time path response model

        GotReconcileBatchResponse user path rest response ->
            handleReconcileBatchResponse time user path rest response model

        ReconcileBatch user rows ->
            ( model, Api.Request.reconcileBatch user rows )

        GotRunResponse path count response ->
            handleRunResponse path count response model

        GotUnlockResponse response ->
            handleUnlockResponse response model

        GotSaveResponse path response ->
            handleSaveResponse time path response model

        GotCheckResponse path requestTime response ->
            handleCheckResponse time path requestTime response model

        GotPasswordResponse count response ->
            handlePasswordResponse count response model

        GotTokenResponse requestTime response ->
            handleTokenResponse requestTime response model


handleAuthenticateResponse : Posix -> Response User () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleAuthenticateResponse time response model =
    case response of
        Success user ->
            let
                ( main, mainCmd ) =
                    Main.init (StateTimer.new time user)
            in
            ( Core.setMain main model, mainCmd )

        ApiError error ->
            Log.stdErr error.message model

        BackendError error ->
            case error.code of
                ErrorCode.AuthenticationFailure ->
                    Log.stdErr error.message model
                        |> Update.andThen (Login.update Login.Core.AuthenticationFailed)

                _ ->
                    Log.stdErr error.message model


handleConfigResponse : Response Config () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleConfigResponse response model =
    case response of
        Success config ->
            Config.update (Config.Core.GotConfig config) model

        ApiError error ->
            Log.stdErr error.message model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            Log.stdErr ("api/config: " ++ error.message) model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleTranslationResponse : Response Translation () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleTranslationResponse response model =
    case response of
        Success translation ->
            Translation.update (Translation.Core.GotTranslation translation) model

        ApiError error ->
            Log.stdErr error.message model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            Log.stdErr error.message model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)


loginInvalid : String -> Core.Model -> ( Core.Model, Cmd Core.Msg )
loginInvalid message model =
    case model.page of
        Core.LoginPage _ ->
            ( model, Cmd.none )

        Core.MainPage main ->
            let
                ( login, loginCmd ) =
                    Login.withUsername (StateTimer.unwrap main.user |> .name)
            in
            ( { model | page = Core.LoginPage login }, loginCmd )
                |> Update.andThen (Log.update Log.Core.Clear)
                |> Update.andThen (Log.update (Log.Core.StdErr message))


handleIndexResponse : Posix -> Int -> Response Index () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleIndexResponse time count response model =
    case response of
        Success index ->
            Main.update (Main.Core.GotIndex count index time) model

        ApiError error ->
            Log.stdErr error.message model
                |> Update.andThen (Main.update (Main.Core.IndexFailed count))
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Main.update (Main.Core.IndexFailed count))
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleViewResponse : String -> Posix -> Response View () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleViewResponse path requestTime response model =
    case response of
        Success view ->
            Views.update (Views.Core.GotView requestTime view) model

        ApiError error ->
            Log.stdErr ("api/view: " ++ error.message) model
                |> Update.andThen (Views.update (Views.Core.ViewFailed requestTime path))
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr ("api/view: " ++ error.message) model
                        |> Update.andThen (Views.update (Views.Core.ViewFailed requestTime path))
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleCheckResponse : Posix -> String -> Posix -> Response View () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleCheckResponse time path requestTime response model =
    case response of
        Success view ->
            Views.update (Views.Core.GotView requestTime view) model
                |> Update.andThen (Main.update (Main.Core.Refresh time))

        ApiError error ->
            Log.stdErr ("api/check: " ++ error.message) model
                |> Update.andThen (Views.update (Views.Core.ViewFailed requestTime path))
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr ("api/check: " ++ error.message) model
                        |> Update.andThen (Views.update (Views.Core.ViewFailed requestTime path))
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handlePasswordResponse : Int -> Response Password () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handlePasswordResponse count response model =
    case response of
        Success pw ->
            Main.update (Main.Core.GotPassword count pw) model

        ApiError error ->
            Log.stdErr ("api/password: " ++ error.message) model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr ("api/password: " ++ error.message) model
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleDeleteResponse : Posix -> String -> Response String () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleDeleteResponse time path response model =
    case response of
        Success viewPath ->
            Views.update (Views.Core.Unblock path) model
                |> Update.andThen
                    (Main.update (Main.Core.Refresh time))
                |> Update.cmd
                    (\newModel -> Nav.pushUrl newModel.key viewPath)

        ApiError error ->
            Log.stdErr ("api/delete: " ++ error.message) model
                |> Update.andThen (Views.update (Views.Core.Unblock path))
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Views.update (Views.Core.Unblock path))
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleReconcileResponse : Posix -> String -> Response String () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleReconcileResponse time path response model =
    case response of
        Success newPath ->
            Views.update (Views.Core.Unblock path) model
                |> Update.andThen
                    (Main.update (Main.Core.Refresh time))
                |> Update.cmd
                    (\newModel -> Nav.pushUrl newModel.key newPath)

        ApiError error ->
            Log.stdErr ("api/reconcile: " ++ error.message) model
                |> Update.andThen (Views.update (Views.Core.Unblock path))
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Views.update (Views.Core.Unblock path))
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleReconcileBatchResponse : Posix -> User -> String -> List String -> Response String () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleReconcileBatchResponse time user path rest response model =
    case response of
        Success _ ->
            Log.stdOut ("Reconcile Batch: Successful for " ++ path) model
                |> Update.andThen (Views.update (Views.Core.Unblock path))
                |> (if List.isEmpty rest then
                        Update.andThen (Main.update (Main.Core.Refresh time))

                    else
                        Update.cmd
                            (\_ ->
                                Task.perform
                                    (\_ -> Core.ApiMsg (ReconcileBatch user rest))
                                    (Process.sleep 50)
                            )
                   )

        ApiError error ->
            Log.stdErr ("api/reconcileBatch at " ++ path ++ ": " ++ error.message) model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleRunResponse : String -> Int -> Response Row () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleRunResponse path count response model =
    case response of
        Success row ->
            Form.update (Form.Core.GotRow path count row) model

        ApiError error ->
            Log.stdErr ("api/run: " ++ error.message) model
                |> Update.andThen (Form.update (Form.Core.RunFailed path count))

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Form.update (Form.Core.RunFailed path count))


handleUnlockResponse : Response () () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleUnlockResponse response model =
    case response of
        Success _ ->
            ( model, Cmd.none )

        ApiError error ->
            Log.stdErr error.message model
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleSaveResponse : Posix -> String -> Response ( String, String ) () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleSaveResponse time path response model =
    case response of
        Success ( viewPath, rowPath ) ->
            Form.update (Form.Core.SaveSucceeded path) model
                |> Update.andThen (Main.update (Main.Core.Refresh time))
                |> Update.cmd
                    (\model_ -> Nav.pushUrl model_.key rowPath)

        ApiError error ->
            Log.stdErr error.message model
                |> Update.andThen (Form.update (Form.Core.SaveFailed path))
                |> Update.andThen (Main.update Main.Core.OpenLogMenu)

        BackendError error ->
            case error.code of
                ErrorCode.InvalidInput ->
                    Form.update (Form.Core.SaveInvalid path) model

                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
                        |> Update.andThen (Form.update (Form.Core.SaveFailed path))
                        |> Update.andThen (Main.update Main.Core.OpenLogMenu)


handleTokenResponse : Posix -> Response User () -> Core.Model -> ( Core.Model, Cmd Core.Msg )
handleTokenResponse time response model =
    case response of
        Success user ->
            Main.update (Main.Core.GotUser user time) model

        ApiError error ->
            Log.stdErr error.message model

        BackendError error ->
            case error.code of
                ErrorCode.LoginExpired ->
                    loginInvalid error.message model

                ErrorCode.AuthenticationFailure ->
                    loginInvalid error.message model

                _ ->
                    Log.stdErr error.message model
