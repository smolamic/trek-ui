{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Cache exposing (Cache, cache, cachedKeys, cachedValues, clean, empty, get, load, match, matchAll, miss, refresh, time, update)

import Dict exposing (Dict)
import RemoteResource as Rr exposing (RemoteResource(..))
import RemoteResource.Timer as Rt exposing (ResourceTimer)
import StateTimer as St
import Time exposing (Posix)


type alias Cache a =
    Dict String (ResourceTimer a)


get : String -> Cache a -> RemoteResource a
get key cache_ =
    case Dict.get key cache_ of
        Just val ->
            St.unwrap val

        Nothing ->
            Missing


cache : String -> Posix -> a -> Cache a -> Cache a
cache key time_ val =
    Dict.update key (Just << Rt.cache time_ val << Maybe.withDefault (Rt.loading time_))


load : List String -> Posix -> Cache a -> Cache a
load keys time_ cache_ =
    List.foldl
        (\key newCache ->
            Dict.update key
                (Just << Rt.load time_ << Maybe.withDefault (Rt.loading time_))
                newCache
        )
        cache_
        keys


clean : Int -> Posix -> Cache a -> Cache a
clean timeout time_ =
    Dict.filter (\_ rt -> St.age time_ rt < timeout)


refresh : Int -> (String -> ResourceTimer a -> Bool) -> Posix -> Cache a -> Cache a
refresh timeout shouldRefresh time_ cache_ =
    Dict.filter (\_ rt -> St.age time_ rt < timeout) cache_
        |> Dict.map
            (\key res ->
                if shouldRefresh key res then
                    Rt.load time_ res

                else
                    res
            )


miss : String -> Posix -> Cache a -> Cache a
miss key time_ cache_ =
    Dict.update key (Just << Rt.miss time_ << Maybe.withDefault (Rt.missing time_)) cache_


empty : Cache a
empty =
    Dict.empty


match : String -> Cache a -> ( String, RemoteResource a )
match search cache_ =
    let
        res =
            Dict.filter (\key _ -> String.startsWith key search) cache_
                |> Dict.map (\_ value -> St.unwrap value)
                |> Dict.toList
                |> List.reverse
                |> List.head
    in
    case res of
        Just closestMatch ->
            closestMatch

        Nothing ->
            ( search, Missing )


matchAll : String -> Cache a -> List ( String, RemoteResource a )
matchAll search cache_ =
    Dict.filter (\key _ -> String.startsWith key search) cache_
        |> Dict.map (\_ value -> St.unwrap value)
        |> Dict.toList
        |> List.reverse


cachedValues : Cache a -> List a
cachedValues cache_ =
    Dict.values cache_
        |> List.filterMap (St.unwrap >> Rr.toMaybe)


cachedKeys : Cache a -> List String
cachedKeys cache_ =
    Dict.toList cache_
        |> List.filterMap
            (\( key, value ) ->
                St.unwrap value |> Rr.toMaybe |> Maybe.map (\_ -> key)
            )


update : String -> Posix -> (RemoteResource a -> RemoteResource a) -> Cache a -> Cache a
update key time_ method =
    Dict.update key (Maybe.map (St.update time_ method))


time : String -> Cache a -> Posix
time key =
    Dict.get key
        >> Maybe.map St.time
        >> Maybe.withDefault (Time.millisToPosix 0)
