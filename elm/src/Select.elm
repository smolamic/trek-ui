{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Select exposing (main_)

import Core exposing (..)
import Main.Core exposing (Main)


main_ : Model -> Maybe Main
main_ model =
    case model.page of
        MainPage main ->
            Just main

        _ ->
            Nothing
