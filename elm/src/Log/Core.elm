{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Log.Core exposing (Log, LogEntry(..), Msg(..))

import Array exposing (Array)


type alias Log =
    Array LogEntry


type Msg
    = StdErr String
    | StdOut String
    | Clear


type LogEntry
    = Output String
    | Error String
