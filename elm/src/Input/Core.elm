{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Input.Core exposing (Attributes, BasicInputValue, BoolInputValue, CheckInputValue, InputOption, RefInputValue, State(..), StringInputValue, Value(..), Variant)

import Array exposing (Array)
import RemoteResource exposing (RemoteResource)


type alias BasicInputValue =
    { value : Maybe String
    , visible : Bool
    , validity : List String
    }


type alias BoolInputValue =
    { value : Maybe Bool
    , visible : Bool
    , validity : List String
    }


type alias CheckInputValue =
    { value : Bool
    , visible : Bool
    }


type alias Variant =
    { value : Int
    , label : String
    }


type alias InputOption =
    { value : String
    , label : String
    }


type alias StringInputValue =
    { value : Maybe String
    , visible : Bool
    , validity : List String
    , options : Array InputOption
    }


type alias RefInputValue =
    { value : Maybe String
    , label : Maybe String
    , search : Maybe String
    , visible : Bool
    , validity : List String
    , options : Array InputOption
    , accept : Bool
    }


type Value
    = BasicValue BasicInputValue
    | BoolValue BoolInputValue
    | CheckValue CheckInputValue
    | StringValue StringInputValue
    | RefValue RefInputValue
    | BarcodeValue BasicInputValue


type State
    = Active
    | Enabled
    | Disabled


type alias Attributes msg =
    { onFocus : msg
    , onBlur : msg
    , onMouseOver : msg
    , onMouseOut : msg
    , onSelect : Int -> msg
    , id : String
    , required : Bool
    , state : State
    , selected : Int
    , onAccept : Value -> msg
    , onInput : Value -> msg
    , value : RemoteResource Value
    , unit : Maybe String
    , step : Maybe String
    , variants : Maybe (List Variant)
    }
