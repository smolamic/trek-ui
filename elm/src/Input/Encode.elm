{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Input.Encode exposing (value)

import Input.Core exposing (..)
import Json.Encode exposing (..)


value : Input.Core.Value -> Json.Encode.Value
value value_ =
    case value_ of
        BasicValue bv ->
            basicValue bv

        BoolValue bv ->
            boolValue bv

        CheckValue cv ->
            checkValue cv

        StringValue sv ->
            basicValue sv

        RefValue rv ->
            refValue rv

        BarcodeValue bv ->
            basicValue bv


nullable : (v -> Json.Encode.Value) -> Maybe v -> Json.Encode.Value
nullable encoder maybeValue =
    case maybeValue of
        Just value_ ->
            encoder value_

        Nothing ->
            null


basicValue : { v | value : Maybe String } -> Json.Encode.Value
basicValue bv =
    object
        [ ( "value", nullable string bv.value ) ]


boolValue : BoolInputValue -> Json.Encode.Value
boolValue bv =
    object
        [ ( "value", nullable bool bv.value ) ]


checkValue : CheckInputValue -> Json.Encode.Value
checkValue cv =
    object
        [ ( "value", bool cv.value ) ]


refValue : RefInputValue -> Json.Encode.Value
refValue rv =
    object
        [ ( "value", nullable string rv.value )
        , ( "search", nullable string rv.search )
        , ( "accept", bool rv.accept )
        ]
