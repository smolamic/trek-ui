{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Input.Filter exposing (bool, enum, number_, text, timestamp)

import Config.Core exposing (Theme)
import Core exposing (Model)
import Css exposing (Style)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Json.Encode as E
import Translation.Core exposing (tr, trText)
import Views.Core exposing (Filter(..), Variant, between, equal, greaterThan, lessThan, like)



-- STYLE


inputStyle : Theme -> Style
inputStyle theme =
    Css.batch
        [ Css.border3 (Css.px 1) Css.solid theme.grey.medium
        , Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.05) theme.grey.medium
        , Css.padding (Css.rem 0.2)
        , Css.color theme.black
        , Css.boxSizing Css.borderBox
        , Css.backgroundColor Css.transparent
        , Css.fontSize (Css.em 1)
        , Css.width (Css.pct 100)
        ]



-- INPUTS


text : Model -> Attributes -> Html Core.Msg
text model attr =
    let
        theme =
            model.config.theme
    in
    case attr.filter of
        Like that ->
            Html.input
                [ Attr.css [ inputStyle theme ]
                , Attr.value (String.slice 1 -1 that)
                , Attr.placeholder attr.label
                , Attr.id attr.id
                , Evt.onInput (attr.onInput << like)
                ]
                []

        All ->
            Html.input
                [ Attr.css [ inputStyle theme ]
                , Attr.placeholder attr.label
                , Attr.id attr.id
                , Evt.onInput (attr.onInput << like)
                ]
                []

        _ ->
            Html.input
                [ Attr.css [ inputStyle theme ]
                , Attr.disabled True
                , Attr.placeholder attr.label
                , Attr.id attr.id
                , Attr.value (tr model.translation "Wrong input type.")
                ]
                []


number_ : Model -> Attributes -> Html Core.Msg
number_ model attr =
    let
        theme =
            model.config.theme

        row symbol msg value =
            Html.span
                [ Attr.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.row
                    , Css.boxSizing Css.borderBox
                    , Css.justifyContent Css.flexEnd
                    , Css.width (Css.pct 100)
                    , Css.position Css.relative
                    ]
                ]
                [ Html.label
                    [ Attr.for (attr.id ++ "-" ++ symbol)
                    , Attr.css
                        [ Css.padding (Css.em 0.5)
                        , Css.position Css.absolute
                        , Css.height (Css.pct 100)
                        , Css.left Css.zero
                        , Css.top Css.zero
                        , Css.displayFlex
                        , Css.justifyContent Css.center
                        , Css.alignItems Css.center
                        ]
                    ]
                    [ Html.text symbol ]
                , Html.input
                    [ Attr.css
                        [ inputStyle theme
                        , Css.marginBottom (Css.em 0.2)
                        , Css.paddingLeft (Css.em 1.5)
                        ]
                    , Attr.value value
                    , Attr.type_ "number"
                    , Attr.step "any"
                    , Attr.placeholder attr.label
                    , Attr.id (attr.id ++ "-" ++ symbol)
                    , Evt.onInput (attr.onInput << msg)
                    ]
                    []
                ]

        column =
            Html.div
                [ Attr.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.column
                    ]
                ]
    in
    case attr.filter of
        GreaterThan gt ->
            column
                [ row ">" greaterThan gt
                , row "<" (between gt) ""
                ]

        LessThan lt ->
            column
                [ row ">" (\gt -> between gt lt) ""
                , row "<" lessThan lt
                ]

        Between gt lt ->
            column
                [ row ">" (\new -> between new lt) gt
                , row "<" (between gt) lt
                ]

        All ->
            column
                [ row ">" greaterThan ""
                , row "<" lessThan ""
                ]

        _ ->
            Html.div
                [ Attr.css [ Css.overflowWrap Css.breakWord ] ]
                [ trText model.translation "Wrong input type." ]


checked : Bool -> Filter
checked isChecked =
    if isChecked then
        Equal "1"

    else
        Equal "0"


bool : Model -> Attributes -> Html Core.Msg
bool model attr =
    let
        theme =
            model.config.theme

        container =
            Html.span
                [ Attr.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.row
                    , Css.justifyContent Css.center
                    , Css.width (Css.pct 100)
                    ]
                ]
    in
    case attr.filter of
        Equal "1" ->
            container
                [ Html.input
                    [ Attr.type_ "checkbox"
                    , Attr.checked True
                    , Evt.onCheck (attr.onInput << checked)
                    ]
                    []
                ]

        Equal "0" ->
            container
                [ Html.input
                    [ Attr.type_ "checkbox"
                    , Attr.checked False
                    , Evt.onCheck (attr.onInput << checked)
                    ]
                    []
                ]

        All ->
            container
                [ Html.input
                    [ Attr.type_ "checkbox"
                    , Attr.checked False
                    , Evt.onCheck (attr.onInput << checked)
                    ]
                    []
                ]

        _ ->
            container
                [ trText model.translation "Wrong input type." ]


enum : Model -> Attributes -> Html Core.Msg
enum model attr =
    let
        theme =
            model.config.theme

        select current =
            Html.select
                [ Attr.css [ inputStyle theme ]
                , Attr.value (Maybe.withDefault "" current)
                , Attr.id attr.id
                , Evt.onInput (attr.onInput << equal)
                ]
                (case attr.variants of
                    Just variants ->
                        List.map
                            (\variant ->
                                Html.option [ Attr.value (String.fromInt variant.value) ]
                                    [ Html.text variant.label ]
                            )
                            variants

                    Nothing ->
                        []
                )
    in
    case attr.filter of
        Equal index ->
            select (Just index)

        All ->
            select Nothing

        _ ->
            Html.input
                [ Attr.css [ inputStyle theme ]
                , Attr.disabled True
                , Attr.value (tr model.translation "Wrong input type.")
                , Attr.id attr.id
                ]
                []


type alias TimeStamp =
    { year : String
    , month : String
    , day : String
    , hour : String
    , minute : String
    , second : String
    }


parseTimeStamp : String -> Result String TimeStamp
parseTimeStamp raw =
    case String.split " " raw of
        [ date, time ] ->
            case ( String.split "-" date, String.split ":" time ) of
                ( [ year, month, day ], [ hour, minute, second ] ) ->
                    Ok
                        (TimeStamp
                            (String.replace "_" "" year)
                            (String.replace "_" "" month)
                            (String.replace "_" "" day)
                            (String.replace "_" "" hour)
                            (String.replace "_" "" minute)
                            (String.replace "_" "" second)
                        )

                ( [ year, month, day ], _ ) ->
                    Err "Time does not match required format"

                ( _, [ hour, minute, second ] ) ->
                    Err "Date does not match required format"

                ( _, _ ) ->
                    Err "Timestamp does not match required format"

        _ ->
            Err "Timestamp does not match required format"


emptyTimeStamp : TimeStamp
emptyTimeStamp =
    TimeStamp "" "" "" "" "" ""


setYear : String -> Filter
setYear year =
    if String.isEmpty year then
        All

    else
        Like (String.padRight 4 '_' (String.left 4 year) ++ "-__-__ __:__:__")


formatNumber : String -> String
formatNumber val =
    if String.isEmpty val then
        "__"

    else
        String.padLeft 2 '0' (String.left 2 val)


setMonth : TimeStamp -> String -> Filter
setMonth ts month =
    Like
        (ts.year
            ++ "-"
            ++ formatNumber month
            ++ "-__ __:__:__"
        )


setDay : TimeStamp -> String -> Filter
setDay ts day =
    Like
        (ts.year
            ++ "-"
            ++ ts.month
            ++ "-"
            ++ formatNumber day
            ++ " __:__:__"
        )


setHour : TimeStamp -> String -> Filter
setHour ts hour =
    Like
        (ts.year
            ++ "-"
            ++ ts.month
            ++ "-"
            ++ ts.day
            ++ " "
            ++ formatNumber hour
            ++ ":__:__"
        )


setMinute : TimeStamp -> String -> Filter
setMinute ts minute =
    Like
        (ts.year
            ++ "-"
            ++ ts.month
            ++ "-"
            ++ ts.day
            ++ " "
            ++ ts.hour
            ++ ":"
            ++ formatNumber minute
            ++ ":__"
        )


setSecond : TimeStamp -> String -> Filter
setSecond ts second =
    Like
        (ts.year
            ++ "-"
            ++ ts.month
            ++ "-"
            ++ ts.day
            ++ " "
            ++ ts.hour
            ++ ":"
            ++ ts.minute
            ++ ":"
            ++ formatNumber second
        )


timestamp : Model -> Attributes -> Html Core.Msg
timestamp model attr =
    let
        theme =
            model.config.theme

        column =
            Html.div
                [ Attr.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.column
                    ]
                ]

        yearInput ts =
            Html.input
                [ Attr.type_ "number"
                , Attr.css [ inputStyle theme ]
                , Attr.min "1970"
                , Attr.max "2500"
                , Attr.step "1"
                , Attr.placeholder "year"
                , Attr.value ts.year
                , Evt.onInput (attr.onInput << setYear)
                ]
                []

        monthInput ts =
            if String.length ts.year < 4 then
                Html.text ""

            else
                Html.input
                    [ Attr.type_ "number"
                    , Attr.css [ inputStyle theme ]
                    , Attr.min "1"
                    , Attr.max "12"
                    , Attr.step "1"
                    , Attr.placeholder "month"
                    , Attr.value ts.month
                    , Evt.onInput (attr.onInput << setMonth ts)
                    ]
                    []

        dayInput ts =
            if String.isEmpty ts.month then
                Html.text ""

            else
                Html.input
                    [ Attr.type_ "number"
                    , Attr.css [ inputStyle theme ]
                    , Attr.min "1"
                    , Attr.max "31"
                    , Attr.step "1"
                    , Attr.placeholder "day"
                    , Attr.value ts.day
                    , Evt.onInput (attr.onInput << setDay ts)
                    ]
                    []

        hourInput ts =
            if String.isEmpty ts.day then
                Html.text ""

            else
                Html.input
                    [ Attr.type_ "number"
                    , Attr.css [ inputStyle theme ]
                    , Attr.min "0"
                    , Attr.max "23"
                    , Attr.step "1"
                    , Attr.placeholder "hour"
                    , Attr.value ts.hour
                    , Evt.onInput (attr.onInput << setHour ts)
                    ]
                    []

        minuteInput ts =
            if String.isEmpty ts.hour then
                Html.text ""

            else
                Html.input
                    [ Attr.type_ "number"
                    , Attr.css [ inputStyle theme ]
                    , Attr.min "0"
                    , Attr.max "59"
                    , Attr.step "1"
                    , Attr.placeholder "minute"
                    , Attr.value ts.minute
                    , Evt.onInput (attr.onInput << setMinute ts)
                    ]
                    []

        secondInput ts =
            if String.isEmpty ts.minute then
                Html.text ""

            else
                Html.input
                    [ Attr.type_ "number"
                    , Attr.css [ inputStyle theme ]
                    , Attr.min "0"
                    , Attr.max "59"
                    , Attr.step "1"
                    , Attr.placeholder "second"
                    , Attr.value ts.second
                    , Evt.onInput (attr.onInput << setSecond ts)
                    ]
                    []

        errorDisplay message =
            Html.input
                [ Attr.css [ inputStyle theme ]
                , Attr.disabled True
                , Attr.value (tr model.translation message)
                , Attr.id attr.id
                ]
                []
    in
    case attr.filter of
        All ->
            column [ yearInput emptyTimeStamp ]

        Like that ->
            case parseTimeStamp that of
                Ok ts ->
                    column
                        [ yearInput ts
                        , monthInput ts
                        , dayInput ts
                        , hourInput ts
                        , minuteInput ts
                        , secondInput ts
                        ]

                Err message ->
                    errorDisplay message

        _ ->
            errorDisplay "Wrong input type."



--
-- ATTRIBUTES


type alias Attributes =
    { filter : Filter
    , onInput : Filter -> Core.Msg
    , label : String
    , id : String
    , variants : Maybe (List Variant)
    }
