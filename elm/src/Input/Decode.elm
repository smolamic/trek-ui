{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Input.Decode exposing (barcodeValue, basicValue, boolValue, checkValue, refValue, stringValue)

import Input.Core exposing (..)
import Json.Decode exposing (..)


basicValue : Decoder Input.Core.Value
basicValue =
    map3 BasicInputValue
        (field "value" (nullable string))
        (field "visible" bool)
        (field "validity" (list string))
        |> map BasicValue


boolValue : Decoder Input.Core.Value
boolValue =
    map3 BoolInputValue
        (field "value" (nullable bool))
        (field "visible" bool)
        (field "validity" (list string))
        |> map BoolValue


checkValue : Decoder Input.Core.Value
checkValue =
    map2 CheckInputValue
        (field "value" bool)
        (field "visible" bool)
        |> map CheckValue


inputOption : Decoder InputOption
inputOption =
    map2 InputOption
        (index 0 string)
        (index 1 string)


stringValue : Decoder Input.Core.Value
stringValue =
    map4 StringInputValue
        (field "value" (nullable string))
        (field "visible" bool)
        (field "validity" (list string))
        (field "options" (array inputOption))
        |> map StringValue


refValue : Decoder Input.Core.Value
refValue =
    map7 RefInputValue
        (field "value" (nullable string))
        (field "label" (nullable string))
        (field "search" (nullable string))
        (field "visible" bool)
        (field "validity" (list string))
        (field "options" (array inputOption))
        (succeed False)
        |> map RefValue


barcodeValue : Decoder Input.Core.Value
barcodeValue =
    map3 BasicInputValue
        (field "value" (nullable string))
        (field "visible" bool)
        (field "validity" (list string))
        |> map BarcodeValue
