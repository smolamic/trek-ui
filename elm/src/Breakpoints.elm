{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Breakpoints exposing (Breakpoint(..), down, isDesktop, isMobile, isTouch, max, min, only, up)


type Breakpoint
    = Xl
    | Lg
    | Md
    | Sm
    | Xs


toFloat : Breakpoint -> Float
toFloat breakpoint =
    case breakpoint of
        Xl ->
            1920

        Lg ->
            1024

        Md ->
            768

        Sm ->
            576

        Xs ->
            0


up : Breakpoint -> Float -> Bool
up breakpoint width =
    width >= toFloat breakpoint


down : Breakpoint -> Float -> Bool
down breakpoint width =
    width < toFloat breakpoint


only : Breakpoint -> Float -> Bool
only breakpoint width =
    case breakpoint of
        Xl ->
            width >= 1920

        Lg ->
            width >= 1024 && width < 1920

        Md ->
            width >= 768 && width < 1024

        Sm ->
            width >= 576 && width < 768

        Xs ->
            width < 576


isTouch : Float -> Bool
isTouch =
    down Lg


isDesktop : Float -> Bool
isDesktop =
    up Lg


isMobile : Float -> Bool
isMobile =
    down Md


min : Float -> Breakpoint
min width =
    if width >= toFloat Xl then
        Xl

    else if width >= toFloat Lg then
        Lg

    else if width >= toFloat Md then
        Md

    else if width >= toFloat Sm then
        Sm

    else
        Xs


max : Float -> Breakpoint
max width =
    if width < toFloat Sm then
        Sm

    else if width < toFloat Md then
        Md

    else if width < toFloat Lg then
        Lg

    else
        Xl


desktopOr : fn -> fn -> Float -> fn
desktopOr onDesktop onMobile width =
    if isDesktop width then
        onDesktop

    else
        onMobile
