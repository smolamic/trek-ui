{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Trek exposing (View, activeRow, sheetView, stringColumn, stringColumnDefaults, tabbedLayout, titleView, toHtml, verticalLayout)

import Core
import Html.Styled as Html exposing (Html)
import Layout.Horizontal as Horizontal
import Layout.Tabbed as Tabbed
import Layout.Vertical as Vertical
import Theme exposing (Theme)
import Translation exposing (Translation)
import View.Sheet as Sheet
import View.Title as Title



-- MAIN


type alias TitleViewProps =
    { title : String
    , subTitle : String
    }


titleViewDefaults : TitleViewProps
titleViewDefaults =
    { title = ""
    , subTitle = ""
    }


titleView : (TitleViewProps -> TitleViewProps) -> Core.View msg
titleView makeProps =
    let
        props =
            makeProps titleViewDefaults
    in
    Core.TitleView props.title props.subTitle


sheetViewDefaults : Core.Sheet msg
sheetViewDefaults =
    { columns = []
    , rows = []
    , inserts = []
    }


sheetView : (Core.Sheet msg -> Core.Sheet msg) -> Core.View msg
sheetView makeProps =
    Core.SheetView (makeProps sheetViewDefaults)


type alias TabbedLayoutProps msg =
    { tabs : List ( String, Bool )
    , view : Maybe (Core.View msg)
    }


tabbedLayoutDefaults : TabbedLayoutProps msg
tabbedLayoutDefaults =
    { tabs = []
    , view = Nothing
    }


tabbedLayout : (TabbedLayoutProps msg -> TabbedLayoutProps msg) -> Core.View msg
tabbedLayout makeProps =
    let
        props =
            makeProps tabbedLayoutDefaults
    in
    Core.TabbedLayout props.tabs props.view


type alias LayoutProps msg =
    { children : List (Core.View msg)
    }


layoutDefaults : LayoutProps msg
layoutDefaults =
    { children = []
    }


verticalLayout : (LayoutProps msg -> LayoutProps msg) -> Core.View msg
verticalLayout makeProps =
    let
        props =
            makeProps layoutDefaults
    in
    Core.VerticalLayout props.children


horizontalLayout : (LayoutProps msg -> LayoutProps msg) -> Core.View msg
horizontalLayout makeProps =
    let
        props =
            makeProps layoutDefaults
    in
    Core.HorizontalLayout props.children


toHtml : Core.Meta -> Core.View msg -> Html msg
toHtml meta view =
    case view of
        Core.TitleView title subtitle ->
            Title.view meta title subtitle

        Core.SheetView sheet ->
            Sheet.view (toHtml meta) sheet

        Core.TabbedLayout tabs activeView ->
            Tabbed.layout (toHtml meta) tabs activeView

        Core.VerticalLayout children ->
            Vertical.layout (List.map (toHtml meta) children)

        Core.HorizontalLayout children ->
            Horizontal.layout (List.map (toHtml meta) children)


type alias StringColumnArgs msg =
    { length : Maybe Int
    , order : Maybe Core.Order
    , onSetOrder : Maybe msg
    , currentFilter : Maybe String
    , onFilter : Maybe (String -> msg)
    }


type alias StringColumnProps msg =
    { title : String
    , length : Maybe Int
    , currentFilter : String
    , onFilter : Maybe (String -> msg)
    , order : Maybe Core.Order
    , onSetOrder : Maybe msg
    }


stringColumnDefaults : StringColumnProps msg
stringColumnDefaults =
    { title = ""
    , length = Nothing
    , currentFilter = ""
    , onFilter = Nothing
    , order = Nothing
    , onSetOrder = Nothing
    }


stringColumn : (StringColumnProps msg -> StringColumnProps msg) -> Core.Column msg
stringColumn makeProps =
    let
        props =
            makeProps stringColumnDefaults
    in
    { title = props.title
    , type_ =
        Core.String
            { length = props.length
            , currentFilter = props.currentFilter
            , onFilter = props.onFilter
            }
    , order = props.order
    , onSetOrder = props.onSetOrder
    }


type alias ActiveRowProps msg =
    { data : List String
    , children : List (Core.View msg)
    , onEdit : Maybe msg
    , onDelete : Maybe msg
    , onClose : Maybe msg
    , id : Maybe String
    }


activeRowDefaults : ActiveRowProps msg
activeRowDefaults =
    { data = []
    , children = []
    , onEdit = Nothing
    , onDelete = Nothing
    , onClose = Nothing
    , id = Nothing
    }


activeRow : (ActiveRowProps msg -> ActiveRowProps msg) -> Core.Row msg
activeRow makeProps =
    let
        props =
            makeProps activeRowDefaults
    in
    { data = props.data
    , state =
        Core.Active
            { children = props.children
            , onEdit = props.onEdit
            , onDelete = props.onDelete
            , onClose = props.onClose
            }
    , id = Maybe.withDefault (String.concat props.data) props.id
    }


type alias View msg =
    Core.View msg
