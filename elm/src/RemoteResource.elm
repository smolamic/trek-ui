{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module RemoteResource exposing (RemoteResource(..), encodeWithDefault, filterMap, filterUpdate, isCached, load, map, toMaybe, update)

import Json.Encode as E


type RemoteResource a
    = Cached a
    | Loading
    | Reloading a
    | Missing


load : RemoteResource a -> RemoteResource a
load res =
    case res of
        Cached data ->
            Reloading data

        Loading ->
            Loading

        Reloading data ->
            Reloading data

        Missing ->
            Loading


toMaybe : RemoteResource a -> Maybe a
toMaybe res =
    case res of
        Cached data ->
            Just data

        Loading ->
            Nothing

        Reloading data ->
            Just data

        Missing ->
            Nothing


isCached : RemoteResource a -> Bool
isCached res =
    case res of
        Cached _ ->
            True

        Loading ->
            False

        Reloading _ ->
            True

        Missing ->
            False


update : (a -> a) -> RemoteResource a -> RemoteResource a
update =
    map


filterUpdate : (a -> Maybe a) -> RemoteResource a -> RemoteResource a
filterUpdate =
    filterMap


map : (a -> b) -> RemoteResource a -> RemoteResource b
map method old =
    case old of
        Cached val ->
            Cached (method val)

        Loading ->
            Loading

        Reloading val ->
            Reloading (method val)

        Missing ->
            Missing


filterMap : (a -> Maybe b) -> RemoteResource a -> RemoteResource b
filterMap method old =
    case old of
        Cached val ->
            case method val of
                Just new ->
                    Cached new

                Nothing ->
                    Missing

        Loading ->
            Loading

        Reloading val ->
            case method val of
                Just new ->
                    Reloading new

                Nothing ->
                    Loading

        Missing ->
            Missing


encodeWithDefault : (a -> E.Value) -> a -> RemoteResource a -> E.Value
encodeWithDefault inner default =
    toMaybe
        >> Maybe.withDefault default
        >> inner
