{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Log exposing (clear, init, latest, list, logResult, pushErr, pushOut, stdErr, stdOut, update, view)

import Array
import Components exposing (heading)
import Core exposing (setLog)
import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Log.Core exposing (..)
import Translation.Core exposing (trText)


init : ( Log, Cmd Core.Msg )
init =
    ( Array.empty, Cmd.none )


update : Msg -> Core.Model -> ( Core.Model, Cmd Core.Msg )
update msg model =
    let
        log =
            model.log
    in
    case msg of
        StdErr message ->
            ( setLog (Array.push (Error message) log) model, Cmd.none )

        StdOut message ->
            ( setLog (Array.push (Output message) log) model, Cmd.none )

        Clear ->
            ( setLog Array.empty model, Cmd.none )


stdOut : String -> Core.Model -> ( Core.Model, Cmd Core.Msg )
stdOut message model =
    ( setLog (Array.push (Output message) model.log) model, Cmd.none )


stdErr : String -> Core.Model -> ( Core.Model, Cmd Core.Msg )
stdErr message model =
    ( setLog (Array.push (Error message) model.log) model, Cmd.none )


clear : Core.Model -> ( Core.Model, Cmd Core.Msg )
clear model =
    ( setLog Array.empty model, Cmd.none )


view : Core.Model -> Html Core.Msg
view model =
    Html.div []
        [ heading 3 [ trText model.translation "Log:" ]
        , Html.div [] (Array.map (logEntry model) model.log |> Array.toList)
        ]


list : Core.Model -> List (Html Core.Msg)
list model =
    if Array.isEmpty model.log then
        [ logEntry model (Output "No log-entries yet.") ]

    else
        Array.map (logEntry model) model.log |> Array.toList


latest : Core.Model -> Html Core.Msg
latest model =
    case Array.get (Array.length model.log - 1) model.log of
        Just (Output message) ->
            Html.span [] [ trText model.translation message ]

        Just (Error message) ->
            Html.span [ Attr.css [ Css.color model.config.theme.danger ] ]
                [ trText model.translation message ]

        Nothing ->
            Html.text ""


logEntry : Core.Model -> LogEntry -> Html Core.Msg
logEntry model entry =
    let
        commonStyle =
            [ Css.padding (Css.rem 1)
            , Css.overflowX Css.auto
            , Css.flexShrink (Css.num 0)
            ]
    in
    case entry of
        Output message ->
            Html.div [ Attr.css (Css.backgroundColor model.config.theme.secondary.light :: commonStyle) ]
                [ Html.pre [] [ trText model.translation message ] ]

        Error message ->
            Html.div [ Attr.css (Css.backgroundColor model.config.theme.danger :: commonStyle) ]
                [ Html.pre [] [ trText model.translation message ] ]


pushOut : String -> Log -> Log
pushOut message =
    Array.push (Output message)


pushErr : String -> Log -> Log
pushErr message =
    Array.push (Error message)


logResult : Result String a -> Log -> Log
logResult result prev =
    case result of
        Ok _ ->
            prev

        Err message ->
            Array.push (Error message) prev
