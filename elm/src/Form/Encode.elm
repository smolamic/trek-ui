{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Form.Encode exposing (rowInput)

import Form.Core exposing (..)
import Input.Encode
import Json.Encode exposing (..)
import RemoteResource exposing (RemoteResource)


rowInput : RemoteResource Row -> Json.Encode.Value
rowInput row =
    case row of
        RemoteResource.Cached row_ ->
            dict identity Input.Encode.value row_.input

        RemoteResource.Reloading row_ ->
            dict identity Input.Encode.value row_.input

        RemoteResource.Loading ->
            object []

        RemoteResource.Missing ->
            object []
