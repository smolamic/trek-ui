{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Form.Decode exposing (row, table)

import Dict exposing (Dict)
import Enum.DataType as DataType
import Enum.Decode exposing (dataType)
import Form.Core exposing (..)
import Input.Core exposing (Variant)
import Input.Decode
import Json.Decode exposing (..)
import Json.Decode.Extra exposing (..)


table : Decoder Table
table =
    map5 Table
        (field "path" string)
        (field "title" string)
        (field "read" bool)
        (field "insert" bool)
        (field "columns" (list column))


variants : Decoder (List Variant)
variants =
    list string
        |> map (List.indexedMap Variant)


column : Decoder Column
column =
    map9 Column
        (field "name" string)
        (field "title" string)
        (field "datatype" dataType)
        (field "unit" (nullable string))
        (field "variants" (nullable variants))
        (field "step" (nullable string))
        (field "required" bool)
        (field "readonly" bool)
        (field "ref" bool)


inputValue : Column -> Decoder Input.Core.Value
inputValue column_ =
    if column_.ref then
        Input.Decode.refValue

    else
        case column_.datatype of
            DataType.String ->
                Input.Decode.stringValue

            DataType.Bool ->
                Input.Decode.boolValue

            DataType.Check ->
                Input.Decode.checkValue

            DataType.Barcode ->
                Input.Decode.barcodeValue

            _ ->
                Input.Decode.basicValue


input : List Column -> Decoder (Dict String Input.Core.Value)
input =
    List.foldl
        (\col res ->
            map2 (Dict.insert col.name)
                (field col.name (inputValue col))
                res
        )
        (succeed Dict.empty)


output : Decoder (List String)
output =
    list string


row : List Column -> Decoder Row
row columns =
    map2 Row
        (field "input" (input columns))
        (field "output" output)
