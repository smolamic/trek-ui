{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Form.Core exposing (Column, Form, Msg(..), Row, State(..), Table)

import Browser.Dom as Dom
import Dict exposing (Dict)
import Enum.DataType exposing (DataType)
import Input.Core exposing (Variant)
import Json.Decode as D
import Json.Encode as E
import RemoteResource exposing (RemoteResource)
import RemoteResource.Counter exposing (ResourceCounter)
import StateCounter exposing (StateCounter)


type alias Column =
    { name : String
    , title : String
    , datatype : DataType
    , unit : Maybe String
    , variants : Maybe (List Variant)
    , step : Maybe String
    , required : Bool
    , readonly : Bool
    , ref : Bool
    }


type alias Table =
    { path : String
    , title : String
    , read : Bool
    , insert : Bool
    , columns : List Column
    }


type alias Row =
    { input : Dict String Input.Core.Value
    , output : List String
    }


type State
    = Valid
    | Invalid
    | Saving
    | Loading


type alias Form =
    { path : String
    , table : Table
    , id : Maybe Int
    , row : ResourceCounter Row
    , state : State
    , checkRequired : Bool
    , activeInput : Maybe String
    , activeOption : Int
    , focused : Maybe String
    , hovered : Maybe String
    }


type Msg
    = Run String Int
    | GotRow String Int Row
    | RunFailed String Int
    | Save
    | SaveSucceeded String
    | SaveFailed String
    | SaveInvalid String
    | PathChanged
    | Focus String
    | Blur String
    | MouseOver String
    | MouseOut String
    | Input String Input.Core.Value
    | Accept String Input.Core.Value
    | Select String Int
    | FocusFailed String
