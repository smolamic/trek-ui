{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Database exposing (Model, Msg, update, view)

import Api
import Dict exposing (Dict)
import Html exposing (Html, div)
import Json.Decode as D


type Msg
    = GoTo String
    | GotView String View
    | Log String


type View
    = HomeView { path : String }
    | DatabaseView { path : String }
    | SheetView { path : String }


type alias Model =
    { views : Dict String View
    }


update : Api.User -> Msg -> Model -> ( Model, Cmd )
update user msg model =
    case msg of
        GoTo path ->
            ( model
            , Api.get user path handleSwitchView viewDecoder
            )

        GotView view ->
            ( { model | views = Dict.insert view.path view model.views }, Cmd.none )

        Log message ->
            ( model, Cmd.none )


view : Model -> Html Msg
view model =
    div [] []


handleSwitchView : Api.Response View -> Msg
handleSwitchView result =
    case result of
        Api.Success view ->
            GotView view

        Api.BackendError error ->
            Log error.message

        Api.ApiError error ->
            Log error.message


viewDecoder : D.Decoder View
viewDecoder =
    D.field "viewclass" ViewClass.viewClass
        |> D.andThen
            (\viewClass ->
                case viewClass of
                    ViewClass.Home ->
                        homeViewDecoder

                    ViewClass.Database ->
                        databaseViewDecoder

                    ViewClass.Sheet ->
                        sheetViewDecoder
            )


homeViewDecoder : D.Decoder View
homeViewDecoder =
    D.field "path" D.string


databaseViewDecoder : D.Decoder View
databaseViewDecoder =
    D.field "path" D.string


sheetViewDecoder : D.Decoder View
sheetViewDecoder =
    D.field "path" D.string
