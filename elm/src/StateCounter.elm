{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module StateCounter exposing (StateCounter, count, inc, is, map, new, unwrap, update)


type alias StateCounter state =
    { count : Int
    , state : state
    }


new : state -> StateCounter state
new =
    StateCounter 0


inc : (a -> b) -> StateCounter a -> StateCounter b
inc method sc =
    StateCounter (sc.count + 1) (method sc.state)


update : Int -> (a -> a) -> StateCounter a -> StateCounter a
update count_ method sc =
    if is count_ sc then
        inc method sc

    else
        sc


map : (a -> b) -> StateCounter a -> StateCounter b
map method sc =
    StateCounter sc.count (method sc.state)


is : Int -> StateCounter state -> Bool
is count_ sc =
    count_ == sc.count


unwrap : StateCounter state -> state
unwrap =
    .state


count : StateCounter state -> Int
count =
    .count
