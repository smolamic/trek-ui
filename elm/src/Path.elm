{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Path exposing (dropRight, fromRight, slice)


fromRight : Int -> String -> String
fromRight count path =
    let
        list =
            String.split "/" path

        len =
            List.length list
    in
    List.drop (len - count) list
        |> String.join "/"
        |> String.append "/"


dropRight : Int -> String -> String
dropRight count path =
    let
        list =
            String.split "/" path

        len =
            List.length list
    in
    List.take (len - count) list
        |> String.join "/"


slice : Int -> Int -> String -> String
slice index count path =
    let
        list =
            String.split "/" path

        len =
            List.length list
    in
    (if index < 0 then
        List.drop (len + index) list

     else
        List.drop (index + 1) list
    )
        |> List.take count
        |> String.join "/"
        |> String.append "/"
