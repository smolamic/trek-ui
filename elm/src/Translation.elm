{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Translation exposing (Translation, noTranslation)

import Html.Styled as Html exposing (Html)


type alias Translation =
    { get : String -> String
    }


noTranslation : Translation
noTranslation =
    { get = identity
    }


trText : Translation -> String -> Html msg
trText translation =
    translation.get >> Html.text


translate1 : Translation -> (String -> ret) -> String -> ret
translate1 translation method =
    translation.get >> method


translate2 : Translation -> (String -> String -> ret) -> String -> String -> ret
translate2 translation method arg1 arg2 =
    method (translation.get arg1) (translation.get arg2)


translate3 : Translation -> (String -> String -> String -> ret) -> String -> String -> String -> ret
translate3 translation method arg1 arg2 arg3 =
    method (translation.get arg1) (translation.get arg2) (translation.get arg3)
