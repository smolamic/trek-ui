{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Api.Decode exposing (ignore, response, responseWithErrorData)

import Api.Core exposing (..)
import Enum.Decode exposing (errorCode)
import Json.Decode exposing (..)


errorWithData : Decoder errorData -> Decoder (Response data errorData)
errorWithData errorData =
    map6 makeBackendError
        (field "code" errorCode)
        (field "message" string)
        (field "file" string)
        (field "line" int)
        (field "trace" string)
        (field "data" errorData)


error : Decoder (Response data ())
error =
    map6 makeBackendError
        (field "code" errorCode)
        (field "message" string)
        (field "file" string)
        (field "line" int)
        (field "trace" string)
        ignore


response : Decoder data -> Decoder (Response data ())
response payload =
    field "success" bool
        |> andThen
            (\success ->
                if success then
                    field "payload" (map Success payload)

                else
                    field "error" error
            )


responseWithErrorData : Decoder data -> Decoder errorData -> Decoder (Response data errorData)
responseWithErrorData payload errorData =
    field "success" bool
        |> andThen
            (\success ->
                if success then
                    field "payload" (map Success payload)

                else
                    field "error" (errorWithData errorData)
            )


ignore : Decoder ()
ignore =
    succeed ()
