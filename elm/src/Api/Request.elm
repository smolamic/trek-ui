{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Api.Request exposing (authenticate, check, config, delete, index, password, reconcile, reconcileBatch, run, save, token, translation, unlock, view)

import Api.Core exposing (..)
import Api.Decode exposing (ignore)
import Base64
import Cache
import Config.Decode
import Core
import Dict exposing (Dict)
import Enum.ErrorCode as ErrorCode
import Form.Core exposing (Column, Form)
import Form.Decode
import Form.Encode
import Http
import Input.Decode
import Json.Decode as D exposing (Decoder)
import Json.Encode as E
import Main.Core exposing (User)
import Main.Decode
import Main.Encode
import RemoteResource
import RemoteResource.Counter exposing (ResourceCounter)
import StateCounter
import StateTimer exposing (StateTimer)
import Translation.Decode
import Url.Builder
import Views.Core exposing (Filter, Order, Views)
import Views.Decode
import Views.Encode
import Views.Select


apiUrl =
    "/api/"


mapError : String -> (Response data errorData -> msg) -> Result Http.Error (Response data errorData) -> msg
mapError url handle result =
    handle
        (case result of
            Ok response ->
                response

            Err error ->
                case error of
                    Http.BadBody message ->
                        makeApiError ErrorCode.BadBody message "Api.elm"

                    Http.BadUrl message ->
                        makeApiError ErrorCode.BadUrl message "Api.elm"

                    Http.Timeout ->
                        makeApiError ErrorCode.Timeout "Timeout" "Api.elm"

                    Http.NetworkError ->
                        makeApiError ErrorCode.NetworkError "Network error" "Api.elm"

                    Http.BadStatus code ->
                        case code of
                            401 ->
                                makeApiError ErrorCode.AccessDenied "Unauthorized" "Api.elm"

                            404 ->
                                makeApiError ErrorCode.ServerNotFound ("Server not found under URL \"" ++ url ++ "\".") "Api.elm"

                            otherCode ->
                                makeApiError ErrorCode.GenericServerError ("Generic server error: http error code " ++ String.fromInt otherCode) "Api.elm"
        )


authenticate : String -> String -> Cmd Core.Msg
authenticate username password_ =
    Http.post
        { url = apiUrl ++ "authenticate"
        , body =
            Http.jsonBody
                (E.object
                    [ ( "username", E.string username )
                    , ( "password", E.string password_ )
                    ]
                )
        , expect =
            Http.expectJson
                (mapError (apiUrl ++ "authenticate") (Core.ApiMsg << GotAuthenticateResponse))
                (Api.Decode.response Main.Decode.user)
        }


config : Cmd Core.Msg
config =
    Http.get
        { url = apiUrl ++ "config"
        , expect =
            Http.expectJson
                (mapError (apiUrl ++ "config") (Core.ApiMsg << GotConfigResponse))
                (Api.Decode.response Config.Decode.config)
        }


translation : String -> Cmd Core.Msg
translation locale =
    Http.get
        { url = apiUrl ++ "translation/" ++ locale
        , expect =
            Http.expectJson
                (mapError (apiUrl ++ "translation/" ++ locale) (Core.ApiMsg << GotTranslationResponse))
                (Api.Decode.response Translation.Decode.translation)
        }


request : String -> User -> String -> Http.Body -> (Response data errorData -> Msg) -> Decoder (Response data errorData) -> Cmd Core.Msg
request method user url body handler decoder =
    Http.request
        { method = method
        , url = url
        , headers = [ Http.header "Authorization" ("Basic " ++ Base64.encode (user.id ++ ":" ++ user.token)) ]
        , timeout = Nothing
        , tracker = Nothing
        , expect = Http.expectJson (mapError url (Core.ApiMsg << handler)) decoder
        , body = body
        }


index : User -> Int -> Cmd Core.Msg
index user count =
    request
        "GET"
        user
        (apiUrl ++ "view/")
        Http.emptyBody
        (GotIndexResponse count)
        (Api.Decode.response Main.Decode.index)


buildViewUrl : String -> Maybe Order -> Maybe (Dict String Filter) -> String
buildViewUrl path order filters =
    Url.Builder.relative [ path ]
        (List.append
            (case order of
                Just order_ ->
                    [ Url.Builder.string "order_by" (E.encode 0 (E.string order_.by))
                    , Url.Builder.string "order_asc" (E.encode 0 (E.bool order_.asc))
                    ]

                Nothing ->
                    []
            )
            (case filters of
                Just filters_ ->
                    [ Url.Builder.string "where" (E.encode 0 (Views.Encode.filters filters_)) ]

                Nothing ->
                    []
            )
        )


view : User -> String -> Views -> Cmd Core.Msg
view user path views =
    let
        time =
            Cache.time path views.cache

        order =
            Views.Select.order path views

        filters =
            Views.Select.filters path views
    in
    request
        "GET"
        user
        (buildViewUrl (apiUrl ++ "view" ++ path) order filters)
        Http.emptyBody
        (GotViewResponse path time)
        (Api.Decode.response Views.Decode.view)


delete : User -> String -> Cmd Core.Msg
delete user path =
    request
        "DELETE"
        user
        (apiUrl ++ "delete" ++ path)
        Http.emptyBody
        (GotDeleteResponse path)
        (Api.Decode.response D.string)


reconcile : User -> String -> Cmd Core.Msg
reconcile user path =
    request
        "PATCH"
        user
        (apiUrl ++ "reconcile" ++ path)
        Http.emptyBody
        (GotReconcileResponse path)
        (Api.Decode.response D.string)


reconcileBatch : User -> List String -> Cmd Core.Msg
reconcileBatch user rows =
    case rows of
        path :: rest ->
            request
                "PATCH"
                user
                (apiUrl ++ "reconcile" ++ path)
                Http.emptyBody
                (GotReconcileBatchResponse user path rest)
                (Api.Decode.response D.string)

        _ ->
            Cmd.none


buildRunUrl : String -> List Column -> Maybe String -> Bool -> String
buildRunUrl path columns maybeActive checkRequired =
    Url.Builder.relative [ path ]
        (List.append
            [ Url.Builder.string "columns" (E.encode 0 (E.list E.string (List.map .name columns)))
            , Url.Builder.string "required"
                (if checkRequired then
                    "true"

                 else
                    "false"
                )
            ]
            (case maybeActive of
                Just active ->
                    [ Url.Builder.string "active" (E.encode 0 (E.string active)) ]

                Nothing ->
                    []
            )
        )


run : User -> Form -> Cmd Core.Msg
run user form =
    request
        "POST"
        user
        (buildRunUrl (apiUrl ++ "run" ++ form.path) form.table.columns form.activeInput form.checkRequired)
        (Http.jsonBody (Form.Encode.rowInput (StateCounter.unwrap form.row)))
        (GotRunResponse form.path (StateCounter.count form.row))
        (Api.Decode.response (Form.Decode.row form.table.columns))


unlock : User -> Cmd Core.Msg
unlock user =
    request
        "PATCH"
        user
        (apiUrl ++ "unlock")
        Http.emptyBody
        GotUnlockResponse
        (Api.Decode.response ignore)


save : User -> Form -> Cmd Core.Msg
save user form =
    request
        "POST"
        user
        (apiUrl ++ "save" ++ form.path)
        (Http.jsonBody (Form.Encode.rowInput (StateCounter.unwrap form.row)))
        (GotSaveResponse form.path)
        (Api.Decode.response (D.map2 Tuple.pair (D.field "view" D.string) (D.field "row" D.string)))


check : User -> String -> Views -> String -> Bool -> Cmd Core.Msg
check user viewPath views path checked =
    let
        time =
            Cache.time viewPath views.cache

        order =
            Views.Select.order viewPath views

        filters =
            Views.Select.filters viewPath views
    in
    request
        "POST"
        user
        (buildViewUrl (apiUrl ++ "check" ++ path) order filters)
        (Http.jsonBody (E.bool checked))
        (GotCheckResponse viewPath time)
        (Api.Decode.response Views.Decode.view)


password : User -> ResourceCounter Main.Core.Password -> Cmd Core.Msg
password user password_ =
    let
        count =
            StateCounter.count password_
    in
    request
        "POST"
        user
        (apiUrl ++ "password")
        (Http.jsonBody (Main.Encode.passwordResource password_))
        (GotPasswordResponse count)
        (Api.Decode.response Main.Decode.password)


token : StateTimer User -> Cmd Core.Msg
token userTimer =
    let
        time =
            StateTimer.time userTimer

        user =
            StateTimer.unwrap userTimer
    in
    request
        "GET"
        user
        (apiUrl ++ "token")
        Http.emptyBody
        (GotTokenResponse time)
        (Api.Decode.response Main.Decode.user)


encodeParameters : List ( String, E.Value ) -> String
encodeParameters parameters =
    List.map
        (\( key, value ) ->
            Url.Builder.string key (E.encode 0 value)
        )
        parameters
        |> Url.Builder.toQuery
