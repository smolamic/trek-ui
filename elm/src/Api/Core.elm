{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Api.Core exposing (Msg(..), Response(..), makeApiError, makeBackendError)

import Config.Core exposing (Config)
import Enum.ErrorCode exposing (ErrorCode)
import Form.Core
import Json.Decode as D
import Main.Core exposing (Index, Password, User)
import Time exposing (Posix)
import Translation.Core exposing (Translation)
import Views.Core exposing (View)


type Msg
    = GotAuthenticateResponse (Response User ())
    | GotConfigResponse (Response Config ())
    | GotTranslationResponse (Response Translation ())
    | GotIndexResponse Int (Response Index ())
    | GotViewResponse String Posix (Response View ())
    | GotDeleteResponse String (Response String ())
    | GotReconcileResponse String (Response String ())
    | GotReconcileBatchResponse User String (List String) (Response String ())
    | ReconcileBatch User (List String)
    | GotRunResponse String Int (Response Form.Core.Row ())
    | GotUnlockResponse (Response () ())
    | GotSaveResponse String (Response ( String, String ) ())
    | GotCheckResponse String Posix (Response View ())
    | GotPasswordResponse Int (Response Password ())
    | GotTokenResponse Posix (Response User ())


type Response data errorData
    = Success data
    | BackendError
        { code : ErrorCode
        , message : String
        , file : String
        , line : Int
        , trace : String
        , data : errorData
        }
    | ApiError
        { code : ErrorCode
        , message : String
        , file : String
        }


makeBackendError : ErrorCode -> String -> String -> Int -> String -> errorData -> Response data errorData
makeBackendError code message file line trace data =
    BackendError
        { code = code
        , message = message
        , file = file
        , line = line
        , trace = trace
        , data = data
        }


makeApiError : ErrorCode -> String -> String -> Response data errorData
makeApiError code message file =
    ApiError
        { code = code
        , message = message
        , file = file
        }
