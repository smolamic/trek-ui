module Trek.Props exposing (columns, data, length, rows, title)

import Core


title : String -> { a | title : String } -> { a | title : String }
title text rest =
    { rest | title = text }


subTitle : String -> { a | subTitle : String } -> { a | subTitle : String }
subTitle text rest =
    { rest | subTitle = text }


length : Int -> { a | length : Maybe Int } -> { a | length : Maybe Int }
length value rest =
    { rest | length = Just value }


type alias Columns msg =
    List (Core.Column msg)


columns : Columns msg -> { a | columns : Columns msg } -> { a | columns : Columns msg }
columns value rest =
    { rest | columns = value }


type alias Rows msg =
    List (Core.Row msg)


rows : Rows msg -> { a | rows : Rows msg } -> { a | rows : Rows msg }
rows value rest =
    { rest | rows = value }


data : List String -> { a | data : List String } -> { a | data : List String }
data value rest =
    { rest | data = value }


id : String -> { a | id : Maybe String } -> { a | id : Maybe String }
id value rest =
    { rest | id = Just value }
