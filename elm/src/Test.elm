{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Test exposing (view)

-- TODO remove Api.Request, dev dependency

import Api
import Api.Request
import Browser
import Browser.Navigation as Nav
import Config
import Core exposing (..)
import Html.Styled as Html
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Http
import Json.Decode as D
import Log
import Login
import Main
import Main.Core
import Translation
import Update
import Url exposing (Url)



-- MAIN


main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        , onUrlRequest = \_ -> NoMsg
        , onUrlChange = \_ -> NoMsg
        }


type Model
    = NoModel


init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init _ url key =
    ( NoModel, Cmd.none )


type Msg
    = NoMsg


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    ( model, Cmd.none )



--{-| initialize from sub-initializers
---}
--init : () -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
--init _ url key =
--    let
--        ( login, loginCmd ) =
--            Login.init
--
--        ( config, configCmd ) =
--            Config.init
--
--        ( translation, translationCmd ) =
--            Translation.init
--
--        ( log, logCmd ) =
--            Log.init
--    in
--    ( { page = LoginPage login
--      , config = config
--      , translation = translation
--      , log = log
--      , url = url
--      , key = key
--      }
--      --, Cmd.none
--    , Cmd.batch
--        [ loginCmd
--        , configCmd
--        , translationCmd
--        , logCmd
--        , Api.Request.authenticate "admin" "warzenschwein"
--        ]
--    )
--
--
--
---- UPDATE
--
--
--{-| Main Update aggregating from Login.Msg, Database.Msg, etc.
---}
--update : Msg -> Model -> ( Model, Cmd Msg )
--update msg model =
--    case msg of
--        ApiMsg apiMsg ->
--            Api.update apiMsg model
--
--        LoginMsg loginMsg ->
--            Login.update loginMsg model
--
--        MainMsg mainMsg ->
--            Main.update mainMsg model
--
--        ConfigMsg configMsg ->
--            Config.update configMsg model
--
--        TranslationMsg translationMsg ->
--            Translation.update translationMsg model
--
--        LogMsg logMsg ->
--            Log.update logMsg model
--
--        LinkClicked urlRequest ->
--            case urlRequest of
--                Browser.Internal url ->
--                    ( model, Nav.pushUrl model.key (Url.toString url) )
--
--                Browser.External href ->
--                    ( model, Nav.load href )
--
--        UrlChanged url ->
--            ( { model | url = url }, Cmd.none )
--                |> Update.andThen (Main.update Main.Core.PathChanged)
--
--
--
-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none



---- VIEW
--
--
--{-| Main View
---}


view : Model -> Browser.Document Msg
view model =
    Browser.Document
        "Trek"
        [ Html.toUnstyled
            (Html.div
                []
                []
            )

        --(Html.main_ []
        --    [ case model.page of
        --        LoginPage login ->
        --            Login.view model login
        --        MainPage main_ ->
        --            Main.view model main_
        --    ]
        --)
        ]
