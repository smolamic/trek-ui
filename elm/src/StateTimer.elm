{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module StateTimer exposing (StateTimer, age, decoder, encoder, map, new, time, unwrap, update)

import Json.Decode as D
import Json.Encode as E
import Time exposing (Posix)


type alias StateTimer state =
    { time : Posix
    , state : state
    }


new : Posix -> state -> StateTimer state
new =
    StateTimer


update : Posix -> (a -> a) -> StateTimer a -> StateTimer a
update time_ method timer =
    if Time.posixToMillis time_ >= Time.posixToMillis timer.time then
        StateTimer time_ (method timer.state)

    else
        timer


map : (a -> b) -> StateTimer a -> StateTimer b
map method timer =
    StateTimer timer.time (method timer.state)


age : Posix -> StateTimer state -> Int
age time_ timer =
    Time.posixToMillis time_ - Time.posixToMillis timer.time


unwrap : StateTimer state -> state
unwrap =
    .state


time : StateTimer state -> Posix
time =
    .time


encoder : (state -> E.Value) -> StateTimer state -> E.Value
encoder state st =
    E.object
        [ ( "time", E.int (Time.posixToMillis st.time) )
        , ( "state", state st.state )
        ]


decoder : D.Decoder state -> D.Decoder (StateTimer state)
decoder state =
    D.map2 StateTimer
        (D.field "time" D.int |> D.map Time.millisToPosix)
        (D.field "state" state)
