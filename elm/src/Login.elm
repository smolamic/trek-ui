{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Login exposing (init, update, view, withUsername)

import Api.Request
import Breakpoints.Select as Bp
import Components
import Config.Core exposing (Theme)
import Core exposing (setLogin)
import Css exposing (Style)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Json.Decode as D
import Json.Encode as E
import Log
import Login.Core exposing (..)
import Translation.Core exposing (tr)



-- MODEL


init : ( Login, Cmd Core.Msg )
init =
    ( { username = ""
      , password = ""
      , state = Idle
      }
    , Cmd.none
    )


withUsername : String -> ( Login, Cmd Core.Msg )
withUsername username =
    ( { username = username
      , password = ""
      , state = Idle
      }
    , Cmd.none
    )


authenticate : Core.Msg
authenticate =
    Core.LoginMsg Authenticate


setUsername : String -> Core.Msg
setUsername =
    Core.LoginMsg << SetUsername


setPassword : String -> Core.Msg
setPassword =
    Core.LoginMsg << SetPassword



-- UPDATE


update : Msg -> Core.Model -> ( Core.Model, Cmd Core.Msg )
update msg model =
    case model.page of
        Core.MainPage _ ->
            ( model, Cmd.none )

        Core.LoginPage login ->
            case msg of
                SetUsername username ->
                    ( setLogin { login | username = username } model, Cmd.none )

                SetPassword password ->
                    ( setLogin { login | password = password } model, Cmd.none )

                Authenticate ->
                    ( setLogin { login | state = Busy } model
                    , Api.Request.authenticate login.username login.password
                    )

                ResetPassword ->
                    ( setLogin { login | password = "", state = Idle } model, Cmd.none )

                AuthenticationFailed ->
                    ( setLogin { login | state = Idle } model, Cmd.none )



-- VIEW


inputStyle : Theme -> Style
inputStyle theme =
    Css.batch
        [ Css.border3 (Css.px 1) Css.solid theme.black
        , Css.focus [ Css.borderColor theme.secondary.light ]
        , Css.padding (Css.rem 0.5)
        , Css.margin2 (Css.rem 0.5) (Css.rem 0.2)
        , Css.width (Css.pct 100)
        , Css.boxSizing Css.borderBox
        ]


loginForm : Core.Model -> String -> String -> Html Core.Msg
loginForm model username password =
    Html.form [ Evt.preventDefaultOn "submit" (D.succeed ( authenticate, True )) ]
        [ Html.div []
            [ Html.input
                [ Attr.css [ inputStyle model.config.theme ]
                , Attr.type_ "text"
                , Attr.value username
                , Evt.onInput setUsername
                , Attr.placeholder (tr model.translation "Username")
                ]
                []
            ]
        , Html.div []
            [ Html.input
                [ Attr.css [ inputStyle model.config.theme ]
                , Attr.type_ "password"
                , Attr.value password
                , Evt.onInput setPassword
                , Attr.placeholder (tr model.translation "Password")
                ]
                []
            ]
        , Components.whiteFilledSubmitButton model
            (username /= "" && password /= "")
            "submit-login"
            [ Html.text "Log in" ]
        ]


view : Core.Model -> Login -> Html Core.Msg
view model login =
    let
        theme =
            model.config.theme
    in
    if Bp.desktop model then
        Html.section [ Attr.css [ Css.height (Css.pct 100) ] ]
            [ Html.div
                [ Attr.css
                    [ Css.width (Css.pct 100)
                    , Css.height (Css.pct 100)
                    , Css.backgroundColor theme.primary.medium
                    , Css.padding (Css.rem 2)
                    , Css.displayFlex
                    , Css.flexDirection Css.row
                    , Css.justifyContent Css.spaceBetween
                    , Css.alignItems Css.center
                    , Css.flexWrap Css.wrap
                    ]
                ]
                [ Html.div
                    [ Attr.css
                        [ Css.fontSize (Css.vw 20)
                        , Css.width (Css.vw 50)
                        ]
                    ]
                    [ Html.node "trek-html" [ Attr.property "content" (E.string model.config.logo) ] []
                    , Html.div [ Attr.css [ Css.fontSize (Css.rem 2) ] ] [ Log.latest model ]
                    ]
                , Html.div
                    [ Attr.css
                        [ Css.displayFlex
                        , Css.flexDirection Css.column
                        , Css.width (Css.rem 15)
                        , Css.position Css.relative
                        , Css.boxSizing Css.borderBox
                        ]
                    ]
                    [ loginForm model login.username login.password ]
                ]
            ]

    else
        Html.section [ Attr.css [ Css.height (Css.pct 100) ] ]
            [ Html.div
                [ Attr.css
                    [ Css.width (Css.pct 100)
                    , Css.height (Css.pct 100)
                    , Css.backgroundColor theme.primary.medium
                    , Css.padding (Css.rem 0.5)
                    , Css.displayFlex
                    , Css.flexDirection Css.column
                    , Css.justifyContent Css.spaceAround
                    , Css.alignItems Css.center
                    ]
                ]
                [ Html.div
                    [ Attr.css
                        [ Css.fontSize (Css.vw 20)
                        , Css.width (Css.pct 90)
                        ]
                    ]
                    [ Html.node "trek-html" [ Attr.property "content" (E.string model.config.logo) ] [] ]
                , Html.div
                    [ Attr.css
                        [ Css.displayFlex
                        , Css.flexDirection Css.column
                        , Css.width (Css.pct 90)
                        , Css.position Css.relative
                        , Css.boxSizing Css.borderBox
                        ]
                    ]
                    [ loginForm model login.username login.password ]
                , Html.div
                    [ Attr.css [ Css.fontSize (Css.rem 1.5) ] ]
                    [ Log.latest model ]
                ]
            ]
