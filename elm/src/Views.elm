{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Views exposing (init, update, view)

import Api.Request
import Browser.Dom as Dom
import Cache
import Components exposing (loadingAnimation)
import Config.Core exposing (Theme)
import Core exposing (Model, setMain)
import Css
import Dict exposing (Dict)
import Enum.DataType as DataType exposing (DataType)
import Form
import Form.Core exposing (Table)
import Hex
import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Html.Styled.Keyed as Keyed
import Html.Styled.Lazy as Lazy
import Input.Filter
import Json.Decode as D
import Link
import Log
import Main.Core exposing (Main)
import Murmur3
import Path
import Process
import RemoteResource exposing (RemoteResource(..))
import Set
import StateCounter
import StateTimer
import Task
import Time exposing (Posix)
import Translation.Core exposing (Translation, trText)
import Update
import Views.Core exposing (..)


delete : String -> Core.Msg
delete =
    Core.ViewsMsg << Delete


deleteCommit : Core.Msg
deleteCommit =
    Core.ViewsMsg DeleteCommit


deleteCancel : Core.Msg
deleteCancel =
    Core.ViewsMsg DeleteCancel


check : String -> Bool -> Core.Msg
check path checked =
    Core.WithTime (Core.ViewsMsg << Check path checked)


orderBy : String -> String -> Core.Msg
orderBy path column =
    Core.WithTime (Core.ViewsMsg << OrderBy path column)


sheetConnected : SheetData -> Core.Msg
sheetConnected =
    Core.ViewsMsg << SheetConnected


customConnected : CustomData -> Core.Msg
customConnected =
    Core.ViewsMsg << CustomConnected


gotLabelVisibility : String -> Result Dom.Error Bool -> Core.Msg
gotLabelVisibility path result =
    Core.ViewsMsg (GotLabelVisibility path result)


reload : String -> Posix -> () -> Core.Msg
reload path time _ =
    Core.ViewsMsg (Reload path time)


setFilter : String -> String -> Filter -> Core.Msg
setFilter path column filter =
    Core.WithTime (Core.ViewsMsg << SetFilter path column filter)


resetFilter : String -> String -> Core.Msg
resetFilter path column =
    Core.WithTime (Core.ViewsMsg << ResetFilter path column)


toggleFilters : String -> Core.Msg
toggleFilters path =
    Core.WithTime (Core.ViewsMsg << ToggleFilters path)


init : Views
init =
    { cache = Cache.empty
    , showLabels = Set.empty
    , blockedRows = Set.empty
    , delete = Nothing
    }


update : Msg -> Model -> ( Model, Cmd Core.Msg )
update msg model =
    case model.page of
        Core.LoginPage _ ->
            ( model, Cmd.none )

        Core.MainPage main ->
            let
                toCore ( newViews, cmd ) =
                    ( setMain { main | views = newViews } model, cmd )

                views =
                    main.views
            in
            case msg of
                GotView time view_ ->
                    case view_ of
                        DatabaseView databaseData ->
                            handleDatabase time databaseData main model |> toCore

                        SheetView sheetData ->
                            handleSheet time sheetData main model

                        CustomView customData ->
                            handleCustom time customData main model |> toCore

                ViewFailed time path ->
                    ( { views | cache = Cache.miss path time views.cache }
                    , Cmd.none
                    )
                        |> toCore

                Resize ->
                    ( model, getLabelVisibilityAll main )

                SheetConnected sheetData ->
                    ( model, getLabelVisibility sheetData )

                CustomConnected customData ->
                    ( model, getLabelVisibility customData )

                GotLabelVisibility path result ->
                    handleLabelVisibility path (Result.withDefault False result) main.views |> toCore

                Unblock path ->
                    ( { views | blockedRows = Set.remove path views.blockedRows }
                    , Cmd.none
                    )
                        |> toCore

                Delete path ->
                    ( { views
                        | blockedRows = Set.insert path views.blockedRows
                        , delete = Just path
                      }
                    , Cmd.none
                    )
                        |> toCore

                DeleteCommit ->
                    case views.delete of
                        Just row ->
                            ( { views | delete = Nothing }
                            , Api.Request.delete (StateTimer.unwrap main.user) row
                            )
                                |> toCore

                        Nothing ->
                            ( model, Cmd.none )

                DeleteCancel ->
                    case views.delete of
                        Just row ->
                            ( { views
                                | delete = Nothing
                                , blockedRows = Set.remove row views.blockedRows
                              }
                            , Cmd.none
                            )
                                |> toCore

                        Nothing ->
                            ( model, Cmd.none )

                PathChanged time ->
                    handlePathChange time main model

                Check path checked time ->
                    handleCheck time path checked main |> toCore

                OrderBy path column time ->
                    handleOrderBy time path column main |> toCore

                CleanUp time ->
                    let
                        cache =
                            Cache.clean model.config.cacheDuration
                                time
                                views.cache

                        newViews =
                            { views | cache = cache }
                    in
                    ( newViews, Cmd.none )
                        |> toCore

                Reload path time ->
                    if Cache.time path views.cache == time then
                        ( model, Api.Request.view (StateTimer.unwrap main.user) path views )

                    else
                        ( model, Cmd.none )

                ToggleFilters path time ->
                    let
                        newViews =
                            setSheetData time
                                path
                                (\sheetData ->
                                    { sheetData
                                        | filters =
                                            case sheetData.filters of
                                                Just _ ->
                                                    Nothing

                                                Nothing ->
                                                    Just Dict.empty
                                    }
                                )
                                views
                    in
                    ( newViews
                    , Api.Request.view (StateTimer.unwrap main.user) path newViews
                    )
                        |> toCore

                SetFilter path column filter time ->
                    let
                        newViews =
                            setSheetData time
                                path
                                (\sheetData -> { sheetData | filters = Maybe.map (Dict.insert column filter) sheetData.filters })
                                views
                    in
                    ( newViews
                    , Task.perform (reload path time) (Process.sleep 150)
                    )
                        |> toCore

                ResetFilter path column time ->
                    let
                        newViews =
                            setSheetData time
                                path
                                (\sheetData -> { sheetData | filters = Maybe.map (Dict.remove column) sheetData.filters })
                                views
                    in
                    ( newViews
                    , Api.Request.view (StateTimer.unwrap main.user) path newViews
                    )
                        |> toCore


setSheetData : Posix -> String -> (SheetData -> SheetData) -> Views -> Views
setSheetData time path method views =
    { views
        | cache =
            Cache.update path
                time
                (RemoteResource.update
                    (\v ->
                        case v of
                            SheetView sheetData ->
                                SheetView (method sheetData)

                            _ ->
                                v
                    )
                )
                views.cache
    }


updateOrder : String -> View -> View
updateOrder column view_ =
    case view_ of
        SheetView sheetData ->
            case sheetData.order of
                Just { by, asc } ->
                    if by == column then
                        SheetView { sheetData | order = Just { by = by, asc = not asc } }

                    else
                        SheetView { sheetData | order = Just { by = column, asc = False } }

                Nothing ->
                    SheetView { sheetData | order = Just { by = column, asc = False } }

        _ ->
            view_


handleOrderBy : Posix -> String -> String -> Main -> ( Views, Cmd Core.Msg )
handleOrderBy time path column main =
    let
        views =
            main.views

        cache =
            Cache.update path
                time
                (\res ->
                    case res of
                        RemoteResource.Cached view_ ->
                            RemoteResource.Reloading (updateOrder column view_)

                        RemoteResource.Reloading view_ ->
                            RemoteResource.Reloading (updateOrder column view_)

                        _ ->
                            RemoteResource.Loading
                )
                views.cache

        newViews =
            { views | cache = cache }
    in
    ( newViews
    , Api.Request.view
        (StateTimer.unwrap main.user)
        path
        newViews
    )


handleCheck : Posix -> String -> Bool -> Main -> ( Views, Cmd Core.Msg )
handleCheck time path checked main =
    let
        viewPath =
            Path.dropRight 4 path

        views =
            main.views

        newViews =
            { views | cache = Cache.load [ viewPath ] time views.cache }
    in
    ( newViews, Api.Request.check (StateTimer.unwrap main.user) viewPath newViews path checked )


handlePathChange : Posix -> Main -> Model -> ( Model, Cmd Core.Msg )
handlePathChange time main model =
    if model.url.path == "/" then
        ( model, Cmd.none )

    else
        let
            views =
                main.views

            toCore ( views_, cmd ) =
                ( setMain { main | views = views_ } model, cmd )
        in
        case Cache.match model.url.path views.cache of
            ( _, Cached (SheetView sheetData) ) ->
                let
                    newViews =
                        { views | cache = Cache.load [ sheetData.path ] time views.cache }

                    reload_ =
                        ( newViews
                        , Api.Request.view (StateTimer.unwrap main.user) sheetData.path newViews
                        )
                in
                if model.url.path == sheetData.path then
                    reload_ |> toCore

                else if String.endsWith "/new" model.url.path then
                    case Dict.get (String.dropRight 4 model.url.path) sheetData.tables of
                        Just activeTable ->
                            let
                                ( form, formCmd ) =
                                    Form.new model.url.path activeTable main
                            in
                            ( setMain { main | form = Just form } model, formCmd )

                        Nothing ->
                            reload_ |> toCore

                else
                    case findActive model.url.path sheetData.rows of
                        Just activeRow ->
                            handleActiveRow time sheetData activeRow main model

                        Nothing ->
                            reload_ |> toCore

            ( _, Cached (DatabaseView databaseData) ) ->
                let
                    newViews =
                        { views | cache = Cache.load [ databaseData.path ] time views.cache }
                in
                ( newViews, Api.Request.view (StateTimer.unwrap main.user) databaseData.path newViews )
                    |> toCore

            ( _, Cached (CustomView customData) ) ->
                let
                    newViews =
                        { views | cache = Cache.load [ customData.path ] time views.cache }
                in
                ( newViews, Api.Request.view (StateTimer.unwrap main.user) customData.path newViews )
                    |> toCore

            ( _, Loading ) ->
                ( model, Cmd.none )

            ( _, Reloading (SheetView sheetData) ) ->
                if model.url.path == sheetData.path then
                    ( model, Cmd.none )

                else if String.endsWith "/new" model.url.path then
                    case Dict.get (Path.slice -3 2 model.url.path) sheetData.tables of
                        Just activeTable ->
                            let
                                ( form, formCmd ) =
                                    Form.new model.url.path activeTable main
                            in
                            ( setMain { main | form = Just form } model, formCmd )

                        Nothing ->
                            ( model, Cmd.none )

                else
                    case findActive model.url.path sheetData.rows of
                        Just activeRow ->
                            handleActiveRow time sheetData activeRow main model

                        Nothing ->
                            ( model, Cmd.none )

            ( _, Reloading (DatabaseView databaseData) ) ->
                ( model, Cmd.none )

            ( _, Reloading (CustomView customData) ) ->
                ( model, Cmd.none )

            ( viewPath, Missing ) ->
                let
                    newViews =
                        { views | cache = Cache.load [ viewPath ] time views.cache }
                in
                ( newViews, Api.Request.view (StateTimer.unwrap main.user) viewPath newViews )
                    |> toCore


handleActiveRow : Posix -> SheetData -> Row -> Main -> Model -> ( Model, Cmd Core.Msg )
handleActiveRow time sheetData row main model =
    let
        views =
            main.views
    in
    if
        model.url.path
            == (row.path ++ "/edit")
            && Maybe.map .path main.form
            /= Just model.url.path
    then
        case Dict.get row.table sheetData.tables of
            Just table ->
                let
                    ( form, formCmd ) =
                        Form.edit model.url.path table row.id row.data main
                in
                ( setMain { main | form = Just form } model, formCmd )

            Nothing ->
                ( model, Cmd.none )

    else
        let
            newViews =
                { views | cache = Cache.load (List.map .href row.views) time views.cache }
        in
        ( setMain { main | views = newViews } model
        , Cmd.batch
            (List.map
                (\link ->
                    Api.Request.view (StateTimer.unwrap main.user) link.href newViews
                )
                row.views
            )
        )


handleSheet : Posix -> SheetData -> Main -> Model -> ( Model, Cmd Core.Msg )
handleSheet time sheetData main model =
    let
        views =
            main.views

        cache =
            Cache.cache sheetData.path time (SheetView sheetData) views.cache

        newMain =
            { main | views = { views | cache = cache } }
    in
    case findActive model.url.path sheetData.rows of
        Just activeRow ->
            handleActiveRow time sheetData activeRow newMain model

        Nothing ->
            if
                String.endsWith "/new" model.url.path
                    && Maybe.map .path main.form
                    /= Just model.url.path
            then
                case Dict.get (String.dropRight 4 model.url.path) sheetData.tables of
                    Just activeTable ->
                        let
                            ( form, formCmd ) =
                                Form.new model.url.path activeTable main
                        in
                        ( setMain { newMain | form = Just form } model, formCmd )

                    Nothing ->
                        ( setMain newMain model, Cmd.none )

            else
                ( setMain newMain model, Cmd.none )


handleDatabase : Posix -> DatabaseData -> Main -> Model -> ( Views, Cmd Core.Msg )
handleDatabase time databaseData main model =
    let
        views =
            main.views
    in
    case Link.findActive model.url.path databaseData.views of
        Just active ->
            let
                cache =
                    Cache.load [ active.href ] time views.cache
                        |> Cache.cache databaseData.path time (DatabaseView databaseData)

                newViews =
                    { views | cache = cache }
            in
            ( newViews
            , Api.Request.view
                (StateTimer.unwrap main.user)
                active.href
                newViews
            )

        Nothing ->
            case List.head databaseData.views of
                Just first ->
                    let
                        cache =
                            Cache.load [ first.href ] time views.cache
                                |> Cache.cache databaseData.path
                                    time
                                    (DatabaseView databaseData)

                        newViews =
                            { views | cache = cache }
                    in
                    ( newViews
                    , Api.Request.view
                        (StateTimer.unwrap main.user)
                        first.href
                        newViews
                    )

                Nothing ->
                    ( { views
                        | cache =
                            Cache.cache databaseData.path
                                time
                                (DatabaseView databaseData)
                                views.cache
                      }
                    , Cmd.none
                    )


handleLabelVisibility : String -> Bool -> Views -> ( Views, Cmd Core.Msg )
handleLabelVisibility path visible views =
    if visible then
        ( { views | showLabels = Set.insert path views.showLabels }, Cmd.none )

    else
        ( { views | showLabels = Set.remove path views.showLabels }, Cmd.none )


handleCustom : Posix -> CustomData -> Main -> Model -> ( Views, Cmd Core.Msg )
handleCustom time customData main model =
    let
        views =
            main.views
    in
    ( { views
        | cache =
            Cache.cache customData.path
                time
                (CustomView customData)
                views.cache
      }
    , Cmd.none
    )


sheetView : Model -> Main -> SheetData -> Html Core.Msg
sheetView model main sheetData =
    Keyed.node "div"
        [ Attr.css [ Css.displayFlex, Css.flexDirection Css.column ]
        , Attr.attribute "role" "table"
        ]
        (List.append
            [ headRowView model main sheetData
            , filterRowView model sheetData
            , newRowView model main sheetData
            ]
            (List.map
                (rowView
                    model
                    main
                    (Set.member sheetData.path main.views.showLabels)
                    sheetData.columns
                )
                sheetData.rows
            )
        )


columnWidth : DataType -> Float
columnWidth dataType =
    toFloat (DataType.width dataType * 4)


headRowView : Model -> Main -> SheetData -> ( String, Html Core.Msg )
headRowView model main sheetData =
    ( "head"
    , Html.node "trek-connect-notifier"
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.row
            , Css.padding (Css.rem 0.1)
            , Css.fontWeight Css.bold
            , Css.borderBottom3 (Css.px 1) Css.solid (Css.rgb 255 255 255)
            , Css.flexWrap Css.wrap
            , Css.outline Css.none
            ]
        , Attr.attribute "role" "row"
        , Attr.id (headRowId sheetData.path)
        , Evt.on "connect" (D.succeed (sheetConnected sheetData))
        ]
        (List.map
            (\column ->
                Components.link
                    [ dataCol column.name
                    , Attr.id (columnId sheetData.path column.name)
                    , Attr.attribute "role" "columnheader"
                    , Attr.css
                        [ Css.flexGrow (Css.num (columnWidth column.datatype))
                        , Css.width (Css.rem (columnWidth column.datatype))
                        , Css.property "text-align" (DataType.align column.datatype)
                        , Css.padding2 (Css.rem 0.1) (Css.rem 0.2)
                        , Css.overflowWrap Css.breakWord
                        ]
                    , Evt.onClick (orderBy sheetData.path column.name)
                    ]
                    (List.append
                        [ trText model.translation column.title ]
                        (case sheetData.order of
                            Just order ->
                                if order.by == column.name then
                                    [ Html.span [ Attr.css [ Css.marginLeft (Css.rem 0.5) ] ]
                                        [ Components.caret 0.6
                                            model.config.theme.black
                                            (if order.asc then
                                                Components.Up

                                             else
                                                Components.Down
                                            )
                                        ]
                                    ]

                                else
                                    []

                            Nothing ->
                                []
                        )
                    )
            )
            sheetData.columns
        )
    )


dataId : Int -> Attribute msg
dataId id =
    Attr.attribute "data-id" (String.fromInt id)


dataCol : String -> Attribute msg
dataCol col =
    Attr.attribute "data-col" col


filterRowView : Model -> SheetData -> ( String, Html Core.Msg )
filterRowView model sheetData =
    ( "filters"
    , case sheetData.filters of
        Nothing ->
            Html.text ""

        Just filters ->
            Html.div
                [ Attr.css
                    [ Css.displayFlex
                    , Css.flexDirection Css.row
                    , Css.padding (Css.rem 0.1)
                    , Css.borderBottom3 (Css.px 1) Css.solid (Css.rgb 255 255 255)
                    , Css.flexWrap Css.wrap
                    , Css.outline Css.none
                    ]
                , Attr.attribute "role" "row"
                ]
                (List.map
                    (\column ->
                        let
                            filter =
                                Dict.get column.name filters
                                    |> Maybe.withDefault All

                            inputAttr =
                                { filter = filter
                                , onInput = setFilter sheetData.path column.name
                                , label = column.title
                                , id = columnId sheetData.path column.name
                                , variants = column.variants
                                }
                        in
                        Html.span
                            [ dataCol column.name
                            , Attr.css
                                [ Css.flexGrow (Css.num (columnWidth column.datatype))
                                , Css.width (Css.rem (columnWidth column.datatype))
                                , Css.padding2 (Css.rem 0.1) (Css.rem 0.2)
                                , Css.overflowWrap Css.breakWord
                                , Css.fontSize (Css.rem 0.8)
                                , Css.property "text-align" (DataType.align column.datatype)
                                ]
                            ]
                            [ Html.div
                                [ Attr.css
                                    [ Css.display Css.inlineFlex
                                    , Css.flexDirection Css.column
                                    , Css.alignItems Css.flexStart
                                    ]
                                ]
                                [ case column.datatype of
                                    DataType.Int ->
                                        Input.Filter.number_ model inputAttr

                                    DataType.Float ->
                                        Input.Filter.number_ model inputAttr

                                    DataType.Currency ->
                                        Input.Filter.number_ model inputAttr

                                    DataType.Bool ->
                                        Input.Filter.bool model inputAttr

                                    DataType.Check ->
                                        Input.Filter.bool model inputAttr

                                    DataType.Enum ->
                                        Input.Filter.enum model inputAttr

                                    DataType.Timestamp ->
                                        Input.Filter.timestamp model inputAttr

                                    _ ->
                                        Input.Filter.text model inputAttr
                                , Components.link
                                    [ Attr.css
                                        [ if filter == All then
                                            Css.visibility Css.hidden

                                          else
                                            Css.visibility Css.visible
                                        , Css.color model.config.theme.secondary.dark
                                        ]
                                    , Evt.onClick (resetFilter sheetData.path column.name)
                                    ]
                                    [ trText model.translation "Reset" ]
                                ]
                            ]
                    )
                    sheetData.columns
                )
    )


newButton : Model -> Table -> Html Core.Msg
newButton model table =
    Html.a
        [ Attr.href (table.path ++ "/new")
        , Attr.css [ Css.flexGrow (Css.num 1), Css.displayFlex, Css.flexDirection Css.column ]
        ]
        [ Components.slimPrimaryButton model
            True
            [ trText model.translation "Add ", trText model.translation table.title ]
        ]


newRowView : Model -> Main -> SheetData -> ( String, Html Core.Msg )
newRowView model main sheetData =
    ( "new"
    , if String.endsWith "/new" model.url.path && Path.dropRight 3 model.url.path == sheetData.path then
        case main.form of
            Just form ->
                let
                    responsiveCell =
                        cellView model.translation model.url.path (Set.member sheetData.path main.views.showLabels) False
                in
                Html.div [ Attr.attribute "role" "row" ]
                    [ rowHead model.config.theme.primary.darker
                        (List.map2 responsiveCell
                            sheetData.columns
                            (Form.getOutput form)
                        )
                    , rowBody model.config.theme [ Form.view model main form ]
                    ]

            Nothing ->
                Html.div
                    [ Attr.css [ Css.displayFlex, Css.justifyContent Css.stretch ]
                    , Attr.attribute "role" "row"
                    ]
                    [ trText model.translation ("Failed to load form for \"" ++ model.url.path ++ "\".") ]

      else
        Html.div
            [ Attr.css
                [ Css.displayFlex
                , Css.flexDirection Css.row
                , Css.justifyContent Css.stretch
                , Css.flexWrap Css.wrap
                ]
            , Attr.attribute "role" "row"
            ]
            (List.concat
                [ [ filterButton model sheetData.path ]
                , Dict.values sheetData.tables |> List.filter .insert |> List.map (newButton model)
                , [ printButton model sheetData.path ]

                --, [ updateButton model sheetData.path ]
                ]
            )
    )


filterButton : Model -> String -> Html Core.Msg
filterButton model path =
    Components.link
        [ Attr.css [ Css.displayFlex, Css.flexDirection Css.column ]
        , Evt.onClick (toggleFilters path)
        ]
        [ Components.slimSecondaryButton model True [ trText model.translation "Filter" ] ]


printButton : Model -> String -> Html Core.Msg
printButton model path =
    Html.a [ Attr.href (path ++ "/print"), Attr.css [ Css.marginLeft Css.auto ] ]
        [ Components.slimSecondaryButton model True [ trText model.translation "Print" ] ]


rowView : Model -> Main -> Bool -> List Column -> Row -> ( String, Html Core.Msg )
rowView model main showLabels columns row =
    ( String.fromInt row.id
    , if String.startsWith (row.path ++ "/") (model.url.path ++ "/") then
        let
            responsiveCell =
                cellView model.translation row.path showLabels
        in
        Html.div
            [ dataId row.id
            , Attr.css
                [ Css.borderTop3 (Css.px 1) Css.solid (Css.rgb 255 255 255) ]
            , Attr.attribute "role" "row"
            ]
            (if main.views.delete == Just row.path then
                [ Html.span [ Evt.onClick deleteCommit ]
                    [ Components.primaryButton model True [ trText model.translation "Delete" ] ]
                , Html.span [ Evt.onClick deleteCancel ]
                    [ Components.button model True [ trText model.translation "Cancel" ] ]
                ]

             else if model.url.path == row.path ++ "/edit" then
                case main.form of
                    Just form ->
                        [ rowHead model.config.theme.primary.darker (List.map2 (responsiveCell False) columns (Form.getOutput form))
                        , rowBody model.config.theme [ Form.view model main form ]
                        ]

                    Nothing ->
                        [ rowHead model.config.theme.primary.darker (List.map2 (responsiveCell False) columns row.data)
                        , rowBody model.config.theme [ trText model.translation ("Failed to load form for \"" ++ model.url.path ++ "\".") ]
                        ]

             else
                [ rowHead model.config.theme.primary.darker (List.map2 (responsiveCell True) columns row.data)
                , rowBody model.config.theme (rowButtons model main row :: rowSubViews model main row)
                ]
            )

      else
        Lazy.lazy5 passiveRowView model.translation model.config.theme showLabels columns row
    )


passiveRowView : Translation -> Theme -> Bool -> List Column -> Row -> Html Core.Msg
passiveRowView translation theme showLabels columns row =
    Html.div
        [ dataId row.id
        , Attr.css
            [ Css.borderTop3 (Css.px 1) Css.solid (Css.rgb 255 255 255) ]
        , Attr.attribute "role" "row"
        ]
        [ Html.a
            [ Attr.href row.path ]
            [ rowHead theme.black (List.map2 (cellView translation row.path showLabels False) columns row.data) ]
        ]


rowHead : Css.Color -> List (Html Core.Msg) -> Html Core.Msg
rowHead color children =
    Html.div
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.row
            , Css.alignItems Css.flexStart
            , Css.color color
            , Css.padding (Css.rem 0.1)
            , Css.flexWrap Css.wrap
            ]
        ]
        (List.append
            children
            [ Html.span
                [ Attr.css [ Css.padding2 (Css.rem 0.1) Css.zero ]
                , Attr.attribute "role" "cell"
                ]
                [ Html.text Components.hair ]
            ]
        )


rowBody : Theme -> List (Html Core.Msg) -> Html Core.Msg
rowBody theme children =
    Html.div
        [ Attr.css
            [ Css.borderLeft3
                (Css.rem 1)
                Css.solid
                (Components.opaque 0.5 theme.primary.medium)
            , Css.paddingLeft (Css.rem 0.2)
            ]
        ]
        children


rowButtons : Model -> Main -> Row -> Html Core.Msg
rowButtons model main row =
    let
        enabled =
            not (Set.member row.path main.views.blockedRows)
    in
    Html.div
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.row
            , Css.justifyContent Css.flexStart
            , Css.alignItems Css.center
            , Css.flexWrap Css.wrap
            ]
        ]
        [ Html.a [ Attr.href (row.path ++ "/edit") ]
            [ Components.button model
                (enabled && row.update)
                [ trText model.translation "Edit" ]
            ]
        , Components.link [ Evt.onClick (delete row.path) ]
            [ Components.button model
                (enabled && row.update)
                [ trText model.translation "Delete" ]
            ]
        , Html.a [ Attr.href (getParent row) ]
            [ Components.button model
                True
                [ trText model.translation "Close" ]
            ]
        ]


rowSubViews : Model -> Main -> Row -> List (Html Core.Msg)
rowSubViews model main row =
    List.map
        (\link ->
            Html.div [ Attr.css [ Css.backgroundColor (Components.opaque 0.1 model.config.theme.grey.dark) ] ]
                [ Components.heading 6 [ trText model.translation link.title ]
                , view model main link.href
                ]
        )
        row.views


cellView : Translation -> String -> Bool -> Bool -> Column -> String -> Html Core.Msg
cellView translation rowPath showLabel checkable column value =
    Html.div
        [ dataCol column.name
        , Attr.css
            [ Css.flexGrow (Css.num (columnWidth column.datatype))
            , Css.width (Css.rem (columnWidth column.datatype))
            , Css.property "text-align" (DataType.align column.datatype)
            , if DataType.numeric column.datatype then
                Css.flexShrink Css.zero

              else
                Css.property "" ""
            , Css.padding2 (Css.rem 0.1) (Css.rem 0.2)
            ]
        ]
        (if showLabel && value /= "" then
            [ Html.p
                [ Attr.css
                    [ Css.fontWeight Css.bold
                    , Css.overflowWrap Css.breakWord
                    ]
                ]
                [ trText translation column.title ]
            , renderValue rowPath checkable column value
            ]

         else
            [ renderValue rowPath checkable column value ]
        )


renderValue : String -> Bool -> Column -> String -> Html Core.Msg
renderValue rowPath active column strVal =
    case column.datatype of
        DataType.Check ->
            renderCheck (rowPath ++ "/" ++ column.name) active strVal

        DataType.Text ->
            renderText active strVal

        DataType.Sql ->
            renderText active strVal

        DataType.Xml ->
            renderText active strVal

        _ ->
            renderString active strVal


renderString : Bool -> String -> Html Core.Msg
renderString active strVal =
    if active then
        Html.p
            [ Attr.css
                [ Css.overflow Css.hidden
                , Css.textOverflow Css.clip
                , Css.whiteSpace Css.normal
                ]
            ]
            [ Html.text strVal ]

    else
        Html.p
            [ Attr.css
                [ Css.overflow Css.hidden
                , Css.textOverflow Css.ellipsis
                , Css.whiteSpace Css.noWrap
                ]
            ]
            [ Html.text strVal ]


renderText : Bool -> String -> Html Core.Msg
renderText active strVal =
    if active then
        Html.p
            [ Attr.css
                [ Css.overflow Css.hidden
                , Css.textOverflow Css.clip
                , Css.whiteSpace Css.normal
                ]
            ]
            (String.split "\n" strVal |> List.map Html.text |> List.intersperse (Html.br [] []))

    else
        Html.p
            [ Attr.css
                [ Css.overflow Css.hidden
                , Css.textOverflow Css.ellipsis
                , Css.whiteSpace Css.noWrap
                ]
            ]
            [ Html.text strVal ]


renderCheck : String -> Bool -> String -> Html Core.Msg
renderCheck path active strVal =
    if active then
        Html.p [ Attr.css [ Css.overflow Css.hidden ] ]
            [ Html.input
                [ Attr.type_ "checkbox"
                , Evt.onCheck (check path)
                , Attr.checked (strVal == "✓")
                ]
                []
            ]

    else
        renderString active strVal


columnId : String -> String -> String
columnId path name =
    Hex.toString (Murmur3.hashString 28379 (path ++ "/" ++ name))


headRowId : String -> String
headRowId path =
    Hex.toString (Murmur3.hashString 29384 (path ++ "/headRow"))


getLabelVisibility : { a | path : String, columns : List Column } -> Cmd Core.Msg
getLabelVisibility data =
    Task.attempt (gotLabelVisibility data.path)
        (Process.sleep 50
            |> Task.andThen
                (\_ -> Dom.getElement (headRowId data.path))
            |> Task.andThen
                (\headRowInfo ->
                    Task.sequence
                        (List.drop 1 data.columns
                            |> List.map
                                (\column ->
                                    Dom.getElement (columnId data.path column.name)
                                )
                        )
                        |> Task.map
                            (List.any
                                (\columnInfo ->
                                    columnInfo.element.x - headRowInfo.element.x < 10
                                )
                            )
                )
        )


findActive : String -> List { a | path : String } -> Maybe { a | path : String }
findActive path list =
    List.foldr
        (\current result ->
            if current.path == path || String.startsWith (current.path ++ "/") path then
                Just current

            else
                result
        )
        Nothing
        list


getParent : Row -> String
getParent row =
    let
        path =
            String.split "/" row.path
    in
    List.take (List.length path - 3) path
        |> String.join "/"


view : Model -> Main -> String -> Html Core.Msg
view model main path =
    case Cache.get path main.views.cache of
        Cached (DatabaseView databaseData) ->
            databaseView model main databaseData

        Cached (SheetView sheetData) ->
            sheetView model main sheetData

        Cached (CustomView customData) ->
            customView model main customData

        Reloading (DatabaseView databaseData) ->
            databaseView model main databaseData

        Reloading (SheetView sheetData) ->
            sheetView model main sheetData

        Reloading (CustomView customData) ->
            customView model main customData

        Loading ->
            loadingAnimation

        Missing ->
            Html.div [] [ trText model.translation ("Loading \"" ++ path ++ "\"failed.") ]


databaseView : Model -> Main -> DatabaseData -> Html Core.Msg
databaseView model main databaseData =
    Html.div []
        [ Components.heading 2 [ trText model.translation databaseData.title ]
        , Html.div
            [ Attr.css [ Css.marginBottom (Css.rem 0.5) ] ]
            [ Link.tabNavigation model databaseData.views ]
        , case List.head databaseData.views of
            Just firstLink ->
                case Link.findActive model.url.path databaseData.views of
                    Just active ->
                        Html.div [ Attr.css [ Css.backgroundColor (Components.opaque 0.1 model.config.theme.grey.dark) ] ]
                            [ view model main active.href ]

                    Nothing ->
                        Html.div [ Attr.css [ Css.backgroundColor (Components.opaque 0.1 model.config.theme.grey.dark) ] ]
                            [ view model main firstLink.href ]

            Nothing ->
                Html.div [] [ trText model.translation "No views." ]
        ]


getLabelVisibilityAll : Main -> Cmd Core.Msg
getLabelVisibilityAll main =
    Cmd.batch
        (Cache.cachedValues main.views.cache
            |> List.filterMap
                (\view_ ->
                    case view_ of
                        DatabaseView _ ->
                            Nothing

                        SheetView sheetData ->
                            Just (getLabelVisibility sheetData)

                        CustomView customData ->
                            Just (getLabelVisibility customData)
                )
        )


customHeadRowView : Model -> Main -> CustomData -> Html Core.Msg
customHeadRowView model main customData =
    Html.node "trek-connect-notifier"
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.row
            , Css.padding (Css.rem 0.1)
            , Css.fontWeight Css.bold
            , Css.borderBottom3 (Css.px 1) Css.solid (Css.rgb 255 255 255)
            , Css.flexWrap Css.wrap
            , Css.outline Css.none
            ]
        , Attr.id (headRowId customData.path)
        , Evt.on "connect" (D.succeed (customConnected customData))
        ]
        (List.map
            (\column ->
                Html.span
                    [ dataCol column.name
                    , Attr.id (columnId customData.path column.name)
                    , Attr.css
                        [ Css.flexGrow (Css.num (columnWidth column.datatype))
                        , Css.width (Css.rem (columnWidth column.datatype))
                        , Css.property "text-align" (DataType.align column.datatype)
                        , Css.padding2 (Css.rem 0.1) (Css.rem 0.2)
                        , Css.overflowWrap Css.breakWord
                        ]
                    ]
                    [ trText model.translation column.title ]
            )
            customData.columns
        )


customRowView : Theme -> Translation -> String -> Bool -> List Column -> List String -> Html Core.Msg
customRowView theme translation path showLabels columns rowData =
    Html.div
        [ Attr.css [ Css.borderTop3 (Css.px 1) Css.solid (Css.rgb 255 255 255) ] ]
        [ rowHead theme.black
            (List.map2 (cellView translation path showLabels False) columns rowData)
        ]


customRowPath : String -> Int -> String
customRowPath viewPath index =
    viewPath ++ "-" ++ String.fromInt index


customView : Model -> Main -> CustomData -> Html Core.Msg
customView model main customData =
    Html.div
        [ Attr.css [ Css.displayFlex, Css.flexDirection Css.column ] ]
        (List.append
            [ customHeadRowView model main customData ]
            (List.indexedMap
                (\index row ->
                    Lazy.lazy6 customRowView
                        model.config.theme
                        model.translation
                        (customRowPath customData.path index)
                        (Set.member customData.path main.views.showLabels)
                        customData.columns
                        row
                )
                customData.rows
            )
        )
