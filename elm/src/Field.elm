{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Field exposing (Field(..), parseCheck, parseField, toString)

import Enum exposing (DataType)
import Time


type Field
    = StringField (Maybe String)
    | IntField (Maybe Int)
    | FloatField (Maybe Float)
    | BoolField (Maybe Bool)
    | TimestampField (Maybe Time.Posix)
    | NullField
    | CheckField Bool


parseStringField : Maybe String -> Result String Field
parseStringField maybeVal =
    Ok (StringField maybeVal)


parseIntField : Maybe String -> Result String Field
parseIntField maybeVal =
    case maybeVal of
        Just stringVal ->
            Result.map (\v -> IntField (Just v))
                (Result.fromMaybe
                    ("Failed to parse \"" ++ stringVal ++ "\" to IntField.")
                    (String.toInt stringVal)
                )

        Nothing ->
            Ok (FloatField Nothing)


parseFloatField : Maybe String -> Result String Field
parseFloatField maybeVal =
    case maybeVal of
        Just stringVal ->
            Result.map (\v -> FloatField (Just v))
                (Result.fromMaybe
                    ("Failed to parse \"" ++ stringVal ++ "\" to FloatField.")
                    (String.toFloat stringVal)
                )

        Nothing ->
            Ok (FloatField Nothing)


parseCheck : Maybe String -> Result String Bool
parseCheck maybeVal =
    case maybeVal of
        Just "0" ->
            Ok False

        Just "1" ->
            Ok True

        Just "FALSE" ->
            Ok False

        Just "TRUE" ->
            Ok True

        Just "false" ->
            Ok False

        Just "true" ->
            Ok True

        Just val ->
            Err ("Failed to parse string \"" ++ val ++ "\" to check.")

        Nothing ->
            Err "Failed to parse null to check."


parseCheckField : Maybe String -> Result String Field
parseCheckField maybeVal =
    Result.map CheckField (parseCheck maybeVal)


parseBoolField : Maybe String -> Result String Field
parseBoolField maybeVal =
    case maybeVal of
        Just "0" ->
            Ok (BoolField (Just False))

        Just "1" ->
            Ok (BoolField (Just True))

        Just "FALSE" ->
            Ok (BoolField (Just False))

        Just "TRUE" ->
            Ok (BoolField (Just True))

        Just "false" ->
            Ok (BoolField (Just False))

        Just "true" ->
            Ok (BoolField (Just True))

        Just "" ->
            Ok (BoolField Nothing)

        Just val ->
            Err ("Failed to parse string \"" ++ val ++ "\" to bool.")

        Nothing ->
            Ok (BoolField Nothing)


parseTimestampField : Maybe String -> Result String Field
parseTimestampField maybeVal =
    case maybeVal of
        Just stringVal ->
            Result.map (\v -> TimestampField (Just v))
                (Result.fromMaybe
                    ("Failed to parse \"" ++ stringVal ++ "\" to Timestamp.")
                    (Maybe.map Time.millisToPosix (String.toInt stringVal))
                )

        Nothing ->
            Ok (TimestampField Nothing)


parseField : DataType -> Maybe String -> Result String Field
parseField dataType maybeVal =
    case dataType of
        Enum.String ->
            parseStringField maybeVal

        Enum.Int ->
            parseIntField maybeVal

        Enum.Float ->
            parseFloatField maybeVal

        Enum.Bool ->
            parseBoolField maybeVal

        Enum.Name ->
            parseStringField maybeVal

        Enum.Password ->
            parseCheckField maybeVal

        Enum.Timestamp ->
            parseTimestampField maybeVal

        Enum.Currency ->
            parseFloatField maybeVal

        Enum.Sha256hash ->
            parseStringField maybeVal

        Enum.Barcode ->
            parseStringField maybeVal

        Enum.Text ->
            parseStringField maybeVal

        Enum.Id ->
            parseIntField maybeVal

        Enum.Sql ->
            parseStringField maybeVal

        Enum.Null ->
            Ok NullField

        Enum.Check ->
            parseCheckField maybeVal

        Enum.Xml ->
            parseStringField maybeVal

        Enum.Vid ->
            parseStringField maybeVal

        Enum.Enum ->
            parseStringField maybeVal


toString : Field -> String
toString field =
    case field of
        StringField (Just val) ->
            val

        StringField Nothing ->
            ""

        IntField (Just val) ->
            String.fromInt val

        IntField Nothing ->
            ""

        FloatField (Just val) ->
            String.fromFloat val

        FloatField Nothing ->
            ""

        BoolField (Just val) ->
            case val of
                True ->
                    String.fromChar (Char.fromCode 10003)

                False ->
                    String.fromChar (Char.fromCode 215)

        BoolField Nothing ->
            ""

        TimestampField (Just posixTime) ->
            let
                year =
                    String.fromInt (Time.toYear Time.utc posixTime)

                month =
                    case Time.toMonth Time.utc posixTime of
                        Time.Jan ->
                            "01"

                        Time.Feb ->
                            "02"

                        Time.Mar ->
                            "03"

                        Time.Apr ->
                            "04"

                        Time.May ->
                            "05"

                        Time.Jun ->
                            "06"

                        Time.Jul ->
                            "07"

                        Time.Aug ->
                            "08"

                        Time.Sep ->
                            "09"

                        Time.Oct ->
                            "10"

                        Time.Nov ->
                            "11"

                        Time.Dec ->
                            "12"

                day =
                    String.padLeft 2 '0' (String.fromInt (Time.toDay Time.utc posixTime))

                hour =
                    String.padLeft 2 '0' (String.fromInt (Time.toHour Time.utc posixTime))

                minute =
                    String.padLeft 2 '0' (String.fromInt (Time.toMinute Time.utc posixTime))

                second =
                    String.padLeft 2 '0' (String.fromInt (Time.toSecond Time.utc posixTime))
            in
            year ++ "-" ++ month ++ "-" ++ day ++ " " ++ hour ++ ":" ++ minute ++ ":" ++ second

        TimestampField Nothing ->
            ""

        NullField ->
            ""

        CheckField val ->
            case val of
                True ->
                    String.fromChar (Char.fromCode 10003)

                False ->
                    String.fromChar (Char.fromCode 215)
