{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Main.Select exposing (form)

import Core exposing (Model)
import Form.Core exposing (Form)
import Select


form : Model -> Maybe Form
form model =
    case Select.main_ model of
        Just main ->
            main.form

        Nothing ->
            Nothing
