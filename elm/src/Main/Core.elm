{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Main.Core exposing (Index, Main, MenuState(..), Msg(..), Password, User, emptyPassword, setForm, toggleLogMenu, toggleMainMenu, togglePasswordMenu, toggleUserMenu)

-- import Json.Decode as D
-- import Dict exposing (Dict)

import Browser.Dom as Dom
import Cache exposing (Cache)
import Dict exposing (Dict)
import Form.Core exposing (Form)
import Link.Core exposing (Link)
import Process
import RemoteResource exposing (RemoteResource)
import RemoteResource.Counter exposing (ResourceCounter)
import Set exposing (Set)
import StateCounter exposing (StateCounter)
import StateTimer exposing (StateTimer)
import Time exposing (Posix)
import Views.Core exposing (Views)


type alias User =
    { id : String
    , name : String
    , token : String
    , locale : String
    , fontSize : Float
    }


type MenuState
    = Closed
    | MainMenu
    | UserMenu
    | LogMenu
    | PasswordMenu


type alias Index =
    List Link


type Msg
    = GotIndex Int (List Link) Posix
    | IndexFailed Int
    | PathChanged
    | ToggleUserMenu
    | TogglePasswordMenu
    | OpenUserMenu
    | ToggleLogMenu
    | OpenLogMenu
    | ToggleMainMenu
    | OpenMainMenu
    | CloseMenu
    | LogOut
    | Resize
    | SetOldPassword String
    | SetNewPassword String
    | SetRepeatPassword String
    | ChangePassword
    | GotPassword Int Password
    | GotUser User Posix
    | Refresh Posix
    | ShowInPrint String String
    | HideInPrint String String


type alias Password =
    { old : String
    , new : String
    , repeat : String
    , validity : List String
    , changed : Bool
    }


emptyPassword : Password
emptyPassword =
    { old = ""
    , new = ""
    , repeat = ""
    , validity = []
    , changed = False
    }


type alias Main =
    { views : Views
    , index : ResourceCounter Index
    , user : StateTimer User
    , menuState : MenuState
    , form : Maybe Form
    , password : ResourceCounter Password
    , hiddenInPrint : Dict String (Set String)
    }


setForm : Form -> Main -> Main
setForm form main =
    { main | form = Just form }


toggleMainMenu : MenuState -> MenuState
toggleMainMenu menuState =
    case menuState of
        MainMenu ->
            Closed

        _ ->
            MainMenu


toggleLogMenu : MenuState -> MenuState
toggleLogMenu menuState =
    case menuState of
        LogMenu ->
            Closed

        _ ->
            LogMenu


toggleUserMenu : MenuState -> MenuState
toggleUserMenu menuState =
    case menuState of
        UserMenu ->
            Closed

        _ ->
            UserMenu


togglePasswordMenu : MenuState -> MenuState
togglePasswordMenu menuState =
    case menuState of
        PasswordMenu ->
            UserMenu

        UserMenu ->
            PasswordMenu

        _ ->
            Closed
