{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Main.Decode exposing (index, password, user, userTimer)

import Enum.Decode exposing (dataType, viewClass)
import Json.Decode exposing (..)
import Link.Decode exposing (link)
import Main.Core exposing (..)
import StateTimer exposing (StateTimer)


user : Decoder User
user =
    map5 User
        (field "id" string)
        (field "name" string)
        (field "token" string)
        (field "locale" string)
        (field "fontSize" float)


userTimer : Decoder (StateTimer User)
userTimer =
    StateTimer.decoder user


index : Decoder Index
index =
    list link


password : Decoder Password
password =
    map5 Password
        (field "old" string)
        (field "new" string)
        (field "repeat" string)
        (field "validity" (list string))
        (field "changed" bool)
