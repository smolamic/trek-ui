{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Main.Encode exposing (passwordResource, user, userTimer)

import Json.Encode exposing (..)
import Main.Core exposing (..)
import RemoteResource
import RemoteResource.Counter exposing (ResourceCounter)
import StateTimer exposing (StateTimer)


password : Password -> Value
password pw =
    object
        [ ( "old", string pw.old )
        , ( "new", string pw.new )
        , ( "repeat", string pw.repeat )
        ]


passwordResource : ResourceCounter Password -> Value
passwordResource =
    RemoteResource.Counter.encodeWithDefault password emptyPassword


user : User -> Value
user user_ =
    object
        [ ( "id", string user_.id )
        , ( "name", string user_.name )
        , ( "token", string user_.token )
        , ( "locale", string user_.locale )
        , ( "fontSize", float user_.fontSize )
        ]


userTimer : StateTimer User -> Value
userTimer =
    StateTimer.encoder user
