{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Input exposing (active, barcode, basic, bool, disabled, enabled, enum, getOptions, getValidity, hasOptions, number_, ref, string, text, timestamp)

import Array exposing (Array)
import Components exposing (loadingAnimation)
import Config.Core exposing (Theme)
import Core exposing (Model)
import Css exposing (Style)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Input.Core exposing (..)
import Json.Decode as D
import RemoteResource exposing (RemoteResource)
import Translation.Core exposing (tr, trText)



-- STYLE


containerStyle : Style
containerStyle =
    Css.position Css.relative


inputStyle : Theme -> Style
inputStyle theme =
    Css.batch
        [ Css.border3 (Css.px 1) Css.solid theme.grey.dark
        , Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.05) theme.grey.dark
        , Css.margin2 (Css.rem 0.2) Css.zero
        , Css.padding (Css.rem 0.3)
        , Css.flexGrow (Css.num 1)
        , Css.fontSize (Css.rem 1.1)
        , Css.lineHeight (Css.rem 1.2)
        , Css.color theme.black
        , Css.width (Css.pct 100)
        , Css.boxSizing Css.borderBox
        ]


activeStyle : Theme -> Style
activeStyle theme =
    Css.batch
        [ Css.borderColor theme.primary.dark
        , Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.05) theme.primary.dark
        ]


dangerStyle : Theme -> Bool -> Style
dangerStyle theme isOk =
    if isOk then
        Css.batch []

    else
        Css.batch
            [ Css.borderColor theme.danger
            , Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.05) theme.danger
            ]


requiredStyle : Theme -> Style
requiredStyle theme =
    Css.batch
        [ Css.borderColor theme.secondary.dark
        , Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.05) theme.secondary.dark
        ]


invalidStyle : Theme -> Style
invalidStyle theme =
    Css.batch
        [ Css.borderColor theme.danger
        , Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.05) theme.danger
        ]


unitStyle : String -> Style
unitStyle symbol =
    Css.batch
        [ Css.after
            [ Css.property "content" ("'" ++ symbol ++ "'")
            , Css.position Css.absolute
            , Css.right (Css.rem 0.8)
            , Css.top Css.zero
            , Css.height (Css.pct 100)
            , Css.displayFlex
            , Css.flexDirection Css.column
            , Css.justifyContent Css.center
            ]
        , Css.position Css.relative
        ]


optionStyle : Theme -> Style
optionStyle theme =
    Css.batch
        [ Css.padding (Css.rem 1)
        ]


activeOptionStyle : Theme -> Bool -> Style
activeOptionStyle theme active_ =
    if active_ then
        Css.batch
            [ Css.backgroundColor theme.primary.light ]

    else
        Css.batch []


optionsMenuStyle : Theme -> Style
optionsMenuStyle theme =
    Css.batch
        [ Css.position Css.absolute
        , Css.backgroundColor theme.grey.light
        , Css.width (Css.pct 100)
        , Css.zIndex (Css.int 10)
        ]



-- OPTIONS


type Span
    = Bold String
    | Regular String


highlight : List String -> String -> List (Html Core.Msg)
highlight highlights option_ =
    List.foldl
        (\highlight_ result ->
            List.concatMap
                (\span ->
                    case span of
                        Bold _ ->
                            [ span ]

                        Regular value ->
                            String.split highlight_ value
                                |> List.map Regular
                                |> List.intersperse (Bold highlight_)
                )
                result
        )
        [ Regular option_ ]
        highlights
        |> List.map
            (\span ->
                case span of
                    Bold value ->
                        Html.b [] [ Html.text value ]

                    Regular value ->
                        Html.span [] [ Html.text value ]
            )


optionsMenuItem : Theme -> (Int -> Core.Msg) -> (InputOption -> Core.Msg) -> Int -> Int -> InputOption -> Html Core.Msg
optionsMenuItem theme onSelect onAccept selected index option =
    Html.li
        [ Attr.css
            [ optionStyle theme
            , Css.cursor Css.pointer
            , activeOptionStyle theme (index == selected)
            ]
        , Evt.onClick (onAccept option)
        , Evt.onMouseOver (onSelect index)
        ]
        [ Html.text option.label ]


optionsMenu : Theme -> (InputOption -> Value) -> Attributes Core.Msg -> Array InputOption -> Html Core.Msg
optionsMenu theme acceptOption attr options =
    Html.ul
        [ Attr.css [ optionsMenuStyle theme ] ]
        (Array.toList
            (Array.indexedMap
                (optionsMenuItem theme
                    attr.onSelect
                    (attr.onAccept << acceptOption)
                    attr.selected
                )
                options
            )
        )


optionsMenuSingle : Theme -> Value -> Attributes Core.Msg -> String -> Html Core.Msg
optionsMenuSingle theme value attr label =
    Html.ul
        [ Attr.css [ optionsMenuStyle theme ] ]
        [ Html.li
            [ Attr.css
                [ optionStyle theme
                , Css.cursor Css.pointer
                , activeOptionStyle theme True
                ]
            , Evt.onClick (attr.onAccept value)
            ]
            [ Html.text label ]
        ]


optionsMenuEmpty : Model -> Html Core.Msg
optionsMenuEmpty model =
    let
        theme =
            model.config.theme
    in
    Html.ul [ Attr.css [ optionsMenuStyle theme ] ]
        [ Html.li [ Attr.css [ optionStyle theme ] ]
            [ trText model.translation "Nothing found." ]
        ]


optionsMenuLoading : Theme -> Value -> Attributes Core.Msg -> Html Core.Msg
optionsMenuLoading theme accepted attr =
    Html.ul [ Attr.css [ optionsMenuStyle theme ] ]
        [ Html.li
            [ Attr.css [ optionStyle theme, Css.cursor Css.pointer ]
            , Evt.onClick (attr.onAccept accepted)
            ]
            [ loadingAnimation ]
        ]


validityInfo : Model -> Bool -> List String -> Html Core.Msg
validityInfo model active_ validity =
    if List.isEmpty validity || not active_ then
        Html.text ""

    else
        Html.div
            [ Attr.css
                [ Css.position Css.absolute
                , Css.width (Css.rem 20)
                , Css.backgroundColor model.config.theme.grey.light
                , Css.opacity (Css.num 0.1)
                ]
            ]
            [ Html.ul []
                (List.map
                    (Html.li [] << List.singleton << trText model.translation)
                    validity
                )
            ]



-- INPUTS


onKeyUp : Core.Msg -> Core.Msg -> Core.Msg -> Core.Msg -> D.Decoder Core.Msg
onKeyUp onArrowUp onArrowDown onEnter onEscape =
    D.field "key" D.string
        |> D.map
            (\key ->
                case key of
                    "ArrowUp" ->
                        onArrowUp

                    "ArrowDown" ->
                        onArrowDown

                    "Enter" ->
                        onEnter

                    "Escape" ->
                        onEscape

                    _ ->
                        Core.NoOp
            )


setString : StringInputValue -> String -> Value
setString sv value =
    { sv | value = Just value } |> StringValue


acceptString : StringInputValue -> InputOption -> Value
acceptString sv option =
    { sv | value = Just option.value } |> StringValue


setBasic : BasicInputValue -> String -> Value
setBasic bv value =
    { bv | value = Just value } |> BasicValue


setBarcode : BasicInputValue -> String -> Value
setBarcode bv value =
    { bv | value = Just value } |> BarcodeValue


getDate : BasicInputValue -> String
getDate bv =
    case Maybe.map String.words bv.value of
        Just (date :: _) ->
            date

        _ ->
            ""


getTime : BasicInputValue -> String
getTime bv =
    case Maybe.map String.words bv.value of
        Just (date :: time :: _) ->
            time

        _ ->
            ""


setDate : BasicInputValue -> String -> Value
setDate bv value =
    { bv | value = Just (value ++ " " ++ getTime bv) } |> BasicValue


setTime : BasicInputValue -> String -> Value
setTime bv value =
    { bv | value = Just (getDate bv ++ " " ++ value) } |> BasicValue


setBool : BoolInputValue -> Maybe Bool -> Value
setBool bv value =
    { bv | value = value } |> BoolValue


setCheck : CheckInputValue -> Bool -> Value
setCheck bv value =
    { bv | value = value } |> CheckValue


setRefSearch : RefInputValue -> String -> Value
setRefSearch rv value =
    { rv | search = Just value } |> RefValue


acceptRef : RefInputValue -> InputOption -> Value
acceptRef rv option =
    { rv | search = Nothing, value = Just option.value, label = Just option.label }
        |> RefValue


acceptMatchingRef : RefInputValue -> Value
acceptMatchingRef rv =
    { rv | accept = True }
        |> RefValue


safeModBy : Int -> Int -> Int
safeModBy div num =
    if div == 0 then
        0

    else
        modBy div num


barcode : Model -> Attributes Core.Msg -> Html Core.Msg
barcode model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        containerActive bv =
            if attr.state == Active then
                [ Evt.on "keyup"
                    (onKeyUp
                        Core.NoOp
                        Core.NoOp
                        (attr.onAccept (BarcodeValue bv))
                        (attr.onAccept (BarcodeValue bv))
                    )
                ]

            else
                []

        base =
            getBasicInputAttr "text" attr

        full bv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setBarcode bv)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setBarcode bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled True
                        , Evt.onInput (attr.onInput << setBarcode bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

        info text_ =
            List.append base
                [ Attr.value (tr model.translation text_)
                , Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (BarcodeValue bv) ->
            Html.span (container ++ containerActive bv)
                [ Html.input (full bv) []
                , if attr.state == Active then
                    optionsMenuSingle theme (BarcodeValue bv) attr (Maybe.withDefault "" bv.value)

                  else
                    Html.text ""
                ]

        RemoteResource.Reloading (BarcodeValue bv) ->
            Html.span (container ++ containerActive bv)
                [ Html.input (full bv) []
                , if attr.state == Active then
                    optionsMenuSingle theme (BarcodeValue bv) attr (Maybe.withDefault "" bv.value)

                  else
                    Html.text ""
                ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.input (info "loading...") [] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.input (info "Failed to load data.") [] ]

        _ ->
            Html.span container
                [ Html.input (info "Wrong input type.") [] ]


string : Model -> Attributes Core.Msg -> Html Core.Msg
string model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        containerActive sv =
            if attr.state == Active then
                [ Evt.on "keyup"
                    (onKeyUp
                        (attr.onSelect (safeModBy (Array.length sv.options) attr.selected - 1))
                        (attr.onSelect (safeModBy (Array.length sv.options) attr.selected + 1))
                        (case Array.get attr.selected sv.options of
                            Just currentOption ->
                                attr.onAccept (acceptString sv currentOption)

                            Nothing ->
                                attr.onAccept (StringValue sv)
                        )
                        (attr.onAccept (StringValue sv))
                    )
                ]

            else
                []

        base =
            getBasicInputAttr "text" attr

        full sv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" sv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setString sv)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty sv.validity)
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" sv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setString sv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty sv.validity)
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" sv.value)
                        , Attr.disabled True
                        , Evt.onInput (attr.onInput << setString sv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty sv.validity)
                            ]
                        ]

        info text_ =
            List.append base
                [ Attr.value (tr model.translation text_)
                , Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (StringValue sv) ->
            Html.span (container ++ containerActive sv)
                [ Html.input (full sv) []
                , if attr.state == Active then
                    if Array.isEmpty sv.options then
                        optionsMenuEmpty model

                    else
                        optionsMenu theme (acceptString sv) attr sv.options

                  else
                    Html.text ""

                --, validityInfo model (attr.state == Active) sv.validity
                ]

        RemoteResource.Reloading (StringValue sv) ->
            Html.span (container ++ containerActive sv)
                [ Html.input (full sv) []
                , if attr.state == Active then
                    optionsMenuLoading theme (StringValue sv) attr

                  else
                    Html.text ""

                --, validityInfo model sv.validity
                ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.input (info "loading...") [] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.input (info "Failed to load data.") [] ]

        _ ->
            Html.span container
                [ Html.input (info "Wrong input type.") [] ]


number_ : Model -> Attributes Core.Msg -> Html Core.Msg
number_ model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        base =
            getNumberInputAttr attr

        ( containerWithUnit, inputWithUnit ) =
            case attr.unit of
                Just symbol ->
                    ( [ Attr.css [ unitStyle symbol ] ]
                    , Css.paddingRight (Css.rem (toFloat (1 + String.length symbol)))
                    )

                Nothing ->
                    ( [], Css.batch [] )

        full bv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            , inputWithUnit
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            , inputWithUnit
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled True
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            , inputWithUnit
                            ]
                        ]

        info text_ =
            List.append base
                [ Attr.value (tr model.translation text_)
                , Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (BasicValue bv) ->
            Html.span containerWithUnit
                [ Html.input (full bv) [] ]

        RemoteResource.Reloading (BasicValue bv) ->
            Html.span containerWithUnit
                [ Html.input (full bv) [] ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.input (info "loading...") [] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.input (info "Failed to load data.") [] ]

        _ ->
            Html.span container
                [ Html.input (info "Wrong input type.") [] ]


basic : String -> Model -> Attributes Core.Msg -> Html Core.Msg
basic type_ model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        base =
            getBasicInputAttr type_ attr

        full bv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.disabled True
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

        info text_ =
            List.append base
                [ Attr.value (tr model.translation text_)
                , Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (BasicValue bv) ->
            Html.span container
                [ Html.input (full bv) [] ]

        RemoteResource.Reloading (BasicValue bv) ->
            Html.span container
                [ Html.input (full bv) [] ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.input (info "loading...") [] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.input (info "Failed to load data.") [] ]

        _ ->
            Html.span container
                [ Html.input (info "Wrong input type.") [] ]


ref : Model -> Attributes Core.Msg -> Html Core.Msg
ref model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        containerActive rv =
            if attr.state == Active && Array.length rv.options > 0 then
                [ Evt.on "keyup"
                    (onKeyUp
                        (attr.onSelect (safeModBy (Array.length rv.options) (attr.selected - 1)))
                        (attr.onSelect (safeModBy (Array.length rv.options) (attr.selected + 1)))
                        (case rv.search of
                            Just _ ->
                                attr.onAccept (acceptMatchingRef rv)

                            Nothing ->
                                case Array.get attr.selected rv.options of
                                    Just currentOption ->
                                        attr.onAccept (acceptRef rv currentOption)

                                    Nothing ->
                                        attr.onAccept (acceptMatchingRef rv)
                        )
                        (attr.onAccept (RefValue rv))
                    )
                ]

            else
                []

        base =
            getBasicInputAttr "text" attr

        full rv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" rv.search)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setRefSearch rv)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty rv.validity)
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" rv.label)
                        , Attr.disabled False
                        , Evt.onInput (attr.onInput << setRefSearch rv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty rv.validity)
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.value (Maybe.withDefault "" rv.label)
                        , Attr.disabled True
                        , Evt.onInput (attr.onInput << setRefSearch rv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty rv.validity)
                            ]
                        ]

        info text_ =
            List.append base
                [ Attr.value (tr model.translation text_)
                , Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (RefValue rv) ->
            Html.span (container ++ containerActive rv)
                [ Html.input (full rv) []
                , if attr.state == Active then
                    if Array.isEmpty rv.options then
                        optionsMenuEmpty model

                    else
                        optionsMenu theme (acceptRef rv) attr rv.options

                  else
                    Html.text ""
                ]

        RemoteResource.Reloading (RefValue rv) ->
            Html.span (container ++ containerActive rv)
                [ Html.input (full rv) []
                , if attr.state == Active then
                    optionsMenuLoading theme (acceptMatchingRef rv) attr

                  else
                    Html.text ""
                ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.input (info "loading...") [] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.input (info "Failed to load data.") [] ]

        _ ->
            Html.span container
                [ Html.input (info "Wrong input type.") [] ]


timestamp : Model -> Attributes Core.Msg -> Html Core.Msg
timestamp model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        baseDate =
            getBasicInputAttr "date" attr

        baseTime =
            getTimeInputAttr attr

        withState bv =
            case attr.state of
                Active ->
                    [ Attr.disabled False
                    , Attr.css
                        [ inputStyle theme
                        , activeStyle theme
                        , dangerStyle theme (List.isEmpty bv.validity)
                        ]
                    ]

                Enabled ->
                    [ Attr.disabled False
                    , Attr.css
                        [ inputStyle theme
                        , dangerStyle theme (List.isEmpty bv.validity)
                        ]
                    ]

                Disabled ->
                    [ Attr.disabled True
                    , Attr.css
                        [ inputStyle theme
                        , dangerStyle theme (List.isEmpty bv.validity)
                        ]
                    ]

        date bv =
            baseDate
                ++ withState bv
                ++ [ Attr.value (getDate bv)
                   , Evt.onInput (attr.onInput << setDate bv)
                   ]

        time bv =
            baseTime
                ++ withState bv
                ++ [ Attr.value (getTime bv)
                   , Evt.onInput (attr.onInput << setTime bv)
                   ]

        info text_ =
            List.append (getBasicInputAttr "text" attr)
                [ Attr.value (tr model.translation text_)
                , Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (BasicValue bv) ->
            Html.span container
                [ Html.input (date bv) []
                , Html.input (time bv) []
                ]

        RemoteResource.Reloading (BasicValue bv) ->
            Html.span container
                [ Html.input (date bv) []
                , Html.input (time bv) []
                ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.input (info "loading...") [] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.input (info "Failed to load data.") [] ]

        _ ->
            Html.span container
                [ Html.input (info "Wrong input type.") [] ]


bool : Model -> Attributes Core.Msg -> Html Core.Msg
bool model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        borderColor =
            case attr.state of
                Active ->
                    theme.primary.darker

                Enabled ->
                    theme.grey.darker

                Disabled ->
                    theme.grey.medium

        buttonBase =
            Css.batch
                [ Css.borderTop3 (Css.px 1) Css.solid borderColor
                , Css.borderRight3 (Css.px 1) Css.solid borderColor
                , Css.borderBottom3 (Css.px 1) Css.solid borderColor
                , Css.width (Css.rem 4)
                , Css.height (Css.rem 2)
                ]

        borderLeft =
            Css.borderLeft3 (Css.px 1) Css.solid borderColor

        buttonActive =
            Css.backgroundColor theme.primary.medium

        onClickBool bv value =
            Evt.onClick (attr.onInput (setBool bv value))

        onClickCheck bv value =
            Evt.onClick (attr.onInput (setCheck bv value))

        boolButtons bv =
            case bv.value of
                Just True ->
                    Html.span []
                        [ Html.button
                            [ Attr.type_ "button"
                            , onClickBool bv (Just False)
                            , Evt.onFocus attr.onFocus
                            , Evt.onBlur attr.onBlur
                            , Attr.css [ buttonBase, borderLeft ]
                            ]
                            [ Html.text Components.cross ]
                        , Html.button
                            [ Attr.type_ "button"
                            , onClickBool bv Nothing
                            , Evt.onFocus attr.onFocus
                            , Evt.onBlur attr.onBlur
                            , Attr.css [ buttonBase, buttonActive ]
                            ]
                            [ Html.text Components.check ]
                        ]

                Just False ->
                    Html.span []
                        [ Html.button
                            [ Attr.type_ "button"
                            , onClickBool bv Nothing
                            , Evt.onFocus attr.onFocus
                            , Evt.onBlur attr.onBlur
                            , Attr.css [ buttonBase, borderLeft, buttonActive ]
                            ]
                            [ Html.text Components.cross ]
                        , Html.button
                            [ Attr.type_ "button"
                            , onClickBool bv (Just True)
                            , Evt.onFocus attr.onFocus
                            , Evt.onBlur attr.onBlur
                            , Attr.css [ buttonBase ]
                            ]
                            [ Html.text Components.check ]
                        ]

                Nothing ->
                    Html.span []
                        [ Html.button
                            [ Attr.type_ "button"
                            , onClickBool bv (Just False)
                            , Evt.onFocus attr.onFocus
                            , Evt.onBlur attr.onBlur
                            , Attr.css [ buttonBase, borderLeft ]
                            ]
                            [ Html.text Components.cross ]
                        , Html.button
                            [ Attr.type_ "button"
                            , onClickBool bv (Just True)
                            , Evt.onFocus attr.onFocus
                            , Evt.onBlur attr.onBlur
                            , Attr.css [ buttonBase ]
                            ]
                            [ Html.text Components.check ]
                        ]

        checkButton cv =
            if cv.value then
                Html.button
                    [ Attr.type_ "button"
                    , onClickCheck cv False
                    , Evt.onFocus attr.onFocus
                    , Evt.onBlur attr.onBlur
                    , Attr.css [ buttonBase, borderLeft, buttonActive ]
                    ]
                    [ Html.text Components.check ]

            else
                Html.button
                    [ Attr.type_ "button"
                    , onClickCheck cv True
                    , Evt.onFocus attr.onFocus
                    , Evt.onBlur attr.onBlur
                    , Attr.css [ buttonBase, borderLeft ]
                    ]
                    [ Html.text Components.cross ]
    in
    case attr.value of
        RemoteResource.Cached (BoolValue bv) ->
            Html.span container [ boolButtons bv ]

        RemoteResource.Reloading (BoolValue bv) ->
            Html.span container [ boolButtons bv ]

        RemoteResource.Cached (CheckValue cv) ->
            Html.span container [ checkButton cv ]

        RemoteResource.Reloading (CheckValue cv) ->
            Html.span container [ checkButton cv ]

        RemoteResource.Loading ->
            Html.span container [ loadingAnimation ]

        RemoteResource.Missing ->
            Html.span container [ trText model.translation "Failed to load data." ]

        _ ->
            Html.span container [ trText model.translation "Wrong input type." ]


text : Model -> Attributes Core.Msg -> Html Core.Msg
text model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        base =
            getTextInputAttr attr

        full bv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.disabled True
                        , Evt.onInput (attr.onInput << setBarcode bv)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

        info =
            List.append base
                [ Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]
    in
    case attr.value of
        RemoteResource.Cached (BasicValue bv) ->
            Html.span container
                [ Html.textarea (full bv) [ Html.text (Maybe.withDefault "" bv.value) ] ]

        RemoteResource.Reloading (BasicValue bv) ->
            Html.span container
                [ Html.textarea (full bv) [ Html.text (Maybe.withDefault "" bv.value) ] ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.textarea info [ trText model.translation "loading..." ] ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.textarea info [ trText model.translation "Failed to load data." ] ]

        _ ->
            Html.span container
                [ Html.textarea info [ trText model.translation "Wrong input type." ] ]


enum : Model -> Attributes Core.Msg -> Html Core.Msg
enum model attr =
    let
        theme =
            model.config.theme

        container =
            getContainerAttr attr

        base =
            getSelectInputAttr attr

        full bv =
            case attr.state of
                Active ->
                    List.append base
                        [ Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.css
                            [ inputStyle theme
                            , activeStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Enabled ->
                    List.append base
                        [ Attr.disabled False
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

                Disabled ->
                    List.append base
                        [ Attr.disabled True
                        , Evt.onInput (attr.onInput << setBasic bv)
                        , Attr.value (Maybe.withDefault "" bv.value)
                        , Attr.css
                            [ inputStyle theme
                            , dangerStyle theme (List.isEmpty bv.validity)
                            ]
                        ]

        option { value, label } =
            Html.option [ Attr.value (String.fromInt value) ] [ trText model.translation label ]

        info =
            List.append base
                [ Attr.disabled True
                , Attr.css [ inputStyle theme ]
                ]

        variants =
            Maybe.withDefault [] attr.variants
    in
    case attr.value of
        RemoteResource.Cached (BasicValue bv) ->
            Html.span container
                [ Html.select (full bv) (List.map option variants) ]

        RemoteResource.Reloading (BasicValue bv) ->
            Html.span container
                [ Html.select (full bv) (List.map option variants) ]

        RemoteResource.Loading ->
            Html.span container
                [ Html.select info
                    [ option { value = 0, label = "loading..." } ]
                ]

        RemoteResource.Missing ->
            Html.span container
                [ Html.select info
                    [ option { value = 0, label = "Failed to load data." } ]
                ]

        _ ->
            Html.span container
                [ Html.select info
                    [ option { value = 0, label = "Wrong input type." } ]
                ]



-- ATTRIBUTE HANDLING


getContainerAttr : Attributes Core.Msg -> List (Html.Attribute Core.Msg)
getContainerAttr attr =
    [ Evt.onMouseOver attr.onMouseOver
    , Evt.onMouseOut attr.onMouseOut
    , Attr.css [ containerStyle ]
    ]


getNumberInputAttr : Attributes Core.Msg -> List (Html.Attribute Core.Msg)
getNumberInputAttr attr =
    [ Attr.id attr.id
    , Attr.step (Maybe.withDefault "any" attr.step)
    , Attr.type_ "number"
    , Evt.onFocus attr.onFocus
    , Evt.onBlur attr.onBlur
    ]


getBasicInputAttr : String -> Attributes Core.Msg -> List (Html.Attribute Core.Msg)
getBasicInputAttr type_ attr =
    [ Evt.onFocus attr.onFocus
    , Evt.onBlur attr.onBlur
    , Attr.id attr.id
    , Attr.type_ type_
    ]


getTimeInputAttr : Attributes Core.Msg -> List (Html.Attribute Core.Msg)
getTimeInputAttr attr =
    [ Evt.onFocus attr.onFocus
    , Evt.onBlur attr.onBlur
    , Attr.type_ "time"
    , Attr.step "any"
    ]


getTextInputAttr : Attributes Core.Msg -> List (Html.Attribute Core.Msg)
getTextInputAttr attr =
    [ Evt.onFocus attr.onFocus
    , Evt.onBlur attr.onBlur
    , Attr.id attr.id
    ]


getSelectInputAttr : Attributes Core.Msg -> List (Html.Attribute Core.Msg)
getSelectInputAttr attr =
    [ Evt.onFocus attr.onFocus
    , Evt.onBlur attr.onBlur
    , Attr.id attr.id
    ]



-- STATE


enabled : State
enabled =
    Enabled


disabled : State
disabled =
    Disabled


active : State
active =
    Active



-- METHODS


getValidity : Value -> List String
getValidity value =
    case value of
        BasicValue { validity } ->
            validity

        BoolValue { validity } ->
            validity

        StringValue { validity } ->
            validity

        RefValue { validity } ->
            validity

        BarcodeValue { validity } ->
            validity

        _ ->
            []


getOptions : Value -> Maybe (Array InputOption)
getOptions value =
    case value of
        StringValue { options } ->
            Just options

        RefValue { options } ->
            Just options

        BarcodeValue bv ->
            let
                value_ =
                    Maybe.withDefault "" bv.value
            in
            Just (Array.fromList [ { value = value_, label = value_ } ])

        _ ->
            Nothing


hasOptions : Value -> Bool
hasOptions value =
    case value of
        StringValue _ ->
            True

        RefValue _ ->
            True

        BarcodeValue _ ->
            True

        _ ->
            False
