{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Form exposing (edit, getOutput, new, update, view)

import Api.Request
import Browser.Dom as Dom
import Browser.Events
import Components exposing (loadingAnimation)
import Config.Core exposing (Theme)
import Core exposing (Model, setForm, setMain)
import Css exposing (Style)
import Dict
import Enum.DataType as DataType exposing (DataType)
import Form.Core exposing (..)
import Form.Decode
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Input
import Input.Core
import Json.Decode as D
import Json.Encode as E
import Log
import Log.Core
import Main.Core exposing (Main)
import Main.Select
import Path
import Process
import RemoteResource exposing (RemoteResource)
import RemoteResource.Counter
import Select
import StateCounter exposing (StateCounter)
import StateTimer
import Task
import Translation.Core exposing (trText)
import Update


edit : String -> Table -> Int -> List String -> Main -> ( Form, Cmd Core.Msg )
edit path table id output main =
    let
        form =
            { path = path
            , table = table
            , id = Just id
            , row = RemoteResource.Counter.reloading { input = Dict.empty, output = output }
            , state = Loading
            , checkRequired = True
            , activeInput = Nothing
            , activeOption = 0
            , focused = Nothing
            , hovered = Nothing
            }
    in
    ( form, Api.Request.run (StateTimer.unwrap main.user) { form | checkRequired = False } )


new : String -> Table -> Main -> ( Form, Cmd Core.Msg )
new path table main =
    let
        form =
            { path = path
            , table = table
            , id = Nothing
            , row = RemoteResource.Counter.loading
            , state = Invalid
            , checkRequired = False
            , activeInput = Nothing
            , activeOption = 0
            , focused = Nothing
            , hovered = Nothing
            }
    in
    if List.isEmpty table.columns then
        ( form, Api.Request.save (StateTimer.unwrap main.user) form )

    else
        ( form, Api.Request.run (StateTimer.unwrap main.user) form )


input : String -> Input.Core.Value -> Core.Msg
input column =
    Core.FormMsg << Input column


save : Core.Msg
save =
    Core.FormMsg Save


focus : String -> Core.Msg
focus =
    Core.FormMsg << Focus


blur : String -> Core.Msg
blur =
    Core.FormMsg << Blur


mouseOver : String -> Core.Msg
mouseOver =
    Core.FormMsg << MouseOver


mouseOut : String -> Core.Msg
mouseOut =
    Core.FormMsg << MouseOut


select : String -> Int -> Core.Msg
select column option =
    Core.FormMsg (Select column option)


accept : String -> Input.Core.Value -> Core.Msg
accept column =
    Core.FormMsg << Accept column


run : String -> Int -> () -> Core.Msg
run path count _ =
    Core.FormMsg (Run path count)


triedFocus : Result Dom.Error () -> Core.Msg
triedFocus res =
    case res of
        Ok _ ->
            Core.NoOp

        Err (Dom.NotFound message) ->
            Core.FormMsg (FocusFailed message)


insertInputValue : String -> Input.Core.Value -> RemoteResource Row -> RemoteResource Row
insertInputValue column value row =
    case RemoteResource.toMaybe row of
        Just row_ ->
            RemoteResource.Reloading { input = Dict.insert column value row_.input, output = row_.output }

        Nothing ->
            RemoteResource.Reloading { input = Dict.singleton column value, output = [] }


update : Msg -> Model -> ( Model, Cmd Core.Msg )
update msg model =
    case Maybe.map2 Tuple.pair (Select.main_ model) (Main.Select.form model) of
        Nothing ->
            ( model, Cmd.none )

        Just ( main, form ) ->
            let
                toCore ( newForm, cmd ) =
                    ( setForm newForm main model, cmd )
            in
            case msg of
                Input column value ->
                    let
                        row =
                            StateCounter.inc (insertInputValue column value) form.row

                        count =
                            StateCounter.count row
                    in
                    ( setForm { form | row = row } main model
                    , Task.perform
                        (run form.path count)
                        (Process.sleep 150)
                    )

                Accept column value ->
                    let
                        ( nextActiveInput, nextInputId ) =
                            nextInput column form.table.columns

                        newForm =
                            { form
                                | row = StateCounter.inc (insertInputValue column value) form.row
                                , activeInput = nextActiveInput
                                , focused = nextActiveInput
                                , activeOption = 0
                            }
                    in
                    ( setForm newForm main model
                    , Cmd.batch
                        [ Api.Request.run (StateTimer.unwrap main.user) newForm
                        , Task.attempt triedFocus
                            (Dom.focus nextInputId)
                        ]
                    )

                Run path count ->
                    if form.path == path && StateCounter.is count form.row then
                        ( model
                        , Api.Request.run (StateTimer.unwrap main.user) form
                        )

                    else
                        ( model, Cmd.none )

                GotRow path count row ->
                    if form.path == path then
                        let
                            newRow =
                                RemoteResource.Counter.cache count row form.row
                        in
                        ( setForm { form | row = newRow, state = deriveState row } main model
                        , Cmd.none
                        )

                    else
                        ( model, Cmd.none )

                RunFailed path count ->
                    if form.path == path then
                        let
                            newRow =
                                RemoteResource.Counter.miss count form.row
                        in
                        ( setForm { form | row = newRow, state = Invalid } main model
                        , Cmd.none
                        )

                    else
                        ( model, Cmd.none )

                Save ->
                    case
                        Maybe.map2 Dict.get form.activeInput (StateCounter.unwrap form.row |> RemoteResource.toMaybe |> Maybe.map .input)
                            |> Maybe.andThen identity
                            |> Maybe.map Input.hasOptions
                    of
                        Just True ->
                            ( model, Cmd.none )

                        _ ->
                            let
                                newForm =
                                    { form
                                        | state = Saving
                                        , checkRequired = True
                                    }
                            in
                            ( setForm newForm main model
                            , Api.Request.save (StateTimer.unwrap main.user) newForm
                            )

                SaveSucceeded path ->
                    if path == form.path then
                        ( setMain { main | form = Nothing } model, Cmd.none )

                    else
                        ( model, Cmd.none )

                SaveInvalid path ->
                    if path == form.path then
                        let
                            newForm =
                                { form | state = Invalid }
                        in
                        ( setForm newForm main model
                        , Api.Request.run (StateTimer.unwrap main.user) newForm
                        )

                    else
                        ( model, Cmd.none )

                SaveFailed path ->
                    if path == form.path then
                        ( setForm { form | state = Invalid } main model
                        , Cmd.none
                        )

                    else
                        ( model, Cmd.none )

                PathChanged ->
                    if model.url.path == form.path then
                        ( model, Cmd.none )

                    else
                        ( setMain { main | form = Nothing } model
                        , Api.Request.unlock (StateTimer.unwrap main.user)
                        )

                Focus column ->
                    let
                        newForm =
                            { form
                                | focused = Just column
                                , activeInput = activeInput form.activeInput (Just column) form.hovered
                                , activeOption = 0
                            }
                    in
                    if newForm.activeInput == form.activeInput && newForm.focused == form.focused then
                        ( model, Cmd.none )

                    else
                        ( setForm newForm main model, Api.Request.run (StateTimer.unwrap main.user) newForm )

                Blur column ->
                    if form.focused == Just column then
                        ( setForm
                            { form
                                | focused = Nothing
                                , activeInput = activeInput form.activeInput Nothing form.hovered
                            }
                            main
                            model
                        , Cmd.none
                        )

                    else
                        ( model, Cmd.none )

                MouseOver column ->
                    ( setForm
                        { form
                            | hovered = Just column
                            , activeInput = activeInput form.activeInput form.focused (Just column)
                        }
                        main
                        model
                    , Cmd.none
                    )

                MouseOut column ->
                    ( setForm
                        { form
                            | hovered = Nothing
                            , activeInput = activeInput form.activeInput form.focused Nothing
                        }
                        main
                        model
                    , Cmd.none
                    )

                Select column option ->
                    if form.activeInput == Just column then
                        ( setForm { form | activeOption = option } main model
                        , Cmd.none
                        )

                    else
                        ( model, Cmd.none )

                FocusFailed message ->
                    Log.stdErr message model


nextInput : String -> List Column -> ( Maybe String, String )
nextInput colname columns =
    List.indexedMap
        (\index column ->
            if column.name == colname then
                List.drop (index + 1) columns |> List.head

            else
                Nothing
        )
        columns
        |> List.filterMap identity
        |> List.head
        |> Maybe.map (\next -> ( Just next.name, inputId next.name ))
        |> Maybe.withDefault ( Nothing, "form-submit" )


activeInput : Maybe String -> Maybe String -> Maybe String -> Maybe String
activeInput active focused hovered =
    case focused of
        Just column ->
            Just column

        Nothing ->
            if active == hovered then
                active

            else
                Nothing


view : Model -> Main -> Form -> Html Core.Msg
view model main form =
    Html.div
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.rowReverse
            , Css.flexWrap Css.wrap
            , Css.justifyContent Css.flexEnd
            ]
        ]
        [ Html.div
            [ Attr.css [ Css.minWidth (Css.rem 20), Css.marginBottom (Css.rem 0.5) ] ]
            (case StateCounter.unwrap form.row of
                RemoteResource.Cached row ->
                    List.filterMap
                        (\col ->
                            Dict.get col.name row.input
                                |> Maybe.map
                                    (\value ->
                                        validityDisplay model col.title (Input.getValidity value)
                                    )
                        )
                        form.table.columns

                RemoteResource.Loading ->
                    [ loadingAnimation ]

                RemoteResource.Reloading _ ->
                    [ loadingAnimation ]

                RemoteResource.Missing ->
                    [ trText model.translation "Failed to load validity information" ]
            )
        , Html.div
            [ Attr.css [ Css.width (Css.rem 30), Css.maxWidth (Css.pct 95), Css.marginRight (Css.rem 1) ] ]
            [ Html.form
                [ Evt.preventDefaultOn "submit" (D.succeed ( save, True ))
                , Attr.autocomplete False
                ]
                [ Html.div [] (List.map (field model form) form.table.columns)
                , buttons model form
                ]
            ]
        ]


validityDisplay : Model -> String -> List String -> Html Core.Msg
validityDisplay model title validity =
    if List.isEmpty validity then
        Html.text ""

    else
        Html.div []
            [ Components.heading 6 [ trText model.translation title ]
            , Html.ul [ Attr.css [ Css.color model.config.theme.danger ] ]
                (List.map
                    (\message -> Html.li [] [ trText model.translation message ])
                    validity
                )
            ]


inputId : String -> String
inputId column =
    "form-input-" ++ column


field : Model -> Form -> Column -> Html Core.Msg
field model form column =
    let
        id =
            inputId column.name

        value =
            StateCounter.unwrap form.row
                |> RemoteResource.filterMap (.input >> Dict.get column.name)

        attr =
            { onFocus = focus column.name
            , onBlur = blur column.name
            , onMouseOver = mouseOver column.name
            , onMouseOut = mouseOut column.name
            , onSelect = select column.name
            , onAccept = accept column.name
            , onInput = input column.name
            , selected = form.activeOption
            , id = id
            , unit = column.unit
            , step = column.step
            , required = column.required
            , state =
                if form.state == Saving then
                    Input.disabled

                else if form.activeInput == Just column.name then
                    Input.active

                else
                    Input.enabled
            , value = value
            , variants = column.variants
            }
    in
    Html.p
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.column
            , Css.padding2 Css.zero (Css.rem 0.2)
            ]
        ]
        [ Html.label [ Attr.for id ] [ trText model.translation column.title ]
        , if column.ref then
            Input.ref model attr

          else
            case column.datatype of
                DataType.Int ->
                    Input.number_ model attr

                DataType.Float ->
                    Input.number_ model attr

                DataType.Currency ->
                    Input.number_ model attr

                DataType.String ->
                    Input.string model attr

                DataType.Password ->
                    Input.basic "password" model attr

                DataType.Timestamp ->
                    Input.timestamp model attr

                DataType.Bool ->
                    Input.bool model attr

                DataType.Check ->
                    Input.bool model attr

                DataType.Barcode ->
                    Input.barcode model attr

                DataType.Text ->
                    Input.text model attr

                DataType.Sql ->
                    Input.text model attr

                DataType.Xml ->
                    Input.text model attr

                DataType.Enum ->
                    Input.enum model attr

                _ ->
                    Input.basic "text" model attr
        ]


viewPath : Form -> String
viewPath form =
    case form.id of
        Just id ->
            Path.dropRight 1 form.path

        Nothing ->
            Path.dropRight 3 form.path


buttons : Model -> Form -> Html Core.Msg
buttons model form =
    let
        saveLabel =
            trText model.translation "Save"

        cancelLabel =
            trText model.translation "Cancel"
    in
    Html.p [ Attr.css [ Css.displayFlex, Css.flexDirection Css.row, Css.justifyContent Css.flexStart ] ]
        (case form.state of
            Valid ->
                [ Html.span [] [ Components.primarySubmitButton model True "form-submit" [ saveLabel ] ]
                , Html.a [ Attr.href (viewPath form) ] [ Components.button model True [ cancelLabel ] ]
                ]

            Saving ->
                [ Html.span [] [ Components.primarySubmitButton model False "form-submit" [ loadingAnimation ] ]
                , Html.a [] [ Components.button model False [ cancelLabel ] ]
                ]

            Invalid ->
                [ Html.span [] [ Components.primarySubmitButton model False "form-submit" [ saveLabel ] ]
                , Html.a [ Attr.href (viewPath form) ] [ Components.button model True [ cancelLabel ] ]
                ]

            Loading ->
                [ Html.span [] [ Components.primarySubmitButton model False "form-submit" [ saveLabel ] ]
                , Html.a [] [ Components.button model False [ cancelLabel ] ]
                ]
        )


deriveState : Row -> State
deriveState =
    .input
        >> Dict.foldl
            (\c v p ->
                if List.isEmpty (Input.getValidity v) then
                    p

                else
                    Invalid
            )
            Valid


getOutput : Form -> List String
getOutput form =
    case StateCounter.unwrap form.row of
        RemoteResource.Cached row ->
            row.output

        RemoteResource.Loading ->
            []

        RemoteResource.Reloading row ->
            row.output

        RemoteResource.Missing ->
            []
