{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Core exposing (Column, ColumnType(..), Meta, Order(..), Row, RowState(..), Scene, Sheet, ToHtml, View(..))

import Html.Styled as Html exposing (Html)
import Theme exposing (Theme)
import Translation exposing (Translation)



-- MAIN


type alias Scene =
    { width : Float
    , height : Float
    }


type alias Meta =
    { scene : Scene
    , translation : Translation
    , theme : Theme
    }


type View msg
    = TitleView String String
    | SheetView (Sheet msg)
    | TabbedLayout (List ( String, Bool )) (Maybe (View msg))
    | VerticalLayout (List (View msg))
    | HorizontalLayout (List (View msg))


type alias Sheet msg =
    { columns : List (Column msg)
    , rows : List (Row msg)
    , inserts : List (Insert msg)
    }


type alias Insert msg =
    { title : String
    , onClick : msg
    }


type alias Column msg =
    { title : String
    , type_ : ColumnType msg
    , order : Maybe Order
    , onSetOrder : Maybe msg
    }


type ColumnType msg
    = String
        { length : Maybe Int
        , currentFilter : String
        , onFilter : Maybe (String -> msg)
        }
    | Text
        { onFilter : Maybe (String -> msg)
        , currentFilter : String
        }
    | Int
        { onFilter : Maybe (( Int, Int ) -> msg)
        , currentFilter : ( Int, Int )
        }


type Order
    = Asc
    | Desc


type alias Row msg =
    { data : List String
    , state : RowState msg
    , id : String
    }


type RowState msg
    = Active
        { children : List (View msg)
        , onEdit : Maybe msg
        , onDelete : Maybe msg
        , onClose : Maybe msg
        }
    | Passive { onSelect : msg }
    | Edit Form


type alias Form =
    {}


type alias ToHtml msg =
    View msg -> Html msg
