{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Config.Decode exposing (config)

import Config.Core exposing (..)
import Css exposing (Color, hex, rgb)
import Json.Decode exposing (..)
import Json.Decode.Extra exposing (..)


config : Decoder Config
config =
    map5 Config
        (fieldWithDefault "theme" default.theme theme)
        (fieldWithDefault "logo" default.logo string)
        (fieldWithDefault "refreshInterval" default.refreshInterval float)
        (fieldWithDefault "cacheDuration" default.cacheDuration int)
        (fieldWithDefault "tokenRefreshInterval" default.tokenRefreshInterval int)


theme : Decoder Theme
theme =
    map8 Theme
        (fieldWithDefault "primary" default.theme.primary palette)
        (fieldWithDefault "secondary" default.theme.secondary palette)
        (fieldWithDefault "grey" default.theme.grey palette)
        (fieldWithDefault "black" default.theme.black color)
        (fieldWithDefault "link" default.theme.link color)
        (fieldWithDefault "success" default.theme.success color)
        (fieldWithDefault "warning" default.theme.warning color)
        (fieldWithDefault "danger" default.theme.danger color)


palette : Decoder Palette
palette =
    map5 Palette
        (index 0 color)
        (index 1 color)
        (index 2 color)
        (index 3 color)
        (index 4 color)


color : Decoder Color
color =
    map hex string
