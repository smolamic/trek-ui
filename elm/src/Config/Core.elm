{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Config.Core exposing (Config, Msg(..), Palette, Theme, default)

import Css exposing (Color, rgb)


type alias Config =
    { theme : Theme
    , logo : String
    , refreshInterval : Float
    , cacheDuration : Int
    , tokenRefreshInterval : Int
    }


type alias Palette =
    { darker : Color
    , dark : Color
    , medium : Color
    , light : Color
    , lighter : Color
    }


type alias Theme =
    { primary : Palette
    , secondary : Palette
    , grey : Palette
    , black : Color
    , link : Color
    , success : Color
    , warning : Color
    , danger : Color
    }


type Msg
    = GotConfig Config


defaultTheme : Theme
defaultTheme =
    { primary =
        Palette
            (rgb 11 158 143)
            (rgb 14 187 169)
            (rgb 16 218 198)
            (rgb 130 244 236)
            (rgb 194 248 248)
    , secondary =
        Palette
            (rgb 17 48 142)
            (rgb 21 104 167)
            (rgb 24 120 202)
            (rgb 60 152 232)
            (rgb 142 196 255)
    , grey =
        Palette
            (rgb 89 89 89)
            (rgb 116 116 116)
            (rgb 157 157 157)
            (rgb 187 187 187)
            (rgb 224 224 224)
    , black = rgb 40 40 40
    , link = rgb 24 26 205
    , success = rgb 0 239 87
    , warning = rgb 234 239 0
    , danger = rgb 241 68 0
    }


defaultLogo : String
defaultLogo =
    "<i>TREK</i>"


default : Config
default =
    { theme = defaultTheme
    , logo = defaultLogo
    , refreshInterval = 30000
    , cacheDuration = 600000
    , tokenRefreshInterval = 600000
    }
