{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Translation.Core exposing (Msg(..), Translation, tr, trText)

import Dict exposing (Dict)
import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attr


type Msg
    = GotTranslation Translation


tr : Translation -> String -> String
tr translation text =
    Dict.get text translation
        |> Maybe.withDefault text


trText : Translation -> String -> Html msg
trText translation text =
    Html.text (tr translation text)
