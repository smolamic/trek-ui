{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Translation.Decode exposing (translation)

import Json.Decode exposing (..)
import Translation.Core exposing (..)


translation : Decoder Translation
translation =
    dict string
