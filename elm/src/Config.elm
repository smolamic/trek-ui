{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Config exposing (init, update)

import Api.Request
import Config.Core exposing (..)
import Core exposing (setConfig)
import Css exposing (Color, hex, rgb)
import Json.Decode as D


init : ( Config, Cmd Core.Msg )
init =
    ( default, Api.Request.config )


update : Msg -> Core.Model -> ( Core.Model, Cmd Core.Msg )
update msg model =
    case msg of
        GotConfig config ->
            ( setConfig config model, Cmd.none )
