{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module RemoteResource.Timer exposing (ResourceTimer, cache, cached, encodeWithDefault, filterMap, filterUpdate, isCached, load, loading, map, miss, missing, reloading, toMaybe, update)

import Json.Encode as E
import RemoteResource as Rr exposing (RemoteResource(..))
import StateTimer as St exposing (StateTimer)
import Time exposing (Posix)


type alias ResourceTimer a =
    StateTimer (RemoteResource a)


cached : Posix -> a -> ResourceTimer a
cached time =
    St.new time << Cached


loading : Posix -> ResourceTimer a
loading time =
    St.new time Loading


reloading : Posix -> a -> ResourceTimer a
reloading time =
    St.new time << Reloading


missing : Posix -> ResourceTimer a
missing time =
    St.new time Missing


load : Posix -> ResourceTimer a -> ResourceTimer a
load time =
    St.update time Rr.load


toMaybe : ResourceTimer a -> Maybe a
toMaybe =
    St.unwrap >> Rr.toMaybe


isCached : ResourceTimer a -> Bool
isCached =
    St.unwrap >> Rr.isCached


cache : Posix -> a -> ResourceTimer a -> ResourceTimer a
cache time next =
    St.update time (\_ -> Cached next)


miss : Posix -> ResourceTimer a -> ResourceTimer a
miss time =
    St.update time (\_ -> Missing)


update : Posix -> (a -> a) -> ResourceTimer a -> ResourceTimer a
update time method =
    St.update time (Rr.map method)


map : (a -> b) -> ResourceTimer a -> ResourceTimer b
map method =
    St.map (Rr.map method)


filterUpdate : Posix -> (a -> Maybe a) -> ResourceTimer a -> ResourceTimer a
filterUpdate time method =
    St.update time (Rr.filterMap method)


filterMap : (a -> Maybe b) -> ResourceTimer a -> ResourceTimer b
filterMap method =
    St.map (Rr.filterMap method)


encodeWithDefault : (a -> E.Value) -> a -> ResourceTimer a -> E.Value
encodeWithDefault inner default res =
    toMaybe res
        |> Maybe.withDefault default
        |> inner
