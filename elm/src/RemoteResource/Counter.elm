{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module RemoteResource.Counter exposing (ResourceCounter, cache, cached, encodeWithDefault, filterInc, filterMap, filterSet, inc, isCached, load, loading, map, miss, missing, reloading, set, toMaybe)

import Json.Encode as E
import RemoteResource as Rr exposing (RemoteResource(..))
import StateCounter as Sc exposing (StateCounter)


type alias ResourceCounter a =
    StateCounter (RemoteResource a)


cached : a -> ResourceCounter a
cached =
    Sc.new << Cached


loading : ResourceCounter a
loading =
    Sc.new Loading


reloading : a -> ResourceCounter a
reloading =
    Sc.new << Reloading


missing : ResourceCounter a
missing =
    Sc.new Missing


load : ResourceCounter a -> ResourceCounter a
load =
    Sc.inc Rr.load


toMaybe : ResourceCounter a -> Maybe a
toMaybe =
    Sc.unwrap >> Rr.toMaybe


isCached : ResourceCounter a -> Bool
isCached =
    Sc.unwrap >> Rr.isCached


cache : Int -> a -> ResourceCounter a -> ResourceCounter a
cache count next =
    Sc.update count (\_ -> Cached next)


miss : Int -> ResourceCounter a -> ResourceCounter a
miss count =
    Sc.update count (\_ -> Missing)


inc : (a -> b) -> ResourceCounter a -> ResourceCounter b
inc method =
    Sc.inc (Rr.map method)


set : Int -> (a -> a) -> ResourceCounter a -> ResourceCounter a
set count method =
    Sc.update count (Rr.update method)


map : (a -> b) -> ResourceCounter a -> ResourceCounter b
map method =
    Sc.map (Rr.map method)


filterInc : (a -> Maybe b) -> ResourceCounter a -> ResourceCounter b
filterInc method =
    Sc.inc (Rr.filterMap method)


filterSet : Int -> (a -> Maybe a) -> ResourceCounter a -> ResourceCounter a
filterSet count method =
    Sc.update count (Rr.filterUpdate method)


filterMap : (a -> Maybe b) -> ResourceCounter a -> ResourceCounter b
filterMap method =
    Sc.map (Rr.filterMap method)


encodeWithDefault : (a -> E.Value) -> a -> ResourceCounter a -> E.Value
encodeWithDefault inner default =
    toMaybe
        >> Maybe.withDefault default
        >> inner
