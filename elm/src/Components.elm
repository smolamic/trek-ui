{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Components exposing (Direction(..), button, caret, check, container, cross, hair, heading1, heading2, heading3, heading4, heading5, heading6, link, loadingAnimation, opaque, slimButton)

import Breakpoints as Bp
import Core
import Css exposing (Color, Style)
import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Theme exposing (Palette, Theme)


loadingAnimation : Html msg
loadingAnimation =
    Html.div [] [ Html.text "loading..." ]


heading1 : List (Html msg) -> Html msg
heading1 children =
    Html.h1
        [ Attr.css
            [ Css.fontSize (Css.rem 5)
            , Css.padding2 (Css.rem 1) Css.zero
            ]
        ]
        children


heading2 : List (Html msg) -> Html msg
heading2 children =
    Html.h2
        [ Attr.css
            [ Css.fontSize (Css.rem 3)
            , Css.padding2 (Css.rem 0.6) Css.zero
            ]
        ]
        children


heading3 : List (Html msg) -> Html msg
heading3 children =
    Html.h3
        [ Attr.css
            [ Css.fontSize (Css.rem 2)
            , Css.padding2 (Css.rem 0.5) Css.zero
            ]
        ]
        children


heading4 : List (Html msg) -> Html msg
heading4 children =
    Html.h4
        [ Attr.css
            [ Css.fontSize (Css.rem 1.5)
            , Css.padding2 (Css.rem 0.4) Css.zero
            ]
        ]
        children


heading5 : List (Html msg) -> Html msg
heading5 children =
    Html.h5
        [ Attr.css
            [ Css.fontSize (Css.rem 1.3)
            , Css.padding2 (Css.rem 0.3) Css.zero
            ]
        ]
        children


heading6 : List (Html msg) -> Html msg
heading6 children =
    Html.h6
        [ Attr.css
            [ Css.fontSize (Css.rem 1.2)
            , Css.padding2 (Css.rem 0.2) Css.zero
            ]
        ]
        children


container : List (Html msg) -> Html msg
container content =
    Html.div
        [ Attr.css
            [ Css.width (Css.pct 100)
            , Css.padding (Css.rem 1)
            ]
        ]
        content


link : List (Attribute msg) -> List (Html msg) -> Html msg
link attributes =
    Html.styled Html.span
        [ Css.cursor Css.pointer, Css.outline Css.none ]
        (List.append
            [ Attr.tabindex 0
            , Attr.attribute "role" "link"
            ]
            attributes
        )


slimButton : Float -> Palette -> List (Attribute msg) -> List (Html msg) -> Html msg
slimButton width palette =
    Html.styled (button width palette)
        [ Css.padding (Css.rem 0.1), Css.margin (Css.rem 0.1) ]


button : Float -> Palette -> List (Attribute msg) -> List (Html msg) -> Html msg
button width palette =
    Html.styled Html.button
        (if Bp.isDesktop width then
            [ Css.hover [ Css.boxShadow4 Css.zero (Css.rem 0.05) (Css.rem 0.2) palette.medium ]
            , Css.active [ Css.boxShadow4 Css.zero Css.zero (Css.rem 0.1) palette.medium ]
            , Css.padding (Css.rem 0.2)
            , Css.margin (Css.rem 0.2)
            , Css.borderStyle Css.none
            , Css.color palette.darker
            , Css.property "background" "none"
            , Css.outline Css.none
            , Css.opacity (Css.num 1)
            , Css.disabled [ Css.opacity (Css.num 0.6) ]
            ]

         else
            [ Css.border3 (Css.px 1) Css.solid palette.dark
            , Css.active
                [ Css.boxShadow5 Css.inset Css.zero Css.zero (Css.rem 0.2) palette.medium
                , Css.disabled [ Css.boxShadow Css.none ]
                ]
            , Css.padding (Css.rem 0.3)
            , Css.margin (Css.rem 0.2)
            , Css.color palette.darker
            , Css.property "background" "none"
            , Css.outline Css.none
            , Css.opacity (Css.num 1)
            , Css.disabled [ Css.opacity (Css.num 0.6) ]
            ]
        )


filledButton : Palette -> List (Attribute msg) -> List (Html msg) -> Html msg
filledButton palette =
    Html.styled Html.button
        [ Css.border3 (Css.px 1) Css.solid palette.medium
        , Css.backgroundColor palette.light
        , Css.padding (Css.rem 0.3)
        , Css.margin (Css.rem 0.2)
        , Css.color palette.dark
        , Css.outline Css.none
        , Css.opacity (Css.num 1)
        , Css.disabled [ Css.opacity (Css.num 0.6) ]
        , Css.focus [ Css.borderColor palette.dark ]
        ]


opaque : Float -> Color -> Color
opaque opacity base =
    Css.rgba base.red base.green base.blue opacity


caret : Float -> Color -> Direction -> Html msg
caret size color direction =
    let
        rotation =
            case direction of
                Up ->
                    45

                Right ->
                    135

                Down ->
                    225

                Left ->
                    315
    in
    Html.span
        [ Attr.css
            [ Css.transform (Css.rotate (Css.deg rotation))
            , Css.display Css.inlineBlock
            , Css.fontSize (Css.rem size)
            ]
        ]
        [ Html.text (String.fromChar '◤') ]


type Direction
    = Up
    | Right
    | Down
    | Left


hair : String
hair =
    String.fromChar '\u{200A}'


cross : String
cross =
    String.fromChar '✕'


check : String
check =
    String.fromChar '✓'
