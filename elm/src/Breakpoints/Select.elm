{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Breakpoints.Select exposing (desktop, down, max, min, mobile, only, touch, up)

import Breakpoints as Bp exposing (Breakpoint(..))
import Core


min : Core.Scene -> Breakpoint
min =
    .width >> Bp.min


max : Core.Scene -> Breakpoint
max =
    .width >> Bp.max


up : Breakpoint -> Core.Scene -> Bool
up breakpoint_ model =
    Bp.up breakpoint_ (.width model)


down : Breakpoint -> Core.Scene -> Bool
down breakpoint_ model =
    Bp.down breakpoint_ (.width model)


only : Breakpoint -> Core.Scene -> Bool
only breakpoint_ model =
    Bp.only breakpoint_ (.width model)


desktop : Core.Scene -> Bool
desktop model =
    Bp.desktop (.width model)


mobile : Core.Scene -> Bool
mobile model =
    Bp.mobile (.width model)


touch : Core.Scene -> Bool
touch model =
    Bp.touch (.width model)
