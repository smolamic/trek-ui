module View.Sheet exposing (view)

import Core
import Html.Styled as Html exposing (Html)


column : Core.Column msg -> Html msg
column data =
    Html.span [] [ Html.text data.title ]


cell : String -> Html msg
cell value =
    Html.span [] [ Html.text value ]


row : Core.ToHtml msg -> Core.Row msg -> Html msg
row toHtml data =
    Html.div [] (List.map cell data.data)


view : Core.ToHtml msg -> Core.Sheet msg -> Html msg
view toHtml sheet =
    Html.div []
        [ Html.div [] (List.map column sheet.columns)
        , Html.div [] (List.map (row toHtml) sheet.rows)
        ]
