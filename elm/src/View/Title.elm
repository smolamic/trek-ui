{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module View.Title exposing (view)

import Components
import Core
import Css
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr


view : Core.Meta -> String -> String -> Html msg
view meta main sub =
    let
        trMain =
            meta.translation.get main

        trSub =
            meta.translation.get sub
    in
    Html.div []
        [ Components.heading2 [ Html.text trMain ]
        , Components.heading3 [ Html.text trSub ]
        ]
