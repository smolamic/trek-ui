{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Login.Core exposing (Login, Msg(..), State(..))


type State
    = Busy
    | Idle


type alias Login =
    { username : String
    , password : String
    , state : State
    }


type Msg
    = SetUsername String
    | SetPassword String
    | Authenticate
    | ResetPassword
    | AuthenticationFailed
