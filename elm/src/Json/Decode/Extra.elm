{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Json.Decode.Extra exposing (fieldWithDefault, map10, map9)

import Json.Decode exposing (..)


map9 :
    (a -> b -> c -> d -> e -> f -> g -> h -> i -> res)
    -> Decoder a
    -> Decoder b
    -> Decoder c
    -> Decoder d
    -> Decoder e
    -> Decoder f
    -> Decoder g
    -> Decoder h
    -> Decoder i
    -> Decoder res
map9 cb da db dc dd de df dg dh di =
    map8
        (\va vb vc vd ve vf vg vh ->
            { va = va
            , vb = vb
            , vc = vc
            , vd = vd
            , ve = ve
            , vf = vf
            , vg = vg
            , vh = vh
            }
        )
        da
        db
        dc
        dd
        de
        df
        dg
        dh
        |> andThen
            (\t ->
                di |> map (cb t.va t.vb t.vc t.vd t.ve t.vf t.vg t.vh)
            )


map10 :
    (a -> b -> c -> d -> e -> f -> g -> h -> i -> j -> res)
    -> Decoder a
    -> Decoder b
    -> Decoder c
    -> Decoder d
    -> Decoder e
    -> Decoder f
    -> Decoder g
    -> Decoder h
    -> Decoder i
    -> Decoder j
    -> Decoder res
map10 cb da db dc dd de df dg dh di dj =
    map8
        (\va vb vc vd ve vf vg vh ->
            { va = va
            , vb = vb
            , vc = vc
            , vd = vd
            , ve = ve
            , vf = vf
            , vg = vg
            , vh = vh
            }
        )
        da
        db
        dc
        dd
        de
        df
        dg
        dh
        |> andThen
            (\t ->
                map2 (cb t.va t.vb t.vc t.vd t.ve t.vf t.vg t.vh)
                    di
                    dj
            )


fieldWithDefault : String -> t -> Decoder t -> Decoder t
fieldWithDefault fieldName default_ decoder =
    map (Maybe.withDefault default_) (maybe (field fieldName decoder))
