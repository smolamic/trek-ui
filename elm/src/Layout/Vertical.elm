module Layout.Vertical exposing (layout)

import Html.Styled as Html exposing (Html)


layout : List (Html msg) -> Html msg
layout =
    Html.div []
