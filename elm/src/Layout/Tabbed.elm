module Layout.Tabbed exposing (layout)

import Core
import Html.Styled as Html exposing (Html)


layout : (Core.View msg -> Html msg) -> List ( String, Bool ) -> Maybe (Core.View msg) -> Html msg
layout toHtml tabs view =
    Html.div [] []
