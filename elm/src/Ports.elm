{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


port module Ports exposing (clearUser, persistUser, updateUser)

import Core
import Json.Encode as E


port persistUser : E.Value -> Cmd msg


port clearUser : () -> Cmd msg


port updateUser : (E.Value -> msg) -> Sub msg
