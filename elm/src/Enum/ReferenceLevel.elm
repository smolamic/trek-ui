module Enum.ReferenceLevel exposing (ReferenceLevel(..))


type ReferenceLevel
    = Static
    | DynamicTable
    | DynamicNamespace
