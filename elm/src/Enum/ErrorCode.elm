module Enum.ErrorCode exposing (ErrorCode(..))


type ErrorCode
    = GenericException
    | GenericServerError
    | AccessDenied
    | DatabaseConnectionFailed
    | SQLError
    | UnexpectedRequest
    | InvalidAssignment
    | MissingViewKey
    | ExitBeforeCurrentRequest
    | AuthenticationFailure
    | XMLError
    | TableNotConnected
    | DynamicIdentifier
    | MissingKey
    | MissingElement
    | DuplicateKey
    | DuplicateElement
    | FileNotFound
    | TargetMismatch
    | BadBody
    | BadUrl
    | Timeout
    | NetworkError
    | ServerNotFound
    | InvalidInput
    | LoginExpired
    | TypeError
