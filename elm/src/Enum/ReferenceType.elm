module Enum.ReferenceType exposing (ReferenceType(..))


type ReferenceType
    = Row
    | Table
    | Group
