module Enum.RowState exposing (RowState(..))


type RowState
    = Default
    | Editable
    | Readonly
    | Locked
