module Enum.ColumnClass exposing (ColumnClass(..))


type ColumnClass
    = Data
    | Ref
