module Enum.Decode exposing (columnClass, dataType, errorCode, referenceLevel, referenceType, rowState, viewClass)

import Enum.ColumnClass exposing (ColumnClass(..))
import Enum.DataType exposing (DataType(..))
import Enum.ErrorCode exposing (ErrorCode(..))
import Enum.ReferenceLevel exposing (ReferenceLevel(..))
import Enum.ReferenceType exposing (ReferenceType(..))
import Enum.RowState exposing (RowState(..))
import Enum.ViewClass exposing (ViewClass(..))
import Json.Decode as D


dataType : D.Decoder DataType
dataType =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed String

                1 ->
                    D.succeed Int

                2 ->
                    D.succeed Float

                3 ->
                    D.succeed Bool

                4 ->
                    D.succeed Name

                5 ->
                    D.succeed Password

                6 ->
                    D.succeed Timestamp

                7 ->
                    D.succeed Currency

                8 ->
                    D.succeed Sha256hash

                9 ->
                    D.succeed Barcode

                10 ->
                    D.succeed Enum

                11 ->
                    D.succeed Text

                12 ->
                    D.succeed Id

                13 ->
                    D.succeed Sql

                14 ->
                    D.succeed Null

                15 ->
                    D.succeed Check

                16 ->
                    D.succeed Xml

                17 ->
                    D.succeed Object

                _ ->
                    D.fail ("Unknown DataType \"" ++ String.fromInt v ++ "\".")
        )
        D.int


viewClass : D.Decoder ViewClass
viewClass =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed Database

                1 ->
                    D.succeed Sheet

                2 ->
                    D.succeed Custom

                _ ->
                    D.fail ("Unknown ViewClass \"" ++ String.fromInt v ++ "\".")
        )
        D.int


errorCode : D.Decoder ErrorCode
errorCode =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed GenericException

                1 ->
                    D.succeed GenericServerError

                2 ->
                    D.succeed AccessDenied

                3 ->
                    D.succeed DatabaseConnectionFailed

                4 ->
                    D.succeed SQLError

                5 ->
                    D.succeed UnexpectedRequest

                6 ->
                    D.succeed InvalidAssignment

                7 ->
                    D.succeed MissingViewKey

                8 ->
                    D.succeed ExitBeforeCurrentRequest

                9 ->
                    D.succeed AuthenticationFailure

                10 ->
                    D.succeed XMLError

                11 ->
                    D.succeed TableNotConnected

                12 ->
                    D.succeed DynamicIdentifier

                13 ->
                    D.succeed MissingKey

                14 ->
                    D.succeed MissingElement

                15 ->
                    D.succeed DuplicateKey

                16 ->
                    D.succeed DuplicateElement

                17 ->
                    D.succeed FileNotFound

                18 ->
                    D.succeed TargetMismatch

                19 ->
                    D.succeed BadBody

                20 ->
                    D.succeed BadUrl

                21 ->
                    D.succeed Timeout

                22 ->
                    D.succeed NetworkError

                23 ->
                    D.succeed ServerNotFound

                24 ->
                    D.succeed InvalidInput

                25 ->
                    D.succeed LoginExpired

                26 ->
                    D.succeed TypeError

                _ ->
                    D.fail ("Unknown ErrorCode \"" ++ String.fromInt v ++ "\".")
        )
        D.int


rowState : D.Decoder RowState
rowState =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed Default

                1 ->
                    D.succeed Editable

                2 ->
                    D.succeed Readonly

                3 ->
                    D.succeed Locked

                _ ->
                    D.fail ("Unknown RowState \"" ++ String.fromInt v ++ "\".")
        )
        D.int


referenceType : D.Decoder ReferenceType
referenceType =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed Row

                1 ->
                    D.succeed Table

                2 ->
                    D.succeed Group

                _ ->
                    D.fail ("Unknown ReferenceType \"" ++ String.fromInt v ++ "\".")
        )
        D.int


referenceLevel : D.Decoder ReferenceLevel
referenceLevel =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed Static

                1 ->
                    D.succeed DynamicTable

                2 ->
                    D.succeed DynamicNamespace

                _ ->
                    D.fail ("Unknown ReferenceLevel \"" ++ String.fromInt v ++ "\".")
        )
        D.int


columnClass : D.Decoder ColumnClass
columnClass =
    D.andThen
        (\v ->
            case v of
                0 ->
                    D.succeed Data

                1 ->
                    D.succeed Ref

                _ ->
                    D.fail ("Unknown ColumnClass \"" ++ String.fromInt v ++ "\".")
        )
        D.int
