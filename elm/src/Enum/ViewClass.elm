module Enum.ViewClass exposing (ViewClass(..))


type ViewClass
    = Database
    | Sheet
    | Custom
