module Enum.DataType exposing (DataType(..), align, numeric, width)


type DataType
    = String
    | Int
    | Float
    | Bool
    | Name
    | Password
    | Timestamp
    | Currency
    | Sha256hash
    | Barcode
    | Enum
    | Text
    | Id
    | Sql
    | Null
    | Check
    | Xml
    | Object


width : DataType -> Int
width val =
    case val of
        String ->
            3

        Name ->
            2

        Password ->
            2

        Timestamp ->
            2

        Currency ->
            2

        Barcode ->
            2

        Enum ->
            2

        Text ->
            4

        Sql ->
            4

        Xml ->
            4

        Object ->
            3

        _ ->
            1


align : DataType -> String
align val =
    case val of
        Int ->
            "right"

        Float ->
            "right"

        Bool ->
            "center"

        Currency ->
            "right"

        Id ->
            "right"

        _ ->
            "left"


numeric : DataType -> Bool
numeric val =
    case val of
        Int ->
            True

        Float ->
            True

        Currency ->
            True

        _ ->
            False
