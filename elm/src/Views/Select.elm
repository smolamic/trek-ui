{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Views.Select exposing (filters, order)

import Cache
import Dict exposing (Dict)
import RemoteResource
import Views.Core exposing (..)


order : String -> Views -> Maybe Views.Core.Order
order path views =
    Cache.get path views.cache
        |> RemoteResource.toMaybe
        |> Maybe.andThen getOrder


filters : String -> Views -> Maybe (Dict String Filter)
filters path views =
    Cache.get path views.cache
        |> RemoteResource.toMaybe
        |> Maybe.andThen getFilters
