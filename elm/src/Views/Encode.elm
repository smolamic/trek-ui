{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Views.Encode exposing (filters)

import Dict exposing (Dict)
import Json.Encode exposing (..)
import Views.Core exposing (..)


filters : Dict String Filter -> Value
filters =
    Dict.map
        (\column filter ->
            case filter of
                Like value ->
                    [ column ++ " LIKE " ++ value ]

                GreaterThan value ->
                    [ column ++ " > " ++ value ]

                LessThan value ->
                    [ column ++ " < " ++ value ]

                Equal value ->
                    [ column ++ " = " ++ value ]

                Between gt lt ->
                    [ column ++ " > " ++ gt, column ++ " < " ++ lt ]

                Within ge le ->
                    [ column ++ " >= " ++ ge, column ++ " <= " ++ le ]

                All ->
                    []
        )
        >> Dict.values
        >> List.concat
        >> list string
