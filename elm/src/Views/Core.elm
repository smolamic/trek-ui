{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Views.Core exposing (Column, CustomData, DatabaseData, Filter(..), Msg(..), Order, Row, SheetData, Variant, View(..), Views, between, equal, getFilters, getOrder, greaterThan, lessThan, like)

import Browser.Dom as Dom
import Cache exposing (Cache)
import Dict exposing (Dict)
import Enum.DataType exposing (DataType)
import Form.Core exposing (Table)
import Json.Decode as D
import Link.Core exposing (Link)
import Set exposing (Set)
import Time exposing (Posix)


type alias Views =
    { cache : Cache View
    , showLabels : Set String
    , blockedRows : Set String
    , delete : Maybe String
    }


type alias Order =
    { by : String
    , asc : Bool
    }


type View
    = DatabaseView DatabaseData
    | SheetView SheetData
    | CustomView CustomData


type Msg
    = GotView Posix View
    | ViewFailed Posix String
    | GotLabelVisibility String (Result Dom.Error Bool)
    | SheetConnected SheetData
    | CustomConnected CustomData
    | Resize
    | Delete String
    | DeleteCommit
    | DeleteCancel
    | PathChanged Posix
    | Check String Bool Posix
    | OrderBy String String Posix
    | CleanUp Posix
    | Reload String Posix
    | ToggleFilters String Posix
    | SetFilter String String Filter Posix
    | ResetFilter String String Posix
    | Unblock String


type alias SheetData =
    { path : String
    , namespace : String
    , name : String
    , title : String
    , columns : List Column
    , tables : Dict String Table
    , rows : List Row
    , order : Maybe Order
    , filters : Maybe (Dict String Filter)
    }


type Filter
    = Like String
    | GreaterThan String
    | LessThan String
    | Equal String
    | Between String String
    | Within String String
    | All


type alias Variant =
    { value : Int
    , label : String
    }


type alias Column =
    { name : String
    , title : String
    , datatype : DataType
    , unit : Maybe String
    , variants : Maybe (List Variant)
    }


type alias Row =
    { path : String
    , id : Int
    , table : String
    , barcode : String
    , views : List Link
    , data : List String
    , update : Bool
    }


type alias DatabaseData =
    { path : String
    , namespace : String
    , name : String
    , title : String
    , views : List Link
    }


type alias CustomData =
    { path : String
    , namespace : String
    , name : String
    , title : String
    , columns : List Column
    , rows : List (List String)
    }


getOrder : View -> Maybe Order
getOrder view =
    case view of
        SheetView sheetData ->
            sheetData.order

        _ ->
            Nothing


getFilters : View -> Maybe (Dict String Filter)
getFilters view =
    case view of
        SheetView sheetData ->
            sheetData.filters

        _ ->
            Nothing


greaterThan : String -> Filter
greaterThan val =
    if val == "" then
        All

    else
        GreaterThan val


lessThan : String -> Filter
lessThan val =
    if val == "" then
        All

    else
        LessThan val


between : String -> String -> Filter
between gt lt =
    if gt == "" && lt == "" then
        All

    else if gt == "" then
        LessThan lt

    else if lt == "" then
        GreaterThan gt

    else
        Between gt lt


like : String -> Filter
like that =
    if that == "" then
        All

    else
        Like ("%" ++ that ++ "%")


equal : String -> Filter
equal that =
    if that == "" then
        All

    else
        Equal that
