{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Views.Decode exposing (row, view)

import Dict exposing (Dict)
import Enum.Decode exposing (dataType, viewClass)
import Enum.ViewClass as ViewClass
import Form.Decode exposing (table)
import Json.Decode exposing (..)
import Json.Decode.Extra exposing (..)
import Link.Decode exposing (link)
import Set exposing (Set)
import Views.Core exposing (..)


view : Decoder View
view =
    field "viewclass" viewClass
        |> andThen
            (\viewClass ->
                case viewClass of
                    ViewClass.Database ->
                        map DatabaseView databaseData

                    ViewClass.Sheet ->
                        map SheetView sheetData

                    ViewClass.Custom ->
                        map CustomView customData
            )


databaseData : Decoder DatabaseData
databaseData =
    map5 DatabaseData
        (field "path" string)
        (field "namespace" string)
        (field "name" string)
        (field "title" string)
        (field "views" (list link))


order : Decoder Views.Core.Order
order =
    map2 Views.Core.Order
        (field "by" string)
        (field "asc" bool)


addFilter : String -> Filter -> Dict String Filter -> Dict String Filter
addFilter key val =
    Dict.update key
        (\maybeFilter ->
            case ( maybeFilter, val ) of
                ( Just (GreaterThan gt), LessThan lt ) ->
                    Just (Between gt lt)

                ( Just (LessThan lt), GreaterThan gt ) ->
                    Just (Between gt lt)

                ( Nothing, LessThan lt ) ->
                    Just (LessThan lt)

                ( Nothing, GreaterThan gt ) ->
                    Just (GreaterThan gt)

                ( _, Like that ) ->
                    Just (Like that)

                ( _, Equal that ) ->
                    Just (Equal that)

                _ ->
                    Nothing
        )


filters : Decoder (Dict String Filter)
filters =
    list string
        |> andThen
            (List.foldl
                (\str res ->
                    case String.split " " str of
                        [ col, "LIKE", val ] ->
                            map (addFilter col (Like val)) res

                        col :: "LIKE" :: vals ->
                            map (addFilter col (Like (String.join " " vals))) res

                        [ col, ">", val ] ->
                            map (addFilter col (GreaterThan val)) res

                        [ col, "<", val ] ->
                            map (addFilter col (LessThan val)) res

                        [ col, "=", val ] ->
                            map (addFilter col (Equal val)) res

                        col :: "=" :: vals ->
                            map (addFilter col (Equal (String.join " " vals))) res

                        _ ->
                            fail "malformatted filter string"
                )
                (succeed Dict.empty)
            )


sheetData : Decoder SheetData
sheetData =
    map9 SheetData
        (field "path" string)
        (field "namespace" string)
        (field "name" string)
        (field "title" string)
        (field "columns" (list column))
        (field "tables" (dict table))
        (field "rows" (list row))
        (field "order" (nullable order))
        (field "filters" (nullable filters))


customData : Decoder CustomData
customData =
    map6 CustomData
        (field "path" string)
        (field "namespace" string)
        (field "name" string)
        (field "title" string)
        (field "columns" (list column))
        (field "rows" (list (list string)))


row : Decoder Row
row =
    map7 Row
        (field "path" string)
        (field "id" int)
        (field "table" string)
        (field "barcode" string)
        (field "views" (list link))
        (field "data" (list string))
        (field "update" bool)


variants : Decoder (List Variant)
variants =
    list string
        |> map (List.indexedMap Variant)


column : Decoder Column
column =
    map5 Column
        (field "name" string)
        (field "title" string)
        (field "datatype" dataType)
        (field "unit" (nullable string))
        (field "variants" (nullable variants))
