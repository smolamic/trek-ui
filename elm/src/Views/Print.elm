{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Views.Print exposing (customView, sheetView)

import Components
import Core exposing (Model)
import Css
import Css.Media
import Enum.DataType as DataType exposing (DataType)
import Html.Styled as Html exposing (Html)
import Html.Styled.Attributes as Attr
import Html.Styled.Events as Evt
import Json.Encode as E
import Main.Core
import Path
import Set exposing (Set)
import Translation.Core exposing (Translation, trText)
import Views.Core exposing (Column, CustomData, Row, SheetData)


toggleColumn : String -> String -> Bool -> Core.Msg
toggleColumn path column visible =
    if visible then
        Core.MainMsg (Main.Core.ShowInPrint path column)

    else
        Core.MainMsg (Main.Core.HideInPrint path column)


type alias ColumnInfo =
    { name : String
    , title : String
    , datatype : DataType
    , visible : Bool
    }


sheetView : Model -> SheetData -> Set String -> Html Core.Msg
sheetView model sheetData hidden =
    let
        visible column =
            not (Set.member column hidden)

        columns =
            List.map
                (\col ->
                    ColumnInfo col.name col.title col.datatype (visible col.name)
                )
                sheetData.columns
    in
    Html.section []
        [ printMenu model sheetData.path sheetData.title (barcodeColumn (visible "__barcode__") :: columns)
        , Html.section
            [ Attr.css
                [ Css.width (Css.cm 21)
                , Css.padding (Css.cm 2)
                ]
            ]
            [ title model sheetData.title
            , Html.div [] (List.map (row model columns (visible "__barcode__")) sheetData.rows)
            ]
        ]


customView : Model -> CustomData -> Set String -> Html Core.Msg
customView model customData hidden =
    let
        columns =
            List.map
                (\col ->
                    ColumnInfo col.name col.title col.datatype (not (Set.member col.name hidden))
                )
                customData.columns
    in
    Html.section []
        [ printMenu model customData.path customData.title columns
        , Html.section
            [ Attr.css
                [ Css.width (Css.cm 21)
                , Css.padding (Css.cm 2)
                ]
            ]
            [ title model customData.title
            , Html.div [] (List.map (customRow model columns) customData.rows)
            ]
        ]


title : Model -> String -> Html Core.Msg
title model title_ =
    Html.div [ Attr.css [ Css.marginBottom (Css.rem 1) ] ]
        [ Components.heading 4 [ trText model.translation title_ ] ]


customRow : Model -> List ColumnInfo -> List String -> Html Core.Msg
customRow model columns rowData =
    Html.div
        [ Attr.css
            [ Css.border3 (Css.px 1) Css.solid model.config.theme.black
            , Css.marginBottom (Css.rem 1)
            , Css.displayFlex
            , Css.flexDirection Css.row
            , Css.justifyContent Css.flexStart
            , Css.flexWrap Css.wrap
            ]
        ]
        (List.map2 (field model.translation) columns rowData)


row : Model -> List ColumnInfo -> Bool -> Row -> Html Core.Msg
row model columns showBarcode row_ =
    Html.div
        [ Attr.css
            [ Css.border3 (Css.px 1) Css.solid model.config.theme.black
            , Css.marginBottom (Css.rem 1)
            , Css.displayFlex
            , Css.flexDirection Css.row
            , Css.justifyContent Css.flexStart
            ]
        ]
        [ if showBarcode then
            Html.node "trek-barcode"
                [ Attr.property "value" (E.string row_.barcode) ]
                []

          else
            Html.text ""
        , Html.div
            [ Attr.css
                [ Css.displayFlex
                , Css.flexDirection Css.row
                , Css.justifyContent Css.flexStart
                , Css.flexWrap Css.wrap
                ]
            ]
            (List.map2 (field model.translation) columns row_.data)
        ]


field : Translation -> ColumnInfo -> String -> Html Core.Msg
field translation column value =
    if not column.visible then
        Html.text ""

    else if value == "" then
        Html.div [ Attr.css [ Css.width (Css.rem 2) ] ] []

    else
        Html.div
            [ Attr.css
                [ Css.displayFlex
                , Css.flexDirection Css.column
                , Css.alignItems Css.flexStart
                , Css.margin (Css.rem 0.5)
                , Css.width (DataType.width column.datatype * 6 |> toFloat |> Css.rem)
                ]
            ]
            [ Html.p
                [ Attr.css [ Css.fontWeight Css.bold ] ]
                [ trText translation column.title ]
            , Html.p []
                [ Html.text value ]
            ]


columnSwitch : Translation -> String -> ColumnInfo -> Html Core.Msg
columnSwitch translation path col =
    Html.span []
        [ Html.input
            [ Attr.type_ "checkbox"
            , Attr.checked col.visible
            , Evt.onCheck (toggleColumn path col.name)
            , Attr.id ("toggle-" ++ col.name)
            ]
            []
        , Html.label [ Attr.for ("toggle-" ++ col.name) ] [ trText translation col.title ]
        ]


barcodeColumn : Bool -> ColumnInfo
barcodeColumn visible =
    { name = "__barcode__"
    , title = "Barcode"
    , datatype = DataType.Barcode
    , visible = visible
    }


printMenu :
    Model
    -> String
    -> String
    -> List ColumnInfo
    -> Html Core.Msg
printMenu model path title_ columns =
    Html.nav
        [ Attr.css
            [ Css.Media.withMedia
                [ Css.Media.not Css.Media.screen [] ]
                [ Css.display Css.none ]
            ]
        ]
        [ Components.heading 3
            [ trText model.translation "Showing Print-View for "
            , trText model.translation title_
            ]
        , Html.a [ Attr.href (Path.dropRight 1 model.url.path) ]
            [ Components.primaryButton model True [ trText model.translation "Go Back" ]
            ]
        , Html.div [] (List.map (columnSwitch model.translation path) columns)
        ]
