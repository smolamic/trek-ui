{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Decode exposing (scene)

import Core exposing (..)
import Json.Decode exposing (..)


scene : Decoder Scene
scene =
    map2 Scene
        (field "width" float)
        (field "height" float)
