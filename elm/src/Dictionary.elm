{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Dictionary exposing (dictionary)

import Dict exposing (Dict)


dictionary : Dict String String
dictionary =
    Dict.empty
