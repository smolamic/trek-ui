{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Update exposing (andThen, cmd)


andThen : (model -> ( model, Cmd msg )) -> ( model, Cmd msg ) -> ( model, Cmd msg )
andThen update ( oldModel, oldCmd ) =
    let
        ( nextModel, nextCmd ) =
            update oldModel
    in
    ( nextModel
    , Cmd.batch
        [ oldCmd
        , nextCmd
        ]
    )


cmd : (model -> Cmd msg) -> ( model, Cmd msg ) -> ( model, Cmd msg )
cmd makeCmd ( model, oldCmd ) =
    ( model, Cmd.batch [ makeCmd model, oldCmd ] )
