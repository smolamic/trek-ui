{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Response exposing (Response)

import Enum.ErrorCode exposing (ErrorCode)


type Response data errorData
    = Success data
    | BackendError
        { code : ErrorCode
        , message : String
        , file : String
        , line : Int
        , trace : String
        }
    | ApiError
        { code : ErrorCode
        , message : String
        , file : String
        }


makeBackendError : ErrorCode -> String -> String -> Int -> String -> Response data
makeBackendError code message file line trace =
    BackendError
        { code = code
        , message = message
        , file = file
        , line = line
        , trace = trace
        }


makeApiError : ErrorCode -> String -> String -> Response data
makeApiError code message file =
    ApiError
        { code = code
        , message = message
        , file = file
        }
