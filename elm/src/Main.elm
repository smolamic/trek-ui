{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Main exposing (main)

import Browser
import Browser.Navigation as Nav
import Core
import Html.Styled as Html
import Json.Decode as D
import Theme exposing (Theme)
import Translation exposing (Translation)
import Trek
import Trek.Props as Props
import Url


type Msg
    = NoOp


type alias Model =
    ()


init : D.Value -> Url.Url -> Nav.Key -> ( Model, Cmd Msg )
init flags url key =
    ( (), Cmd.none )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        NoOp ->
            ( model, Cmd.none )


scene : Core.Scene
scene =
    { width = 1920
    , height = 1080
    }


meta : Core.Meta
meta =
    { scene = scene
    , translation = Translation.noTranslation
    , theme = Theme.defaultTheme
    }


viewModel : Trek.View Msg
viewModel =
    Trek.sheetView
        (Props.columns
            [ Trek.stringColumn (Props.title "Column A" >> Props.length 256)
            , Trek.stringColumn (Props.title "Column B")
            ]
            >> Props.rows [ Trek.activeRow (Props.data [ "Value A", "Value B" ]) ]
        )


view : Model -> Browser.Document Msg
view _ =
    Browser.Document
        "Trek"
        [ Html.toUnstyled (Trek.toHtml meta viewModel) ]


main =
    Browser.application
        { init = init
        , update = update
        , view = view
        , subscriptions = \_ -> Sub.batch []
        , onUrlRequest = \_ -> NoOp
        , onUrlChange = \_ -> NoOp
        }
