{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Link exposing (button, findActive, isActive, map, plain, styled, tab, tabNavigation)

import Components
import Config.Core exposing (Palette, Theme)
import Core
import Css
import Html.Styled as Html exposing (Attribute, Html)
import Html.Styled.Attributes as Attr
import Link.Core exposing (..)
import Translation.Core exposing (Translation, trText)


isActive : String -> Link -> Bool
isActive path link =
    String.startsWith link.href path


findActive : String -> List Link -> Maybe Link
findActive path links =
    List.foldr
        (\link result ->
            if isActive path link then
                Just link

            else
                result
        )
        Nothing
        links


map : (String -> String) -> Link -> Link
map callback link =
    { link | href = callback link.href }


plain : Translation -> Link -> Html msg
plain translation { href, title } =
    Html.a [ Attr.href href ] [ trText translation title ]


styled : Translation -> List Css.Style -> Link -> Html msg
styled translation styles { href, title } =
    Html.styled Html.a styles [ Attr.href href ] [ trText translation title ]


button : Core.Model -> Bool -> Link -> Html Core.Msg
button model enabled { href, title } =
    Html.a [ Attr.href href ]
        [ Components.button model enabled [ trText model.translation title ] ]


primaryButton : Core.Model -> Bool -> Link -> Html Core.Msg
primaryButton model enabled { href, title } =
    Html.a [ Attr.href href ]
        [ Components.primaryButton model enabled [ trText model.translation title ] ]


tab : Core.Model -> (Link -> Bool) -> Link -> Html msg
tab model isActive_ link =
    Html.li
        [ Attr.css
            (if isActive_ link then
                [ Css.padding3 (Css.rem 0.5) (Css.rem 0.7) (Css.rem 0.7)
                , Css.borderTop3 (Css.rem 0.2) Css.solid model.config.theme.primary.medium
                , Css.borderLeft3 (Css.rem 0.1) Css.solid model.config.theme.grey.light
                , Css.borderRight3 (Css.rem 0.1) Css.solid model.config.theme.grey.light
                ]

             else
                [ Css.padding (Css.rem 0.7)
                , Css.borderBottom3 (Css.rem 0.1) Css.solid model.config.theme.grey.light
                ]
            )
        ]
        [ Html.a
            [ Attr.href link.href
            , Attr.css
                [ Css.color model.config.theme.black
                , Css.outline Css.none
                , Css.property "user-select" "none"
                ]
            ]
            [ trText model.translation link.title ]
        ]


tabNavigation : Core.Model -> List Link -> Html msg
tabNavigation model links =
    let
        isActive_ link =
            case List.head links of
                Just first ->
                    case findActive model.url.path links of
                        Just active ->
                            link == active

                        Nothing ->
                            link == first

                Nothing ->
                    False
    in
    Html.ul
        [ Attr.css
            [ Css.displayFlex
            , Css.flexDirection Css.row
            , Css.overflowX Css.auto
            , Css.after
                [ Css.borderBottom3 (Css.rem 0.1) Css.solid model.config.theme.grey.light
                , Css.flex3 (Css.num 1) (Css.num 1) (Css.rem 0.2)
                , Css.property "content" "''"
                ]
            , Css.before
                [ Css.borderBottom3 (Css.rem 0.1) Css.solid model.config.theme.grey.light
                , Css.flex3 (Css.num 0) (Css.num 1) (Css.rem 0.2)
                , Css.property "content" "''"
                ]
            ]
        ]
        (List.map (tab model isActive_) links)
