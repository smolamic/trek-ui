{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Link.Core exposing (Link)


type alias Link =
    { href : String
    , title : String
    }
