{-
   @package Trek
   @author Michel Smola
   @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
-}


module Link.Decode exposing (link, relativeTo)

import Json.Decode exposing (..)
import Link.Core exposing (..)


link : Decoder Link
link =
    map2 Link
        (index 0 string)
        (index 1 string)


relativeTo : String -> Decoder Link
relativeTo base =
    map2 Link
        (index 0 (map (\rel -> base ++ "/" ++ rel) string))
        (index 1 string)
