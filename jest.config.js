module.exports = {
  preset: "ts-jest",
  transformIgnorePatterns: ["/node_modules/(?!@storybook/addon-storyshots)"],
  testPathIgnorePatterns: ["/dist"],
  setupFilesAfterEnv: ["<rootDir>/src/setupTests.ts"],
};
