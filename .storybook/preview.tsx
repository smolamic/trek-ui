import { Reset } from "../src/Reset";

export const actions = { argTypesRegex: "^on[A-Z].*" };
export const controls = {
  matchers: {
    color: /(background|color)$/i,
    date: /Date$/,
  },
};
export const decorators = [
  (Story: React.ElementType) => (
    <>
      <Reset />
      <Story />
    </>
  ),
];
