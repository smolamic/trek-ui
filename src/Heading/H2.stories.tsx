import { Story, Meta } from "@storybook/react";

import { H2 } from "./Heading";

const meta: Meta = {
  title: "Heading 2",
  component: H2,
};

export default meta;

const Template: Story<{ label: string }> = ({ label }) => <H2>{label}</H2>;

export const Default = Template.bind({});
Default.args = { label: "Heading 2" };
