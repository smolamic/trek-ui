import { Story, Meta } from "@storybook/react";

import { H1 } from "./Heading";

const meta: Meta = {
  title: "Heading 1",
  component: H1,
};

export default meta;

const Template: Story<{ label: string }> = ({ label }) => <H1>{label}</H1>;

export const Default = Template.bind({});
Default.args = { label: "Heading 1" };
