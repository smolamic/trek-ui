import styled from "styled-components";

export const H1 = styled.h1`
  font-size: 5rem;
  padding: 1rem 0;
`;

export const H2 = styled.h2`
  font-size: 3rem;
  padding: 0.6rem 0;
`;

export const H3 = styled.h3`
  font-size: 2rem;
  padding: 0.5rem 0;
`;

export const H4 = styled.h4`
  font-size: 1.5rem;
  padding: 0.4rem 0;
`;

export const H5 = styled.h5`
  font-size: 1.3rem;
  padding: 0.3rem 0;
`;

export const H6 = styled.h6`
  font-size: 1.2rem;
  padding: 0.2rem 0;
`;
