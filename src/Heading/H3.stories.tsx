import { Story, Meta } from "@storybook/react";

import { H3 } from "./Heading";

const meta: Meta = {
  title: "Heading 3",
  component: H3,
};

export default meta;

const Template: Story<{ label: string }> = ({ label }) => <H3>{label}</H3>;

export const Default = Template.bind({});
Default.args = { label: "Heading 3" };
