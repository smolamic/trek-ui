import { Story, Meta } from "@storybook/react";

import { H6 } from "./Heading";

const meta: Meta = {
  title: "Heading 6",
  component: H6,
};

export default meta;

const Template: Story<{ label: string }> = ({ label }) => <H6>{label}</H6>;

export const Default = Template.bind({});
Default.args = { label: "Heading 6" };
