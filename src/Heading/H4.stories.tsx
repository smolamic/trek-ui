import { Story, Meta } from "@storybook/react";

import { H4 } from "./Heading";

const meta: Meta = {
  title: "Heading 4",
  component: H4,
};

export default meta;

const Template: Story<{ label: string }> = ({ label }) => <H4>{label}</H4>;

export const Default = Template.bind({});
Default.args = { label: "Heading 4" };
