import { Story, Meta } from "@storybook/react";

import { H5 } from "./Heading";

const meta: Meta = {
  title: "Heading 5",
  component: H5,
};

export default meta;

const Template: Story<{ label: string }> = ({ label }) => <H5>{label}</H5>;

export const Default = Template.bind({});
Default.args = { label: "Heading 5" };
