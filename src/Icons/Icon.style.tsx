import styled from "styled-components";

export type RotateProps = {
  up?: boolean;
  down?: boolean;
  left?: boolean;
  right?: boolean;
};

const rotation = ({ up, down, left, right }: RotateProps): string => {
  switch (true) {
    case right:
      return "90deg";
    case down:
      return "180deg";
    case left:
      return "270deg";
    case up:
    default:
      return "0deg";
  }
};

export const Icon = styled.span<{}>`
  display: inline-flex;
  justify-content: center;
  align-items: center;
`;

export const Rotate = styled(Icon)<RotateProps>`
  transform: rotate(${rotation});
  transition: transform 150ms;
`;
