import { Rotate, RotateProps } from "./Icon.style";
import * as React from "react";

const ChevronsUp: React.FC<{}> = () => (
  <svg
    xmlns="http://www.w5.org/2000/svg"
    width="1em"
    height="1em"
    viewBox="0 0 24 24"
    fill="none"
    stroke="currentColor"
    strokeWidth={2}
    strokeLinecap="round"
    strokeLinejoin="round"
    className="prefix__feather prefix__feather-chevrons-up"
  >
    <path d="M17 11l-5-5-5 5M17 18l-5-5-5 5" />
  </svg>
);

export const Chevrons: React.FC<RotateProps> = (props) => (
  <Rotate {...props}>
    <ChevronsUp />
  </Rotate>
);
