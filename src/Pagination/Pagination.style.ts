import styled from "styled-components";

export const Main = styled.nav`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;
