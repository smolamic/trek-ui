import { Story, Meta } from "@storybook/react";

import { Pagination, PaginationProps } from "./Pagination";

const meta: Meta = {
  title: "Pagination",
  component: Pagination,
};

export default meta;

const Template: Story<PaginationProps> = (args) => <Pagination {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    pages: 20,
    activePage: 7,
  },
});

export const Empty = Object.assign(Template.bind({}), {
  args: {
    pages: 0,
    activePage: 0,
  },
});

export const FewPages = Object.assign(Template.bind({}), {
  args: {
    pages: 2,
    activePage: 2,
  },
});

export const AtStart = Object.assign(Template.bind({}), {
  args: {
    pages: 20,
    activePage: 1,
  },
});

export const AtEnd = Object.assign(Template.bind({}), {
  args: {
    pages: 20,
    activePage: 20,
  },
});
