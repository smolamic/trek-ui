import { Main } from "./Pagination.style";
import * as B from "../Button";
import { Chevron, Chevrons } from "../Icons";

export type PaginationProps = {
  pages: number;
  activePage: number;
  renderButton?: (props: B.ButtonProps, page: number) => React.ReactNode;
  onChangePage?: (page: number) => void;
};

function calculatePagesToShow(pages: number, activePage: number): number[] {
  switch (pages) {
    case 0:
      return [];
    case 1:
      return [1];
    case 2:
      return [1, 2];
    case 3:
      return [1, 2, 3];
    default:
      switch (activePage) {
        case 1:
          return [1, 2, 3];
        case pages:
          return [pages - 2, pages - 1, pages];
        default:
          return [activePage - 1, activePage, activePage + 1];
      }
  }
}

function defaultRenderButton(props: B.ButtonProps): JSX.Element {
  return <B.Button {...props} />;
}

export const Pagination: React.FC<PaginationProps> = ({
  pages,
  activePage,
  renderButton = defaultRenderButton,
  onChangePage,
}) => {
  const canGoLeft = activePage > 1 ? true : undefined;
  const canGoRight = activePage < pages ? true : undefined;

  function changePage(
    page: number
  ): undefined | React.MouseEventHandler<HTMLButtonElement> {
    return onChangePage && (() => onChangePage(page));
  }

  const pagesToShow = calculatePagesToShow(pages, activePage);

  return (
    <Main role="navigation" aria-label="Pagination Navigation">
      {renderButton(
        {
          key: "first",
          disabled: !canGoLeft,
          onClick: canGoLeft && changePage(1),
          "aria-label": "Go to first page",
          children: <Chevrons left />,
        },
        1
      )}
      {renderButton(
        {
          key: "previous",
          disabled: !canGoLeft,
          onClick: canGoLeft && changePage(activePage - 1),
          "aria-label": "Go to previous page",
          children: <Chevron left />,
        },
        Math.min(activePage - 1, 1)
      )}
      {pagesToShow.map((page) =>
        renderButton(
          {
            key: page,
            primary: page === activePage,
            onClick: page !== activePage ? changePage(page) : undefined,
            "aria-label": `Go to page ${page}`,
            "aria-current": page === activePage && "page",
            children: page,
          },
          page
        )
      )}
      {renderButton(
        {
          key: "next",
          disabled: !canGoRight,
          onClick: canGoRight && changePage(activePage + 1),
          "aria-label": "Go to next page",
          children: <Chevron right />,
        },
        Math.max(activePage + 1, pages)
      )}
      {renderButton(
        {
          key: "last",
          disabled: !canGoRight,
          onClick: canGoRight && changePage(pages),
          "aria-label": "Go to last page",
          children: <Chevrons right />,
        },
        pages
      )}
    </Main>
  );
};
