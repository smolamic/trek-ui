import {
  HasTheme,
  HasPalette,
  HasSlimVersion,
  HasFilledVersion,
  CanGrow,
} from "./Props";
import { defaultTheme } from "./Theme";
import { Palette, defaultColors, PaletteTheme } from "./Colors";
import { defaultBreakpoints } from "./Breakpoints";
import { css } from "styled-components";
import { transparentize } from "polished";

const theme = (props: HasTheme) => props.theme || defaultTheme;

const colors = (props: HasTheme) => theme(props).color || defaultColors;

export const palette = Object.assign(
  (props: HasPalette): Palette => {
    const name = props.primary
      ? "primary"
      : props.secondary
      ? "secondary"
      : "grey";
    return colors(props).palette[name];
  },
  {
    grey: (props: HasTheme): Palette => colors(props).palette.grey,
    primary: (props: HasTheme): Palette => colors(props).palette.primary,
    secondary: (props: HasTheme): Palette => colors(props).palette.secondary,
  }
);

export const hasActivePalette = (props: HasPalette): boolean => {
  return (
    props.primary === true || props.secondary === true || props.grey === true
  );
};

export const shade = {
  darker: (props: HasPalette) => palette(props).darker,
  dark: (props: HasPalette) => palette(props).dark,
  medium: (props: HasPalette) => palette(props).medium,
  light: (props: HasPalette) => palette(props).light,
  lighter: (props: HasPalette) => palette(props).lighter,
  grey: {
    darker: (props: HasTheme) => palette.grey(props).darker,
    dark: (props: HasTheme) => palette.grey(props).dark,
    medium: (props: HasTheme) => palette.grey(props).medium,
    light: (props: HasTheme) => palette.grey(props).light,
    lighter: (props: HasTheme) => palette.grey(props).lighter,
  },
  primary: {
    darker: (props: HasTheme) => palette.primary(props).darker,
    dark: (props: HasTheme) => palette.primary(props).dark,
    medium: (props: HasTheme) => palette.primary(props).medium,
    light: (props: HasTheme) => palette.primary(props).light,
    lighter: (props: HasTheme) => palette.primary(props).lighter,
  },
  secondary: {
    darker: (props: HasTheme) => palette.secondary(props).darker,
    dark: (props: HasTheme) => palette.secondary(props).dark,
    medium: (props: HasTheme) => palette.secondary(props).medium,
    light: (props: HasTheme) => palette.secondary(props).light,
    lighter: (props: HasTheme) => palette.secondary(props).lighter,
  },
};

export const semiTransparentBackgroundColor = (props: HasPalette) =>
  transparentize(0.9, shade.dark(props));

const breakpoints = (props: HasTheme) =>
  theme(props).breakpoints || defaultBreakpoints;

export const bp = {
  isDesktop: (props: HasTheme) => `(min-width: ${breakpoints(props).lg}px)`,
};

export const isSlim = (props: HasSlimVersion) => props.slim;

export const isFilled = (props: HasFilledVersion) => props.filled;

export const grow = (props: CanGrow) => {
  switch (props.grow) {
    case undefined:
      return css`
        flex-grow: 0;
      `;
    case true:
      return css`
        flex-grow: 1;
      `;
    case false:
      return css`
        flex-grow: 0;
      `;
    default:
      return css`
        flex-grow: ${props.grow};
      `;
  }
};
