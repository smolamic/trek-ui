export type Palette = {
  darker: string;
  dark: string;
  medium: string;
  light: string;
  lighter: string;
};

export type PaletteTheme = {
  primary: Palette;
  secondary: Palette;
  grey: Palette;
};

export type ColorTheme = {
  palette: PaletteTheme;
};

export const defaultColors: ColorTheme = {
  palette: {
    primary: {
      darker: "rgb(11, 158, 143)",
      dark: "rgb(14,187,169)",
      medium: "rgb(16,218,198)",
      light: "rgb(130,244,236)",
      lighter: "rgb(194,248,248)",
    },
    secondary: {
      darker: "rgb(17,48,142)",
      dark: "rgb(21,104,167)",
      medium: "rgb(24,120,202)",
      light: "rgb(60,152,232)",
      lighter: "rgb(142,196,255)",
    },
    grey: {
      darker: "rgb(89,89,89)",
      dark: "rgb(116,116,116)",
      medium: "rgb(157,157,157)",
      light: "rgb(187,187,187)",
      lighter: "rgb(224,224,224)",
    },
  },
};
