import { Theme } from "./Theme";

export type HasTheme = {
  theme?: Theme;
};

export type HasSlimVersion = {
  slim?: boolean;
};

export type HasFilledVersion = {
  filled?: boolean;
};

export type HasPalette = HasTheme & {
  primary?: boolean;
  secondary?: boolean;
  grey?: boolean;
};

export type CanGrow = {
  grow?: boolean | number;
};
