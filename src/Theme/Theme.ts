import { ColorTheme, defaultColors } from "./Colors";
import { Breakpoints, defaultBreakpoints } from "./Breakpoints";

export type Theme = {
  color: ColorTheme;
  breakpoints: Breakpoints;
};

export const defaultTheme: Theme = {
  color: defaultColors,
  breakpoints: defaultBreakpoints,
};
