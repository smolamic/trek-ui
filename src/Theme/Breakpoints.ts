export type Breakpoints = {
  xl: number;
  lg: number;
  md: number;
  sm: number;
};

export const defaultBreakpoints: Breakpoints = {
  xl: 1920,
  lg: 1024,
  md: 768,
  sm: 576,
};
