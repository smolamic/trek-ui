import { Story, Meta } from "@storybook/react";

import { TFoot, TFootProps } from "./Table";

const meta: Meta = {
  title: "TFoot",
  component: TFoot,
};

export default meta;

const Template: Story<TFootProps> = (args) => <TFoot {...args} />;

export const Default = Object.assign(() => <TFoot>This is a Footer</TFoot>, {
  args: {},
});
