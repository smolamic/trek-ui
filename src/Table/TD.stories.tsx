import { Story, Meta } from "@storybook/react";

import { TD, TDProps } from "./Cell";

const meta: Meta = {
  title: "TD",
  component: TD,
};

export default meta;

const Template: Story<TDProps> = (args) => <TD {...args} />;

export const Text = Object.assign(Template.bind({}), {
  args: { children: "test", span: 2 },
});

export const Number = Object.assign(Template.bind({}), {
  args: { children: "1337", align: "right" },
});

export const WithLabel = Object.assign(Template.bind({}), {
  args: { children: "content", span: 2, label: "label" },
});
