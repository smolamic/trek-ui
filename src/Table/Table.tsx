import * as React from "react";
import { Foot, FootProps, Main, Head, Body } from "./Table.style";
import { ShowLabelsContext } from "./Context";

export type TableProps = {
  children: React.ReactNode;
  showLabels?: boolean;
};

export const Table: React.FC<TableProps> = ({
  children,
  showLabels = true,
}) => {
  return (
    <ShowLabelsContext.Provider value={showLabels}>
      <Main role="table">{children}</Main>
    </ShowLabelsContext.Provider>
  );
};

export type THeadProps = {
  children: React.ReactNode;
};

export const THead: React.FC<THeadProps> = ({ children }) => (
  <Head role="rowgroup">{children}</Head>
);

export type TBodyProps = {
  children: React.ReactNode;
};

export const TBody: React.FC<TBodyProps> = ({ children }) => (
  <Body role="rowgroup">{children}</Body>
);

export type TFootProps = FootProps & {
  children: React.ReactNode;
};

export const TFoot: React.FC<TFootProps> = ({ children, ...props }) => (
  <Foot role="rowgroup" {...props}>
    {children}
  </Foot>
);
