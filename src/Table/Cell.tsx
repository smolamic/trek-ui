import * as React from "react";
import { Chevron } from "../Icons";
import {
  Data,
  HeadTitle,
  CellProps,
  Head,
  OrderCaret,
  DataLabel,
  DataContent,
} from "./Cell.style";
import { Order } from "./Models";
import { ShowLabelsContext } from "./Context";

export type TDProps = CellProps & {
  children: string;
  label?: string;
};

export const TD: React.FC<TDProps> = ({ children, label, ...props }) => {
  const showLabels = React.useContext(ShowLabelsContext);
  return (
    <Data role="cell" {...props}>
      {showLabels && label && <DataLabel>{label}</DataLabel>}
      <DataContent>{children}</DataContent>
    </Data>
  );
};

export type THProps = CellProps & {
  children: string;
  order?: Order;
};

export const TH = React.forwardRef<HTMLDivElement, THProps>(
  ({ children, order, ...props }, ref) => {
    const caret =
      order === "asc" ? (
        <Chevron up />
      ) : order === "desc" ? (
        <Chevron down />
      ) : null;

    return (
      <Head role="columnheader" ref={ref} {...props}>
        <OrderCaret>{caret}</OrderCaret>
        <HeadTitle>{children}</HeadTitle>
      </Head>
    );
  }
);
