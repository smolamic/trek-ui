import * as React from "react";

export const ShowLabelsContext = React.createContext<boolean>(false);
