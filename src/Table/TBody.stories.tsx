import { Story, Meta } from "@storybook/react";

import { TBody, TBodyProps } from "./Table";
import * as TR from "./TR.stories";

const meta: Meta = {
  title: "TBody",
  component: TBody,
};

export default meta;

const Template: Story<TBodyProps> = (args) => <TBody {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TR.Default key="1" {...TR.Default.args} />,
      <TR.Default key="2" {...TR.Default.args} />,
      <TR.Expanded key="3" {...TR.Expanded.args} />,
      <TR.Default key="4" {...TR.Default.args} />,
    ],
  },
});

export const Mobile = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TR.WithLabels key="1" {...TR.WithLabels.args} />,
      <TR.WithLabels key="2" {...TR.WithLabels.args} />,
      <TR.WithLabels key="3" {...TR.WithLabels.args} />,
    ],
  },
});
