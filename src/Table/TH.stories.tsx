import { Story, Meta } from "@storybook/react";

import { TH, THProps } from "./Cell";

const meta: Meta = {
  title: "TH",
  component: TH,
};

export default meta;

const Template: Story<THProps> = (args) => <TH {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: { children: "Column Title", span: 2 },
});

export const OrderedAsc = Object.assign(Template.bind({}), {
  args: { children: "In Ascending Order", order: "asc", span: 2 },
});

export const OrderedDesc = Object.assign(Template.bind({}), {
  args: { children: "In Descending Order", order: "desc", span: 2 },
});

export const Number = Object.assign(Template.bind({}), {
  args: { children: "Number Column", align: "right" },
});

export const NumberOrderedDesc = Object.assign(Template.bind({}), {
  args: {
    children: "Number in Descending Order",
    order: "desc",
    align: "right",
  },
});
