import styled, { css } from "styled-components";
import { shade } from "../Theme";
import {
  HasSlimVersion,
  HasPalette,
  hasActivePalette,
  semiTransparentBackgroundColor,
} from "../Theme";

export type RowProps = HasPalette & { transparent?: boolean };

export const Row = styled.div<RowProps>`
  display: flex;
  flex-direction: column;
  border-top 1px solid white;
  ${({ transparent = false }) =>
    !transparent &&
    css`
      background-color: ${semiTransparentBackgroundColor};
    `}
  margin-top: 1px;
`;

export type HeadProps = HasPalette;

export const Head = styled.div<HeadProps>`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  flex-wrap: wrap;
  box-sizing: border-box;
  position: relative;
  ${(props) =>
    hasActivePalette(props) &&
    css`
      color: ${shade.dark};
    `}
`;

export const Body = styled.div`
  display: flex;
  flex-direction: column;
  border-left: 1rem solid ${shade.primary.medium};
  padding-left: 0.2rem;
`;
