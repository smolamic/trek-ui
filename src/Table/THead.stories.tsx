import { Story, Meta } from "@storybook/react";

import { THead, THeadProps } from "./Table";
import * as TR from "./TR.stories";

const meta: Meta = {
  title: "THead",
  component: THead,
};

export default meta;

const Template: Story<THeadProps> = (args) => <THead {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    children: [<TR.Header key="header" {...TR.Header.args} />],
  },
});

export const WithButtons = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TR.Header key="header" {...TR.Header.args} />,
      <TR.Buttons key="buttons" {...TR.Buttons.args} />,
    ],
  },
});
