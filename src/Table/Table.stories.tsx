import { Story, Meta } from "@storybook/react";

import { Table, TableProps } from "./Table";
import * as THead from "./THead.stories";
import * as TBody from "./TBody.stories";
import * as TFoot from "./TFoot.stories";
import * as TR from "./TR.stories";
import * as TRHead from "./TRHead.stories";
import * as TRBody from "./TRBody.stories";
import * as TH from "./TH.stories";
import { H4 } from "../Heading";

const meta: Meta = {
  title: "Table",
  component: Table,
};

export default meta;

const Template: Story<TableProps> = (args) => <Table {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    children: [
      <THead.Default key="head" {...THead.Default.args} />,
      <TBody.Default key="body" {...TBody.Default.args} />,
      <TFoot.Default key="foot" {...TFoot.Default.args} />,
    ],
  },
});

export const WithButtons = Object.assign(Template.bind({}), {
  args: {
    children: [
      <THead.WithButtons key="head" {...THead.WithButtons.args} />,
      <TBody.Default key="body" {...TBody.Default.args} />,
    ],
  },
});

export const Nested = Object.assign(Template.bind({}), {
  args: {
    children: [
      <THead.WithButtons key="head" {...THead.WithButtons.args} />,
      <TBody.Default key="body" {...TBody.Default.args}>
        <TR.Default {...TR.Default.args} />
        <TR.Default {...TR.Default.args} />
        <TR.Expanded {...TR.Expanded.args}>
          <TRHead.Active {...TRHead.Active.args} />
          <TRBody.Default {...TRBody.Default.args}>
            <H4>Subtable Title</H4>
            <Default {...Default.args} />
          </TRBody.Default>
        </TR.Expanded>
      </TBody.Default>,
    ],
  },
});

export const Mobile = Object.assign(Template.bind({}), {
  args: {
    children: [
      <THead.Default key="head" {...THead.default.args}>
        <TR.Header {...TR.Header.args}>
          <TRHead.Header {...TRHead.Header.args}>
            <TH.Default {...TH.Default.args} />
            <TH.Default {...TH.Default.args} />
            <TH.Default {...TH.Default.args} />
          </TRHead.Header>
        </TR.Header>
      </THead.Default>,
      <TBody.Mobile key="body" {...TBody.Mobile.args} />,
    ],
  },
});
