import { Story, Meta } from "@storybook/react";

import { TRHead, TRHeadProps } from "./Row";
import * as TD from "./TD.stories";
import * as TH from "./TH.stories";
import { Button } from "../Button";

const meta: Meta = {
  title: "TRHead",
  component: TRHead,
};

export default meta;

const Template: Story<TRHeadProps> = (args) => <TRHead {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TD.Text key="1" {...TD.Text.args} />,
      <TD.Number key="2" {...TD.Number.args} />,
      <TD.Number key="3" {...TD.Number.args}>
        42
      </TD.Number>,
    ],
  },
});

export const Active = Object.assign(Template.bind({}), {
  args: {
    primary: true,
    children: [
      <TD.Text key="1" {...TD.Text.args} />,
      <TD.Number key="2" {...TD.Number.args} />,
      <TD.Number key="3" {...TD.Number.args}>
        42
      </TD.Number>,
    ],
  },
});

export const Header = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TH.Default key="1" {...TH.Default.args} />,
      <TH.NumberOrderedDesc key="2" {...TH.NumberOrderedDesc.args} />,
      <TH.Number key="3" {...TH.Number.args} />,
    ],
  },
});

export const Buttons = Object.assign(Template.bind({}), {
  args: {
    children: [
      <Button key="1">Small Button</Button>,
      <Button key="2" primary grow>
        Growing Button
      </Button>,
    ],
  },
});

export const WithLabels = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TD.WithLabel key="1" {...TD.WithLabel.args} />,
      <TD.WithLabel key="2" {...TD.WithLabel.args} />,
      <TD.WithLabel key="3" {...TD.WithLabel.args} />,
    ],
  },
});
