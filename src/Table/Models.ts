export type Order = "asc" | "desc";

export type Value =
  | string
  | number
  | boolean
  | { [key: string]: Value }
  | Value[];
