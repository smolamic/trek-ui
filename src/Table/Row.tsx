import * as React from "react";
import { Row, RowProps, Head, HeadProps, Body } from "./Row.style";

export type TRProps = RowProps & {
  children: React.ReactNode;
};

export const TR: React.FC<TRProps> = ({ children, ...props }) => (
  <Row role="row" {...props}>
    {children}
  </Row>
);

export type TRHeadProps = HeadProps & {
  children: React.ReactNode;
};

function childNodeHasOffsetLeft(
  node: ChildNode
): node is ChildNode & { offsetLeft: number } {
  return "offsetLeft" in node;
}

export const TRHead = React.forwardRef<HTMLDivElement, TRHeadProps>(
  ({ children, ...props }, ref) => (
    <Head ref={ref} {...props}>
      {children}
    </Head>
  )
);

export type TRBodyProps = {
  children: React.ReactNode;
};

export const TRBody: React.FC<TRBodyProps> = ({ children }) => (
  <Body>{children}</Body>
);
