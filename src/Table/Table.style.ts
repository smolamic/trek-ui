import styled, { css } from "styled-components";
import { HasPalette, shade, semiTransparentBackgroundColor } from "../Theme";

export const Main = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Head = styled.div`
  display: flex;
  flex-direction: column;
  border-bottom: 1px solid ${shade.grey.dark};
`;

export const Body = styled.div`
  display: flex;
  flex-direction: column;
`;

export type FootProps = HasPalette & { transparent?: boolean };

export const Foot = styled.div<FootProps>`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  min-height: 2rem;
  border-top: 1px solid ${shade.grey.dark};

  ${({ transparent = false }) =>
    !transparent &&
    css`
      background-color: ${semiTransparentBackgroundColor};
    `}
`;
