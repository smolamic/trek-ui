import { Story, Meta } from "@storybook/react";

import { TR, TRProps } from "./Row";
import * as TRHead from "./TRHead.stories";
import * as TRBody from "./TRBody.stories";
import { Button } from "../Button";

const meta: Meta = {
  title: "TR",
  component: TR,
};

export default meta;

const Template: Story<TRProps> = (args) => <TR {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    children: [<TRHead.Default key="head" {...TRHead.Default.args} />],
  },
});

export const Active = Object.assign(Template.bind({}), {
  args: {
    children: [<TRHead.Active key="head" {...TRHead.Active.args} />],
  },
});

export const Expanded = Object.assign(Template.bind({}), {
  args: {
    children: [
      <TRHead.Active key="head" {...TRHead.Active.args} />,
      <TRBody.Default key="body" {...TRBody.Default.args} />,
    ],
  },
});

export const Header = Object.assign(Template.bind({}), {
  args: {
    children: [<TRHead.Header key="head" {...TRHead.Header.args} />],
  },
});

export const Buttons = Object.assign(Template.bind({}), {
  args: {
    children: [<TRHead.Buttons key="head" {...TRHead.Buttons.args} />],
    slim: true,
  },
});

export const WithLabels = Object.assign(Template.bind({}), {
  args: {
    children: [<TRHead.WithLabels key="head" {...TRHead.WithLabels.args} />],
  },
});

export const Primary = Object.assign(Template.bind({}), {
  args: {
    children: [<TRHead.Default key="head" {...TRHead.Default.args} />],
    primary: true,
  },
});
