import { Story, Meta } from "@storybook/react";

import { TRBody, TRBodyProps } from "./Row";

const meta: Meta = {
  title: "TRBody",
  component: TRBody,
};

export default meta;

const Template: Story<TRBodyProps> = (args) => <TRBody {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    children: "This is some content",
  },
});
