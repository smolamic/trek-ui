import styled, { css } from "styled-components";
import { HasPalette, shade } from "../Theme";
import { transparentize } from "polished";

const DEFAULT_COLUMN_WIDTH_REM = 8;

export type CellProps = {
  span?: number;
  align?: "left" | "right" | "center";
};

export const Data = styled.div<CellProps>`
  display: flex;
  flex-direction: column;
  width: ${({ span = 1 }) => `${span * DEFAULT_COLUMN_WIDTH_REM}rem`};
  flex-grow: ${({ span = 1 }) => span};
  padding: 0.2rem 0.4rem;

  ${({ align = "left" }) => {
    switch (align) {
      case "left":
        return css`
          text-align: left;
          align-items: flex-start;
        `;
      case "right":
        return css`
          text-align: right;
          align-items: flex-end;
        `;
      case "center":
        return css`
          text-align: center;
          align-items: center;
        `;
    }
  }}
`;

export const DataLabel = styled.div`
  font-size: 0.9em;
  opacity: 0.8;
  display: inline-block;
`;

export const DataContent = styled.div`
  display: inline-block;
`;

export const OrderCaret = styled.span`
  display: inline-block;
  width: 1em;
  margin-right: 0.2rem;
`;

export type HeadProps = CellProps & HasPalette;

export const Head = styled.div<CellProps>`
  display: flex;
  flex-direction: row;
  align-items: center;
  width: ${({ span = 1 }) => `${span * DEFAULT_COLUMN_WIDTH_REM}rem`};
  flex-grow: ${({ span = 1 }) => span};
  box-sizing: border-box;
  padding: 0.3rem 0.4rem;
  font-weight: bold;
  ${({ align = "left" }) => {
    switch (align) {
      case "left":
        return css`
          text-align: left;
          justify-content: flex-start;
        `;
      case "right":
        return css`
          text-align: right;
          justify-content: flex-end;
        `;
      case "center":
        return css`
          text-align: center;
          justify-content: center;
        `;
    }
  }}
`;

export const HeadTitle = styled.span`
  display: inline-block;
`;
