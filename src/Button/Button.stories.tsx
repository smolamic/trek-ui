import { Story, Meta } from "@storybook/react";

import { Button, ButtonProps } from "./Button";

const meta: Meta = {
  title: "Button",
  component: Button,
};

export default meta;

const Template: Story<ButtonProps & { label: string }> = ({
  label,
  ...args
}) => <Button {...args}>{label}</Button>;

export const Default = Template.bind({});
Default.args = { label: "Button" };

export const Primary = Template.bind({});
Primary.args = {
  ...Default.args,
  primary: true,
};

export const Secondary = Template.bind({});
Secondary.args = {
  ...Default.args,
  secondary: true,
};

export const Slim = Template.bind({});
Slim.args = {
  ...Default.args,
  slim: true,
};

export const Filled = Template.bind({});
Filled.args = {
  ...Default.args,
  filled: true,
};
