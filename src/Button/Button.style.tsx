import styled, { css, StyledComponentProps } from "styled-components";
import {
  bp,
  shade,
  HasFilledVersion,
  HasPalette,
  HasSlimVersion,
  CanGrow,
  isSlim,
  isFilled,
  grow,
} from "../Theme";

type ButtonOwnProps = HasSlimVersion & HasPalette & HasFilledVersion & CanGrow;

export type ButtonProps = StyledComponentProps<
  "button",
  any,
  ButtonOwnProps,
  never
>;

/**
 * The Main Button Component
 */
export const Button = styled.button<ButtonOwnProps>`
  border: 1px solid ${shade.dark};
  padding: 0.3rem;
  margin: 0.2rem;
  color: ${shade.darker};
  background: none;
  outline: none;
  opacity: 1;

  ${grow}

  ${(props) =>
    props.grow &&
    css`
      flex-grow: ;
    `}

  :disabled {
    opacity: 0.6;
  }

  :active {
    box-shadow: inset 0 0 0.2rem ${shade.medium};

    :disabled {
      box-shadow: none;
    }
  }

  ${(props) =>
    isSlim(props) &&
    css`
      padding: 0.1rem;
      margin: 0.1rem;
    `}

  ${(props) =>
    isFilled(props) &&
    css`
      border: 1px solid ${shade.medium};
      background-color: ${shade.light};
      color: ${shade.darker};

      :focus {
        border-color: ${shade.dark};
      }
    `}


  @media screen and ${bp.isDesktop} {
      padding: 0.2rem;
      border: none;

      :hover {
          box-shadow 0 0.05rem 0.2rem ${shade.medium};
      }

      :active {
          box-shadow 0 0 0.1rem ${shade.medium};
      }
  }
`;
