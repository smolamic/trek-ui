import { WithPagination } from "./Sheet.stories";
import { render } from "@testing-library/react";

describe("Sheet", () => {
  describe("WithPagination", () => {
    it("marks the right page as active", () => {
      const { getByRole } = render(
        <WithPagination
          {...WithPagination.args}
          count={12}
          pageSize={4}
          offset={4}
        />
      );

      expect(getByRole("button", { name: "Go to page 2" })).toHaveAttribute(
        "aria-current",
        "page"
      );
    });
  });
});
