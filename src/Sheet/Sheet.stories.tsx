import { Story, Meta } from "@storybook/react";

import { Sheet, SheetProps } from "./Sheet";
import { Column } from "./Column";
import { H5 } from "../Heading";

const meta: Meta = {
  title: "Sheet",
  component: Sheet,
};

export default meta;

const Template: Story<SheetProps> = (args) => <Sheet {...args} />;

export const Default = Object.assign(Template.bind({}), {
  args: {
    columns: [
      <Column key="1" name="string" type="string">
        String
      </Column>,
      <Column key="2" name="number" type="number">
        Number
      </Column>,
    ],
    data: [
      { key: "1", string: "abc", number: "12.34" },
      { key: "2", string: "def", number: "56.78" },
      { key: "3", string: "ghi", number: "90.00" },
      { key: "4", string: "jkl", number: "105.40" },
    ],
    offset: 0,
    pageSize: 10,
    count: 4,
  },
});

export const Mobile = Object.assign(Template.bind({}), {
  args: Default.args,
  decorators: [
    (Story: React.ElementType) => (
      <div style={{ width: "300px" }}>
        <Story />
      </div>
    ),
  ],
});

const ActiveStory: Story<SheetProps> = (args) => (
  <Sheet {...args}>
    <H5>Row 3</H5>
    <p>This is some row content</p>
  </Sheet>
);
export const Active = Object.assign(ActiveStory, {
  args: {
    ...Default.args,
    activeRow: "3",
  },
});

export const WithPagination = Object.assign(Template.bind({}), {
  args: {
    ...Default.args,
    count: 12,
    pageSize: 4,
    offset: 4,
  },
});
