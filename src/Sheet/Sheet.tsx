import * as React from "react";
import * as T from "../Table";
import { Column, ColumnProps, thFromColumn, tdFromColumn } from "./Column";
import * as P from "../Pagination";

export type Data = { key: string; [key: string]: string };

type RowKey = {
  rowKey?: string;
};

type ForwardRef = {
  ref?: React.RefObject<HTMLDivElement>;
};

type ColumnName = {
  columnName: string;
};

export type SheetProps = {
  columns: React.ReactElement<ColumnProps, typeof Column>[];
  data: Data[];
  offset?: number;
  pageSize?: number;
  count?: number;
  activeRow?: string;
  children?: React.ReactNode;

  renderTRHead?: (props: T.TRHeadProps, data: Data) => React.ReactNode;
  renderPagination?: (props: P.PaginationProps) => React.ReactNode;
};

function isHTMLElement(node: ChildNode): node is HTMLElement {
  return node instanceof HTMLElement;
}

const CHECK_LINE_BREAK_TIMEOUT = 50;

function calculatePages(count?: number, pageSize?: number): number {
  if (count === undefined || pageSize === undefined) {
    return 1;
  }
  return Math.ceil(count / pageSize);
}

function calculateActivePage(offset: number, pageSize?: number): number {
  if (pageSize === undefined) {
    return 1;
  }
  return Math.floor(offset / pageSize) + 1;
}

function defaultRenderTRHead(props: T.TRHeadProps): JSX.Element {
  return <T.TRHead {...props} />;
}

function defaultRenderPagination(props: P.PaginationProps): JSX.Element {
  return <P.Pagination {...props} />;
}

export const Sheet: React.FC<SheetProps> = ({
  columns,
  data,
  activeRow,
  children,
  pageSize,
  offset = 0,
  count,

  renderTRHead = defaultRenderTRHead,
  renderPagination = defaultRenderPagination,
}) => {
  const headRowRef = React.useRef<HTMLDivElement>(null);

  const [lineBreak, setLineBreak] = React.useState(false);

  React.useEffect(() => {
    function checkLineBreak() {
      if (headRowRef.current === null) {
        return;
      }
      const cells = Array.from(headRowRef.current.childNodes)
        .slice(1)
        .filter(isHTMLElement);
      setLineBreak(cells.some((cell) => cell.offsetLeft === 0));
    }

    let timeout: number;
    function checkLineBreakDebounced() {
      window.clearTimeout(timeout);
      timeout = window.setTimeout(checkLineBreak, CHECK_LINE_BREAK_TIMEOUT);
    }

    window.addEventListener("resize", checkLineBreakDebounced, {
      passive: true,
    });

    checkLineBreak();

    return () => {
      window.removeEventListener("resize", checkLineBreakDebounced);
    };
  }, [headRowRef.current]);

  const ths = columns.map((column, index) => thFromColumn(column));

  const trs = data.map((record) => {
    const isActive = !!activeRow && record.key === activeRow;
    return (
      <T.TR key={record.key}>
        {renderTRHead(
          {
            primary: isActive,
            children: columns.map((column) =>
              tdFromColumn(column, record[column.props.name])
            ),
          },
          record
        )}
        {isActive && <T.TRBody>{children}</T.TRBody>}
      </T.TR>
    );
  });

  const pages = calculatePages(count, pageSize);
  const activePage = calculateActivePage(offset, pageSize);

  return (
    <T.Table showLabels={lineBreak}>
      <T.THead>
        <T.TR>
          <T.TRHead ref={headRowRef}>{ths}</T.TRHead>
        </T.TR>
      </T.THead>
      <T.TBody>{trs}</T.TBody>
      {pages > 1 && (
        <T.TFoot>
          {renderPagination({
            pages,
            activePage,
          })}
        </T.TFoot>
      )}
    </T.Table>
  );
};
