import * as React from "react";
import * as T from "../Table";

export type ColumnType =
  | {
      type?: "string";
    }
  | {
      type: "number";
    }
  | {
      type: "boolean";
    };

export type ColumnProps = ColumnType & {
  name: string;
  children: string;
  order?: "asc" | "desc";
  span?: number;
};

export const Column: React.FC<ColumnProps> = () => null;

type TypedTH = (column: React.ReactElement<ColumnProps>) => JSX.Element;

const stringTH: TypedTH = ({ props }) => (
  <T.TH
    key={props.name}
    order={props.order}
    span={props.span ?? 2}
    align="left"
  >
    {props.children}
  </T.TH>
);

const numberTH: TypedTH = ({ props }) => (
  <T.TH
    key={props.name}
    order={props.order}
    span={props.span ?? 1}
    align="right"
  >
    {props.children}
  </T.TH>
);

const booleanTH: TypedTH = ({ props }) => (
  <T.TH
    key={props.name}
    order={props.order}
    span={props.span ?? 1}
    align="center"
  >
    {props.children}
  </T.TH>
);

export const thFromColumn: TypedTH = (column) => {
  switch (column.props.type) {
    case "number":
      return numberTH(column);
    case "boolean":
      return booleanTH(column);
    case "string":
    default:
      return stringTH(column);
  }
};

type TypedTD = (
  column: React.ReactElement<ColumnProps>,
  value: string
) => JSX.Element;

const stringTD: TypedTD = ({ props }, value) => (
  <T.TD
    key={props.name}
    label={props.children}
    span={props.span ?? 2}
    align="left"
  >
    {value}
  </T.TD>
);

const numberTD: TypedTD = ({ props }, value) => (
  <T.TD
    key={props.name}
    label={props.children}
    span={props.span ?? 1}
    align="right"
  >
    {value}
  </T.TD>
);

const booleanTD: TypedTD = ({ props }, value) => (
  <T.TD
    key={props.name}
    label={props.children}
    span={props.span ?? 1}
    align="center"
  >
    {value}
  </T.TD>
);

export const tdFromColumn: TypedTD = (column, value) => {
  switch (column.props.type) {
    case "number":
      return numberTD(column, value);
    case "boolean":
      return booleanTD(column, value);
    case "string":
    default:
      return stringTD(column, value);
  }
};
